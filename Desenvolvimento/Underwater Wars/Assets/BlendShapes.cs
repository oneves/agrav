﻿using UnityEngine;
using System.Collections;

public class BlendShapes : MonoBehaviour {


 
       int blendShapeCount;
       public SkinnedMeshRenderer skinnedMeshRendererHair;
    public SkinnedMeshRenderer skinnedMeshRendererPlane;   
    public SkinnedMeshRenderer skinnedMeshRendererTongue;
    public SkinnedMeshRenderer skinnedMeshRendererEyeL;
    public SkinnedMeshRenderer skinnedMeshRendererEyeR;
    public SkinnedMeshRenderer skinnedMeshRendererUpperTeeth;
    public SkinnedMeshRenderer skinnedMeshRendererLowertTeeth;
       public static int originBlend=0;
       public static int targetBlend=0;
       public  float blendOne = 0f;
       public  float blendTwo = 0f;
        public static float blendSpeed = 7f;
    public  float basespeed = 5f;
       static bool blendOneFinished = false;
       public static bool changeShape = false;
       static bool raising = false;
      static bool firsttime = true;
   
   
    
       
 
       void Start ()
       {
      
       }
 
       void Update ()
       {

                if (changeShape && firsttime)
               { 
                    
                    if (blendOne < 100f)
                  {

                  
                         skinnedMeshRendererHair.SetBlendShapeWeight(targetBlend, blendOne);
                     skinnedMeshRendererPlane.SetBlendShapeWeight(targetBlend, blendOne);   
                    skinnedMeshRendererTongue.SetBlendShapeWeight(targetBlend, blendOne);
                      skinnedMeshRendererEyeL.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererEyeR.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererUpperTeeth.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererLowertTeeth.SetBlendShapeWeight(targetBlend, blendOne);
                blendOne += (blendSpeed+basespeed);
                    } else if (blendOne >= 100f)
               {

                   raising = false;
                   firsttime = false;
                   changeShape = false;
                   originBlend = targetBlend;
               }
                 
                    }else if(changeShape && !firsttime && !raising){
                        skinnedMeshRendererHair.SetBlendShapeWeight(originBlend, blendOne);
                        skinnedMeshRendererPlane.SetBlendShapeWeight(originBlend, blendOne);
                        skinnedMeshRendererTongue.SetBlendShapeWeight(originBlend, blendOne);
                        skinnedMeshRendererEyeL.SetBlendShapeWeight(originBlend, blendOne);
                        skinnedMeshRendererEyeR.SetBlendShapeWeight(originBlend, blendOne);
                        skinnedMeshRendererUpperTeeth.SetBlendShapeWeight(originBlend, blendOne);
                        skinnedMeshRendererLowertTeeth.SetBlendShapeWeight(originBlend, blendOne);

                  //    Debug.Log(targetBlend + " -------" + blendOne);
                   blendOne -= (blendSpeed+basespeed);
                   if (blendOne <= 0)
                   {
                      // blendOne = 0;
                       raising = true;
                      
                   }
                           }

                if (changeShape && raising)
                {
                    skinnedMeshRendererHair.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererPlane.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererTongue.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererEyeL.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererEyeR.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererUpperTeeth.SetBlendShapeWeight(targetBlend, blendOne);
                    skinnedMeshRendererLowertTeeth.SetBlendShapeWeight(targetBlend, blendOne);
                  //  Debug.Log(targetBlend + " -------" + blendOne);
                    blendOne += (blendSpeed+basespeed);
                    if (blendOne >= 100f)
                    {

                        raising = false;

                        changeShape = false;
                        originBlend = targetBlend;
                    }
                }
           
                 
                    }

               
           /*
           if (changeShape && raising && firsttime)
           {

               if (blendOne < 100f)
               {

                   skinnedMeshRenderer.SetBlendShapeWeight(targetBlend, blendOne);

                   Debug.Log(targetBlend + " -------" + blendOne);
                   blendOne += blendSpeed;
                                  }
               else if (blendOne == 100f)
               {
                   raising = false;
                   firsttime = false;
                   changeShape = false;
                   originBlend = targetBlend;
               }



           }


           else if(changeShape && !raising && !FechouOTasco){

              

                   skinnedMeshRenderer.SetBlendShapeWeight(originBlend, blendTwo);

                   Debug.Log(targetBlend + " -------" + blendTwo);
                   blendTwo -= blendSpeed;
                   if (blendTwo <= -100)
                   {
                       blendOne = 0;
                       raising = true;
                       blendOneFinished = true;
                   }
                  
              
 
               
               }
         

           if (raising && blendOneFinished)
           {
               FechouOTasco = true;

               skinnedMeshRenderer.SetBlendShapeWeight(targetBlend, blendOne);

               Debug.Log(targetBlend + " -------" + blendOne);
               blendOne += blendSpeed;
               if (blendOne == 100)
               {

                   Debug.Log("hackfsdfad");
                   changeShape = false;
                   raising = false;
                   blendOneFinished = false;
                   FechouOTasco = false;
                   originBlend=targetBlend;
               }
           }
              */
               //skinnedMeshRenderer.SetBlendShapeWeight(targetBlend, 0);
           }
       


