﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BlindSonar : MonoBehaviour
{

    [SerializeField]
    public Submarine _submarine;
    [SerializeField]
    private float _listenAngle;
    [SerializeField]
    private float _listenRange;
    [SerializeField]
    private float _submarinePitch;
    [SerializeField]
    private float _minePitch;
    [SerializeField]
    private float _repeatTimeClosest;
    [SerializeField]
    private float _repeatTimeFarthest;

    private AudioSource _audioSource;
    private float _timeSinceLastPlay;

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (_submarine)
        {
            float halfListenAngle = _listenAngle * 0.5f;
            Vector3 forwardVector = Quaternion.Euler(0, 0, _submarine.TurretRotation) * Vector3.up;
            Unit closest = null;
            float closestAngle = 360;
            Collider2D[] colliders = Physics2D.OverlapCircleAll(_submarine.transform.position, _listenRange);
            for (int i = 0; i < colliders.Length; i++)
            {
                Unit unit = colliders[i].gameObject.GetComponent<Unit>();
                if (unit == null || unit.gameObject == _submarine.gameObject) continue;

                Vector3 direction = unit.transform.position - _submarine.transform.position;
                float absAngle = Mathf.Abs(Vector3.Angle(direction, forwardVector));
                if (absAngle < halfListenAngle && absAngle < closestAngle)
                {
                    closest = unit;
                    closestAngle = absAngle;
                }

            }

            if (closest != null)
            {
                float distance = (closest.transform.position - _submarine.transform.position).magnitude;
                float distancePercentage = 1 - (distance / _listenRange);
                float anglePercentage = 1 - (closestAngle / halfListenAngle);
                SoundAlarm(closest.Type, distancePercentage, anglePercentage);
            }

            _timeSinceLastPlay -= Time.deltaTime;
        }
    }

    private void SoundAlarm(Unit.UnitType unitType, float distancePercentage, float anglePercentage)
    {
        _audioSource.volume = anglePercentage;
        if (unitType == Unit.UnitType.MINE)
        {
            _audioSource.pitch = _minePitch;
        }
        else if (unitType == Unit.UnitType.SUBMARINE)
        {
            _audioSource.pitch = _submarinePitch;
        }
        if (_timeSinceLastPlay <= 0)
        {
            _audioSource.Play();

            float cdTime = (_repeatTimeFarthest - _repeatTimeClosest) * (1 - distancePercentage) + _repeatTimeClosest;
            _timeSinceLastPlay = cdTime;
        }
    }
}
