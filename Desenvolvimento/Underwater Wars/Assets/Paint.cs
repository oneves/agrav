﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;

public class Paint : MonoBehaviour
{
    public Button btn;

    public Image img;

    Texture2D txt;

    void Start()
    {
        img = GameObject.Find("Button").GetComponent<Image>();
        btn = GameObject.Find("Button").GetComponent<Button>();
        txt = img.sprite.texture;

        txt = new Texture2D(img.sprite.texture.width, img.sprite.texture.height);
    }

    void Update()
    {
        //  if (mouseDown)
        //{
           
            txt = Circle(txt, 0, txt.height, 3, Color.blue);
          
            Sprite s = Sprite.Create(txt, new Rect(0, 0, (int)txt.width, (int)txt.height), new Vector2(0.5f, 0.5f));
             img.sprite = s;
        //}
    }


    public Texture2D Circle(Texture2D tex, float cx, float cy, float r, Color col)
    {
        //byte[] or = tex.EncodeToJPG();
        //File.WriteAllBytes(@"c:\test\or" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".jpg", or);

        //float x, y, px, nx, py, ny, d;

        //for (x = 0; x <= r; x++)
        //{
        //    d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));
        //    for (y = 0; y <= d; y++)
        //    {
        //        px = cx + x;
        //        nx = cx - x;
        //        py = cy + y;
        //        ny = cy - y;

        //        tex.SetPixel((int)px, (int)py, col);
        //        tex.SetPixel((int)nx, (int)py, col);

        //        tex.SetPixel((int)px, (int)ny, col);
        tex.SetPixel((int)cx, (int)cy, col);
        tex.Apply();
        //}
        //}

        //byte[] fe = tex.EncodeToJPG();
        //File.WriteAllBytes(@"c:\test\fe" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".jpg", fe);

        return tex;
    }

}
