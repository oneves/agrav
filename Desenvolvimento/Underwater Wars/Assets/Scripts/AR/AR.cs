﻿using UnityEngine;
using System;

public class AReality : MonoBehaviour
{

    public bool fire = false;
    private float lastZ = 9.8f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        float currZ = Input.acceleration.z;
        if (Math.Abs(currZ - lastZ) > 5)
        {
            fire = true;
        }
        lastZ = currZ;
    }
}
