﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{

    public AudioSource Music;
    public AudioSource Effects1;
    public AudioSource Effects2;
    public AudioSource Effects3;
    public AudioSource Effects4;
    public AudioSource Effects5;
    public AudioSource SpecialEffects; // Used for the beep when changing volume
    public AudioSource Voice1;
    public AudioSource Voice2;
    public AudioSource Voice3;
    public AudioSource Voice4;
    public AudioSource Help;

    public AudioClip intro;

    public AudioClip music;
    public AudioClip beep;
    public AudioClip v_play;
    public AudioClip v_howto;
    public AudioClip v_howtodesc;
    public AudioClip v_set;
    public AudioClip v_cust;
    public AudioClip v_exit;
    public AudioClip v_back;
    public AudioClip v_save;
    public AudioClip v_mus;
    public AudioClip v_efe;
    public AudioClip v_voz;
    public AudioClip v_start;
    public AudioClip v_points;
    public AudioClip v_2;
    public AudioClip v_5;
    public AudioClip v_10;

    public AudioClip a1_1;
    public AudioClip a1_2;
    public AudioClip a2_1;
    public AudioClip a2_2;
    public AudioClip a2_3;
    public AudioClip a2_4;
    public AudioClip a3_1;
    public AudioClip a3_2;
    public AudioClip a4_1;
    public AudioClip a4_2;
    public AudioClip a5_1;
    public AudioClip a5_2;
    public AudioClip a6_1;
    public AudioClip a6_2;
    public AudioClip a9_1;
    public AudioClip a9_2;
    public AudioClip a10_1;

    //GAMEPLAY UI
    public AudioClip OuvirEstado;
    public AudioClip Disparar;
    public AudioClip Minar;
    public AudioClip Menu;
    public AudioClip Norte;
    public AudioClip Sul;
    public AudioClip Este;
    public AudioClip Oeste;
    public AudioClip Ajuda;
    public AudioClip analogPressed;
    public AudioClip analogReleased;
    public AudioClip helpMenu_blind_strat;
    public AudioClip health_75;
    public AudioClip health_50;
    public AudioClip health_25;

    //SONAR
    public AudioClip proximityWarningBeep;

    //TILEMAP_PING
    public AudioClip _1_coord;
    public AudioClip _2_coord;
    public AudioClip _3_coord;
    public AudioClip _4_coord;
    public AudioClip _5_coord;
    public AudioClip _6_coord;
    public AudioClip _7_coord;
    public AudioClip _8_coord;
    public AudioClip _9_coord;
    public AudioClip _10_coord;
    public AudioClip A_coord;
    public AudioClip B_coord;
    public AudioClip C_coord;
    public AudioClip D_coord;
    public AudioClip E_coord;
    public AudioClip F_coord;
    public AudioClip G_coord;
    public AudioClip H_coord;
    public AudioClip I_coord;
    public AudioClip J_coord;
    public AudioClip mineDeploy;
    public AudioClip pingOnCell;


    public void PlayVideoSound(AudioClip clip)
    {
        Music.clip = clip;
        Music.Play();
    }

    public void PlayMusic(string name)
    {
        Music.clip = GetAudioClip(name);
        Music.Play();
    }

    public void StopMusic()
    {
        Music.Stop();
    }

    public void PlayHelp(string name)
    {
        Help.clip = GetAudioClip(name);
        Help.Play();
    }

    public void StopHelp()
    {
        Help.Stop();
    }

    public void PlayEffectWithTempVolume(string name, float volume)
    {
        AudioClip clip = GetAudioClip(name);

        SpecialEffects.Stop();
        SpecialEffects.volume = volume;
        SpecialEffects.clip = clip;
        SpecialEffects.Play();
    }

    public void PlayEffect(string name)
    {

        AudioClip clip = GetAudioClip(name);

        if (!Effects1.isPlaying)
        {
            Effects1.clip = clip;
            Effects1.loop = false;
            Effects1.Play();
        }
        else if (!Effects2.isPlaying)
        {
            Effects2.clip = clip;
            Effects2.loop = false;
            Effects2.Play();
        }
        else if (!Effects3.isPlaying)
        {
            Effects3.clip = clip;
            Effects3.loop = false;
            Effects3.Play();
        }
        else if (!Effects4.isPlaying)
        {
            Effects4.clip = clip;
            Effects4.loop = false;
            Effects4.Play();
        }
        else if (!Effects5.isPlaying)
        {
            Effects5.clip = clip;
            Effects5.loop = false;
            Effects5.Play();
        }
    }
    public void StopEffect(string name)
    {

        AudioClip clip = GetAudioClip(name);

        if (Effects1.isPlaying && Effects1.clip.Equals(clip))
        {
            Effects1.loop = false;
            Effects1.Stop();
        }
        else if (Effects2.isPlaying && Effects2.clip.Equals(clip))
        {
            Effects2.loop = false;
            Effects2.Stop();
        }
        else if (Effects3.isPlaying && Effects3.clip.Equals(clip))
        {
            Effects3.loop = false;
            Effects3.Stop();
        }
        else if (Effects4.isPlaying && Effects4.clip.Equals(clip))
        {
            Effects4.loop = false;
            Effects4.Stop();
        }
        else if (Effects5.isPlaying && Effects5.clip.Equals(clip))
        {
            Effects5.loop = false;
            Effects5.Stop();
        }
    }
    public void StopEffect()
    {
        Effects1.Stop();
        Effects2.Stop();
        Effects3.Stop();
        Effects4.Stop();
        Effects5.Stop();
    }
    public void PlayEffect(string name, bool loop)
    {

        AudioClip clip = GetAudioClip(name);

        if (!Effects1.isPlaying)
        {
            Effects1.clip = clip;
            Effects1.loop = loop;
            Effects1.Play();
        }
        else if (!Effects2.isPlaying)
        {
            Effects2.clip = clip;
            Effects2.loop = loop;
            Effects2.Play();
        }
        else if (!Effects3.isPlaying)
        {
            Effects3.clip = clip;
            Effects3.loop = loop;
            Effects3.Play();
        }
        else if (!Effects4.isPlaying)
        {
            Effects4.clip = clip;
            Effects4.loop = loop;
            Effects4.Play();
        }
        else if (!Effects5.isPlaying)
        {
            Effects5.clip = clip;
            Effects5.loop = loop;
            Effects5.Play();
        }
    }


    public void PlayVoice(string name, float delay_inSec)
    {
        StartCoroutine(_playVoice(name, delay_inSec));
    }

    private IEnumerator _playVoice(string name, float time)
    {
        yield return new WaitForSeconds(time);
        PlayVoice(name);
    }

    public void PlayVoice(string name)
    {

        AudioClip clip = GetAudioClip(name);

        if (!Voice1.isPlaying)
        {
            Voice1.clip = clip;
            Voice1.Play();
        }
        else if (!Voice2.isPlaying)
        {
            Voice2.clip = clip;
            Voice2.Play();
        }
        else if (!Voice3.isPlaying)
        {
            Voice3.clip = clip;
            Voice3.Play();
        }
        else if (!Voice4.isPlaying)
        {
            Voice4.clip = clip;
            Voice4.Play();
        }
    }

    public void PlayVoice(AudioClip clip)
    {

        if (!Voice1.isPlaying)
        {
            Voice1.clip = clip;
            Voice1.Play();
        }
        else if (!Voice2.isPlaying)
        {
            Voice2.clip = clip;
            Voice2.Play();
        }
        else if (!Voice3.isPlaying)
        {
            Voice3.clip = clip;
            Voice3.Play();
        }
        else if (!Voice4.isPlaying)
        {
            Voice4.clip = clip;
            Voice4.Play();
        }
    }

    public void StopVoice(string name)
    {

        AudioClip clip = GetAudioClip(name);

        if (Voice1.clip == clip)
        {
            Voice1.Stop();
        }
        else if (Voice2.clip == clip)
        {
            Voice2.Stop();
        }
        else if (Voice3.clip == clip)
        {
            Voice3.Stop();
        }
        else if (Voice4.clip == clip)
        {
            Voice4.Stop();
        }
    }

    public void StopAllVoices()
    {
        Voice1.Stop();
        Voice2.Stop();
        Voice3.Stop();
        Voice4.Stop();
    }
    public void StopAllVoices(float muteDuration_sec)
    {
        Voice1.mute = true;
        Voice2.mute = true;
        Voice3.mute = true;
        Voice4.mute = true;
        StartCoroutine(_stopAllVoices(muteDuration_sec));
    }
    private IEnumerator _stopAllVoices(float muteDuration_sec)
    {
        yield return new WaitForSeconds(muteDuration_sec);
        Voice1.mute = false;
        Voice2.mute = false;
        Voice3.mute = false;
        Voice4.mute = false;
    }

    public void StopVoice()
    {
        Voice1.Stop();
        Voice2.Stop();
        Voice3.Stop();
        Voice4.Stop();
    }

    public void ChangeMusicVolume(float volume)
    {
        Music.volume = volume;
        GameObject.Find("PreLoad").GetComponent<PreLoad>().MusicVolume = volume;
    }

    public void ChangeEffectsVolume(float volume)
    {
        Effects1.volume = volume;
        Effects2.volume = volume;
        Effects3.volume = volume;
        Effects4.volume = volume;
        Effects5.volume = volume;
        GameObject.Find("PreLoad").GetComponent<PreLoad>().EffectsVolume = volume;
    }

    public void ChangeVoiceVolume(float volume)
    {
        Voice1.volume = volume;
        Voice2.volume = volume;
        Voice3.volume = volume;
        Voice4.volume = volume;
        GameObject.Find("PreLoad").GetComponent<PreLoad>().VoiceVolume = volume;
    }

    private AudioClip GetAudioClip(string name)
    {
        AudioClip clip = null;

        if (name.Equals("intro"))
            clip = intro;
        else if (name.Equals("music"))
            clip = music;
        else if (name.Equals("beep"))
            clip = beep;
        else if (name.Equals("play"))
            clip = v_play;
        else if (name.Equals("howto"))
            clip = v_howto;
        else if (name.Equals("set"))
            clip = v_set;
        else if (name.Equals("cust"))
            clip = v_cust;
        else if (name.Equals("exit"))
            clip = v_exit;
        else if (name.Equals("back"))
            clip = v_back;
        else if (name.Equals("save"))
            clip = v_save;
        else if (name.Equals("mus"))
            clip = v_mus;
        else if (name.Equals("efe"))
            clip = v_efe;
        else if (name.Equals("voz"))
            clip = v_voz;
        else if (name.Equals("start"))
            clip = v_start;
        else if (name.Equals("howtodesc"))
            clip = v_howtodesc;
        else if (name.Equals("points"))
            clip = v_points;
        else if (name.Equals("2"))
            clip = v_2;
        else if (name.Equals("5"))
            clip = v_5;
        else if (name.Equals("10"))
            clip = v_10;
        else if (name.Equals("1_1"))
            clip = a1_1;
        else if (name.Equals("1_2"))
            clip = a1_2;
        else if (name.Equals("2_1"))
            clip = a2_1;
        else if (name.Equals("2_2"))
            clip = a2_2;
        else if (name.Equals("2_3"))
            clip = a2_3;
        else if (name.Equals("2_4"))
            clip = a2_4;
        else if (name.Equals("3_1"))
            clip = a3_1;
        else if (name.Equals("3_2"))
            clip = a3_2;
        else if (name.Equals("4_1"))
            clip = a4_1;
        else if (name.Equals("4_2"))
            clip = a4_2;
        else if (name.Equals("5_1"))
            clip = a5_1;
        else if (name.Equals("5_2"))
            clip = a5_2;
        else if (name.Equals("6_1"))
            clip = a6_1;
        else if (name.Equals("6_2"))
            clip = a6_2;
        else if (name.Equals("9_1"))
            clip = a9_1;
        else if (name.Equals("9_2"))
            clip = a9_2;
        else if (name.Equals("10_1"))
            clip = a10_1;
        else if (name.Equals("OuvirEstado"))
            clip = OuvirEstado;
        else if (name.Equals("Disparar"))
            clip = Disparar;
        else if (name.Equals("Minar"))
            clip = Minar;
        else if (name.Equals("Menu"))
            clip = Menu;
        else if (name.Equals("Norte"))
            clip = Norte;
        else if (name.Equals("Sul"))
            clip = Sul;
        else if (name.Equals("Este"))
            clip = Este;
        else if (name.Equals("Oeste"))
            clip = Oeste;
        else if (name.Equals("Ajuda"))
            clip = Ajuda;
        else if (name.Equals("proximityWarningBeep"))
            clip = proximityWarningBeep;
        else if (name.Equals("analogPressed"))
            clip = analogPressed;
        else if (name.Equals("analogReleased"))
            clip = analogReleased;
        else if (name.Equals("helpMenu_blind_strat"))
            clip = helpMenu_blind_strat;
        else if (name.Equals("_1_coord"))
            clip = _1_coord;
        else if (name.Equals("_2_coord"))
            clip = _2_coord;
        else if (name.Equals("_3_coord"))
            clip = _3_coord;
        else if (name.Equals("_4_coord"))
            clip = _4_coord;
        else if (name.Equals("_5_coord"))
            clip = _5_coord;
        else if (name.Equals("_6_coord"))
            clip = _6_coord;
        else if (name.Equals("_7_coord"))
            clip = _7_coord;
        else if (name.Equals("_8_coord"))
            clip = _8_coord;
        else if (name.Equals("_9_coord"))
            clip = _9_coord;
        else if (name.Equals("_10_coord"))
            clip = _10_coord;
        else if (name.Equals("A_coord"))
            clip = A_coord;
        else if (name.Equals("B_coord"))
            clip = B_coord;
        else if (name.Equals("C_coord"))
            clip = C_coord;
        else if (name.Equals("D_coord"))
            clip = D_coord;
        else if (name.Equals("E_coord"))
            clip = E_coord;
        else if (name.Equals("F_coord"))
            clip = F_coord;
        else if (name.Equals("G_coord"))
            clip = G_coord;
        else if (name.Equals("H_coord"))
            clip = H_coord;
        else if (name.Equals("I_coord"))
            clip = I_coord;
        else if (name.Equals("J_coord"))
            clip = J_coord;
        else if (name.Equals("mineDeploy"))
            clip = mineDeploy;
        else if (name.Equals("pingOnCell"))
            clip = pingOnCell;
        else if (name.Equals("health_75"))
            clip = health_75;
        else if (name.Equals("health_50"))
            clip = health_50;
        else if (name.Equals("health_25"))
            clip = health_25;


        return clip;
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }
}
