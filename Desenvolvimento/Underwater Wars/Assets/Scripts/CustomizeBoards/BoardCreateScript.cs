﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using SimpleJSON;

public class BoardCreateScript : MonoBehaviour
{
    private const string IMAGE_UPLOAD_URL = "http://akatsua.ddns.net/uwars/uploadImage";
    private const string IMAGE_DOWNLOAD_URL = "http://akatsua.ddns.net/uwars/downloadImage";
    private const string IMAGE_DOWNLOAD2_URL = "http://akatsua.ddns.net/uwars/downloadImage2";
    private const string GET_IMAGES_URL = "http://akatsua.ddns.net/uwars/getUserImages";

    //private Dictionary<string, string> serverImages;

    private string[] boardFileNames = { "Skateboard", "Skate_Longboard", "PranchasSurf_Bodyboard-03", "pranchaSurf_Longboard", "pranchaSurf_Shortboard" };

    private string[] skateFileNames = { "Skateboard", "Skate_Longboard" };
    private string[] surfFileNames = { "PranchasSurf_Bodyboard-03", "pranchaSurf_Longboard", "pranchaSurf_Shortboard" };

    private string[] boardsMade;

    private List<string> boardNames = new List<string> { "Skateboard", "Skate longboard", "Surf bodyboard", "Surf longboard", "Surf shortboard" };
    private List<string> skateNames = new List<string> { "Skateboard", "Skate longboard" };
    private List<string> surfNames = new List<string> { "Surf bodyboard", "Surf longboard", "Surf shortboard" };

    private Dropdown optionsBoards;
    private Image boardImage;

    private bool clicking = false;

    private float reasonX, reasonY;

    private Color boardColor = new Color(1, 1, 1, 1);
    private Color blackColor = new Color(0, 0, 0, 1);
    public static Color selectedColor = new Color(1, 1, 1, 1);
    private bool created = false;
    private Sprite spriteBoard;
    private Sprite spriteBoardBackup;
    private Vector2 imageRectSize;
    private Vector2 previousPoint;
    private List<Vector3> pointsList;
    private float colorDiff = 0.01f;

    private int margin = 3;
    private int marginMax = 10;
    private int marginMin = 1;

    public Text res;

    private string dir;
    private List<Sprite> imgs;
    int count = 0;
    private int downloadingCount = 0;
    int madeValue;
    private Image boardMadeImage;

    public void Start()
    {
        dir = Application.persistentDataPath + "/CreatedBoards/";
        getUserImages(int.Parse(PreLoad.GetPlayer().userId));
        

        if (!Directory.Exists(Application.persistentDataPath + "/TorpedoSprites"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/TorpedoSprites");
        }

        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }


        btn1Img.gameObject.SetActive(true);
        btn2Img.gameObject.SetActive(false);

        //optionsBoards = gameObject.transform.FindChild("OptionsBoards").gameObject.GetComponent<Dropdown>();
        //optionsBoards.AddOptions(boardNames);
        boardImage = gameObject.transform.FindChild("UI").gameObject.transform.FindChild("editBoard").gameObject.transform.FindChild("BoardImage").gameObject.GetComponent<Image>();
        boardMadeImage = gameObject.transform.FindChild("UI").gameObject.transform.FindChild("viewCreated").gameObject.transform.FindChild("BoardMadeImg").gameObject.GetComponent<Image>();
        pointsList = new List<Vector3>();
        previousPoint = new Vector2(-13, -37);

        res.text = boardImage.rectTransform.sizeDelta.x.ToString() + "X" + boardImage.rectTransform.sizeDelta.y.ToString();
        var pts = findPoints(new Vector2(0, 4), new Vector2(2, 4), 2);

        select("skate");

    }
    

    void loadMadeBoards()
    {
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        boardsMade = Directory.GetFiles(dir);
        int countBoards = boardsMade.Length;
        imgs = new List<Sprite>();

        if (countBoards > 1)
        {
            madeValue = 0;
            for (int i = 0; i < boardsMade.Length; i++)
            {
                if (!boardsMade[i].Contains("meta"))
                {
                    Texture2D boardImg = new Texture2D(1, 1);
                    byte[] image = File.ReadAllBytes(boardsMade[i]);
                    boardImg.LoadImage(image);
                    Sprite madeSpriteBoard = Sprite.Create(boardImg, new Rect(0, 0, boardImg.width, boardImg.height), new Vector2(0.5f, 0.5f));
                    imgs.Add(madeSpriteBoard);
                }
            }
            boardMadeImage.color = new Color(1, 1, 1, 1);
            boardMadeImage.sprite = imgs[0];
        }
        else { madeValue = -1; }

    }

    public void Update()
    {
        if (created)
        {
            if (Input.GetMouseButton(0))
            {
                if (clicking == false)
                {
                    clicking = true;
                    RectTransform rect = boardImage.GetComponent<RectTransform>();
                    Vector3 s = rect.lossyScale;
                    float w = rect.rect.width * s.x;
                    float h = rect.rect.height * s.y;
                    imageRectSize = new Vector2(w, h);
                    reasonX = imageRectSize.x / spriteBoard.texture.width;
                    reasonY = imageRectSize.y / spriteBoard.texture.height;

                    res.text = boardImage.rectTransform.sizeDelta.x.ToString() + "X" + boardImage.rectTransform.sizeDelta.y.ToString();
                }
            }
            else
            {
                if (clicking == true)
                {
                    clicking = false;
                    previousPoint = new Vector2(-13, -37);
                }
            }
        }
        if (clicking)
        {
            RectTransform rect = boardImage.GetComponent<RectTransform>();
            Vector2 mousePosition = Input.mousePosition;
            float x = mousePosition.x - (rect.position.x - imageRectSize.x / 2);
            float y = /*imageRectSize.y - */mousePosition.y - (rect.position.y - imageRectSize.y / 2);
            x = x / reasonX;
            y = y / reasonY;
            int xi = Mathf.RoundToInt(x);
            int yj = Mathf.RoundToInt(y);

            if (previousPoint.x >= 0 && previousPoint.x <= spriteBoard.rect.width && previousPoint.y >= 0 && previousPoint.y <= spriteBoard.rect.height)
            {
                float dist = checkDistance(previousPoint, new Vector2(xi, yj));
                var points = findPoints(previousPoint, new Vector2(xi, yj), Mathf.RoundToInt(dist / margin));
                foreach (Vector2 p in points)
                {
                    paintDot((int)p.x, (int)p.y, margin);
                }
                spriteBoard.texture.Apply();
            }
            previousPoint = new Vector2(xi, yj);
        }
    }


    //public void createBoard()
    //{
    //    Texture2D boardImg = new Texture2D(1, 1);
    //    byte[] image;
    //    string path = dir + boardFileNames[optionsBoards.value] + ".png";
    //    image = File.ReadAllBytes(path);
    //    boardImg.LoadImage(image);
    //    spriteBoard = Sprite.Create(boardImg, new Rect(0, 0, boardImg.width, boardImg.height), new Vector2(0.5f, 0.5f));
    //    boardImage.color = new Color(1, 1, 1, 1);
    //    boardColor = new Color(1, 1, 1, 1);
    //    boardImage.sprite = spriteBoard;
    //    created = true;
    //}

    //public void plusPress()
    //{
    //    margin++;
    //    if (margin > marginMax) margin = marginMax;
    //}

    //public void minusPress()
    //{
    //    margin--;
    //    if (margin < marginMin) margin = marginMin;
    //}

    public void paintBoard()
    {
        int w = spriteBoard.texture.width, h = spriteBoard.texture.height;
        for (int y = 0; y < h; y++)
        {
            for (int x = 0; x < w; x++)
            {
                Color c = spriteBoard.texture.GetPixel(x, y);
                if (sameColor(c, boardColor))
                {
                    spriteBoard.texture.SetPixel(x, y, selectedColor);
                }
            }
        }
        spriteBoard.texture.Apply();
        boardColor = selectedColor;
    }

    private List<Vector2> findPoints(Vector2 a, Vector2 b, int margin)
    {
        List<Vector2> pointList = new List<Vector2>();
        int count = margin + 1;
        double d = Mathf.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)) / count;
        float fi = Mathf.Atan2(b.y - a.y, b.x - a.x);

        for (int i = 0; i <= count; ++i)
            pointList.Add(new Vector2((float)(a.x + i * d * Mathf.Cos(fi)), (float)(a.y + i * d * Mathf.Sin(fi))));

        return pointList;
    }

    private void paintDot(int x, int y, int margin)
    {
        for (int i = x - margin; i < x + margin; i++)
        {
            for (int j = y - margin; j < y + margin; j++)
            {
                Color c = spriteBoard.texture.GetPixel(i, j);
                if (c.a != 0 && !sameColor(c, blackColor))
                {
                    spriteBoard.texture.SetPixel(i, j, selectedColor);
                }
            }
        }
    }

    private float checkDistance(Vector2 a, Vector2 b)
    {
        return (Mathf.Pow(a.x - b.x, 2) + Mathf.Pow(a.y - b.y, 2));
    }

    private bool sameColor(Color a, Color b)
    {
        return Mathf.Abs(a.r - b.r) < colorDiff && Mathf.Abs(a.g - b.g) < colorDiff && Mathf.Abs(a.b - b.b) < colorDiff && Mathf.Abs(a.a - b.a) < colorDiff;
    }

    public void saveFile()
    {
        byte[] bytes = spriteBoard.texture.EncodeToPNG();
        string fileName = dir + current + count + ".png";
        File.WriteAllBytes(fileName, bytes);
        uploadImage(bytes, spriteBoard.texture.width, spriteBoard.texture.height, current + count + ".png");
        count++;
        Sprite s = Sprite.Create(Instantiate(spriteBoard.texture), new Rect(0, 0, spriteBoard.texture.width, spriteBoard.texture.height), new Vector2(0.5f, 0.5f));
        imgs.Add(s);
        madeValue = count - 1;
        boardMadeImage.color = new Color(1, 1, 1, 1);
        boardMadeImage.sprite = imgs[madeValue];
    }

    public Button btnSkate;
    public Button btnSurf;
    public GameObject btn1Img;
    public GameObject btn2Img;

    int skateValue = 0;
    int surfValue = 0;

    string current = "";

    public void select(string board)
    {
        current = board;
        string name = "";

        if (board.Equals("skate"))
        {
            btn1Img.gameObject.SetActive(true);
            btn2Img.gameObject.SetActive(false);

            name = skateFileNames[skateValue];
        }
        if (board.Equals("surf"))
        {
            btn1Img.gameObject.SetActive(false);
            btn2Img.gameObject.SetActive(true);

            name = surfFileNames[surfValue];
        }

        //Texture2D boardImg = new Texture2D(1, 1);
        //byte[] image;
        //image = File.ReadAllBytes(Application.dataPath + "/Images/" + name + ".png");
        //boardImg.LoadImage(image);

        Texture2D boardImg = Resources.Load<Texture2D>(name);        
        spriteBoard = Sprite.Create(boardImg, new Rect(0, 0, boardImg.width, boardImg.height), new Vector2(0.5f, 0.5f));
        boardImage.color = new Color(1, 1, 1, 1);
        boardColor = new Color(1, 1, 1, 1);
        boardImage.sprite = spriteBoard;
        spriteBoardBackup = Sprite.Create(boardImg, new Rect(0,0, boardImg.width,boardImg.height), new Vector2(0.5f, 0.5f));
        created = true;
    }

    public void next()
    {
        if (current.Equals("skate"))
        {
            skateValue++;

            if (skateValue > skateNames.Count - 1)
            {
                skateValue = 0;
            }
        }

        if (current.Equals("surf"))
        {
            surfValue++;

            if (surfValue > surfNames.Count - 1)
            {
                surfValue = 0;
            }
        }

        spriteBoard = spriteBoardBackup;
        select(current);

    }

    public void previous()
    {
        if (current.Equals("skate"))
        {
            skateValue--;

            if (skateValue < 0)
            {
                skateValue = skateNames.Count - 1;
            }
        }

        if (current.Equals("surf"))
        {
            surfValue--;

            if (surfValue < 0)
            {
                surfValue = surfNames.Count - 1;
            }
        }

        spriteBoard = spriteBoardBackup;
        select(current);
    }

    public void backBtnPress()
    {
        spriteBoard = spriteBoardBackup;
    }   

    public void next2()
    {
        if (count > 0)
        {
            madeValue++;

            if (madeValue > imgs.Count - 1)
            {
                madeValue = 0;
            }

            boardMadeImage.sprite = imgs[madeValue];
        }
    }

    public void previous2()
    {
        if (count > 0)
        {
            madeValue--;

            if (madeValue < 0)
            {
                madeValue = imgs.Count - 1;
            }

            boardMadeImage.sprite = imgs[madeValue];
        }
    }

    public void see()
    {
        if (count > 0)
        {
            spriteBoard = Sprite.Create(Instantiate(boardMadeImage.sprite.texture), new Rect(0, 0, boardMadeImage.sprite.texture.width, boardMadeImage.sprite.texture.height), new Vector2(0.5f, 0.5f));
            boardImage.color = new Color(1, 1, 1, 1);
            boardImage.sprite = spriteBoard;
            created = true;
        }
    }

    public void setAsDefault()
    {
        int w = spriteBoard.texture.width, h = spriteBoard.texture.height;
        bool searchingForBeggin = true;
        List<int> points = new List<int>(6);
        for (int x = 0; x < w; x++)
        {
            bool found = false;
            for (int y = 0; y < h; y++)
            {
                Color c = spriteBoard.texture.GetPixel(x, y);
                if (c.a != 0)
                {
                    found = true;
                    break;
                }
            }
            if (searchingForBeggin && found)
            {
                points.Add(x-1);
                searchingForBeggin = false;
            }
            else
            {
                if (!searchingForBeggin && !found)
                {
                    points.Add(x + 2);
                    searchingForBeggin = true;
                }
            }
        }
        //Sprite front = Sprite.Create(spriteBoard.texture, new Rect(points[0], 0, points[1]-points[0], spriteBoard.texture.height), new Vector2(0.5f, 0.5f));
        //Sprite back = Sprite.Create(Instantiate(spriteBoard.texture), new Rect(points[4], 0, points[5]-points[4], spriteBoard.texture.height), new Vector2(0.5f, 0.5f));
        Texture2D side1 = new Texture2D(points[1] - points[0], spriteBoard.texture.height);
        for(int i = points[0]; i< points[1]; i++)
        {
            for(int j = 0; j< side1.height; j++)
            {
                side1.SetPixel(i-points[0], j, spriteBoard.texture.GetPixel(i, j));
            }
        }
        byte[] bytes = side1.EncodeToPNG();
        string fileName = Application.persistentDataPath + "/TorpedoSprites/side1.png";

        File.WriteAllBytes(fileName, bytes);

        Texture2D side2 = new Texture2D(points[5] - points[4], spriteBoard.texture.height);
        for (int i = points[4]; i < points[5]; i++)
        {
            for (int j = 0; j < side2.height; j++)
            {
                side2.SetPixel(i - points[4], j, spriteBoard.texture.GetPixel(i, j));
            }
        }
        bytes = side2.EncodeToPNG();
        fileName = Application.persistentDataPath + "/TorpedoSprites/side2.png";
        File.WriteAllBytes(fileName, bytes);

    }

    public void setSelectedColor(int color)
    {
        switch (color)
        {
            case 0:
                selectedColor = new Color(255, 0, 0);
                break;
            case 1:
                selectedColor = new Color(0, 0, 255);
                break;
            case 2:
                selectedColor = new Color(0, 255, 0);
                break;
            case 3:
                selectedColor = new Color(255, 255, 0);
                break;
            case 4:
                selectedColor = new Color(0, 0, 0);
                break;
            default:
                selectedColor = new Color(255, 255, 255);
                break;
        }
    }


    public void uploadImage(byte[] image, int width, int height, string fileName)
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
        };

        Player player = PreLoad.GetPlayer();
        string userId = player.userId;

        var postParams = new Dictionary<string, string>()
                {
                    { "userId", userId },
                    { "width", width.ToString() },
                    { "height", height.ToString() }
                };

        rest.POSTBinaryData(IMAGE_UPLOAD_URL, postParams, image, fileName, methodCall);
        Debug.Log("done posting");
    }


    public void downloadImage(string serverFilePath, string localFilePath)
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            Texture2D downloadedTexture = rest.WWWResults.texture;
            byte[] bytes = downloadedTexture.EncodeToPNG();
            File.WriteAllBytes(localFilePath, bytes);
            downloadingCount--;
            if(downloadingCount == 0)
            {
                loadMadeBoards();
            }
        };

        var postParams = new Dictionary<string, string>()
                {
                    { "filepath", serverFilePath }
                };

        rest.POST(IMAGE_DOWNLOAD2_URL, postParams, methodCall);
        Debug.Log("done posting");
    }

    public void getUserImages(int userId)
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            processGetImagesJSON(rest.Results);
        };

        var postParams = new Dictionary<string, string>()
                {
                    { "userId", userId.ToString() }
                };

        rest.POST(GET_IMAGES_URL, postParams, methodCall);
        Debug.Log("done posting");
    }


    public void processGetImagesJSON(string text)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed getUserImages  " + text);
        }
        else if (responseServer == "success")
        {
            string[] files = Directory.GetFiles(dir);    
            JSONArray imagesNode = root["message"].AsArray;
            //serverImages = new Dictionary<string, string>();
            List<string> filesArray = new List<string>(files);
            count = imagesNode.Count;
            for (int i = 0; i < imagesNode.Count; ++i)
            {
                string filePath = imagesNode[i]["filepath"].Value;
                string fileName = imagesNode[i]["filename"].Value;
                //serverImages.Add(fileName, filePath);
                if (!filesArray.Contains(dir+fileName))
                {
                    downloadImage(filePath, dir + fileName);
                    downloadingCount++;
                }
                Debug.Log("FileName: " + fileName);
            }

        }
        if (downloadingCount == 0)
        {
            loadMadeBoards();
        }
    }


}


