﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class ColorPickerScript : MonoBehaviour
{

    private Sprite spritePalette;
    private Image imagePalette;
    private Image colorShow;
    private Vector2 imageRectSize;
    private float reasonX, reasonY;
    private bool clicking = false;


    // Use this for initialization
    void Start()
    {
        colorShow = gameObject.transform.parent.transform.FindChild("ColorShow").gameObject.GetComponent<Image>();

        //Texture2D img = new Texture2D(1, 1);
        //byte[] image;
        //image = File.ReadAllBytes(Application.dataPath + "/Images/hsv_space.png");
        //img.LoadImage(image);
        Texture2D img = Resources.Load<Texture2D>("hsv_space");

        spritePalette = Sprite.Create(img, new Rect(0, 0, img.width, img.height), new Vector2(0.5f, 0.5f));
        imagePalette = gameObject.GetComponent<Image>();
        imagePalette.color = new Color(1, 1, 1, 1);
        imagePalette.sprite = spritePalette;

        RectTransform rect = imagePalette.GetComponent<RectTransform>();
        Vector3 s = rect.lossyScale;
        float w = rect.rect.width * s.x;
        float h = rect.rect.height * s.y;
        imageRectSize = new Vector2(w, h);
        reasonX = imageRectSize.x / spritePalette.texture.width;
        reasonY = imageRectSize.y / spritePalette.texture.height;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (clicking == false)
            {
                clicking = true;
            }
        }
        else
        {
            if (clicking == true)
            {
                clicking = false;
            }
        }
        if (clicking)
        {
            RectTransform rect = imagePalette.GetComponent<RectTransform>();
            Vector2 mousePosition = Input.mousePosition;
            float x = mousePosition.x - (rect.position.x - imageRectSize.x / 2);
            float y = /*imageRectSize.y - */mousePosition.y - (rect.position.y - imageRectSize.y / 2);
            x = x / reasonX;
            y = y / reasonY;
            int xi = Mathf.RoundToInt(x);
            int yj = Mathf.RoundToInt(y);
            if (xi >= 0 && xi < spritePalette.rect.width && yj >= 0 && yj < spritePalette.rect.height)
            {
                Color c = spritePalette.texture.GetPixel(xi, yj); ;
                colorShow.color = c;
                BoardCreateScript.selectedColor = c;
            }
        }
    }
}
