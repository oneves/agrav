﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DDA : MonoSingleton<DDA>
{
    [SerializeField]
    private float _timeBetweenEvaluations = 60;
    [SerializeField]
    private float _averageAccuracy = 0.5f;
    [SerializeField]
    private float _standardDeviation = 0.5f;

    [SerializeField]
    private float _playerLevel1HitboxSizeMult = 2;
    [SerializeField]
    private float _playerLevel1TorpedoSpeedMult = 2;
    [SerializeField]
    private float _playerLevel1TurretCDMult = 0.5f;
    [SerializeField]
    private float _playerLevel2HitboxSizeMult = 1.5f;
    [SerializeField]          
    private float _playerLevel2TorpedoSpeedMult = 1.5f;
    [SerializeField]          
    private float _playerLevel2TurretCDMult = 0.75f;
    [SerializeField]
    private float _playerLevel3HitboxSizeMult = 1;
    [SerializeField]          
    private float _playerLevel3TorpedoSpeedMult = 1;
    [SerializeField]          
    private float _playerLevel3TurretCDMult = 1;
    [SerializeField]
    private float _playerLevel4HitboxSizeMult = 0.75f;
    [SerializeField]          
    private float _playerLevel4TorpedoSpeedMult = 0.75f;
    [SerializeField]          
    private float _playerLevel4TurretCDMult = 1.5f;
    [SerializeField]
    private float _playerLevel5HitboxSizeMult = 0.5f;
    [SerializeField]          
    private float _playerLevel5TorpedoSpeedMult = 0.5f;
    [SerializeField]          
    private float _playerLevel5TurretCDMult = 2;

    private GamePlayer _player;
    private int _playerLevel = 3;
    private float _timeSinceLastEvaluation;
    
    public void SetPlayer(GamePlayer player, int playerLevel)
    {
        _player = player;
        _playerLevel = playerLevel;
    }

    private void Update()
    {
        if (_timeSinceLastEvaluation >= _timeBetweenEvaluations)
        {
            EvaluatePlayer();
        } else
        {
            _timeSinceLastEvaluation += Time.deltaTime;
        }
    }

    private void EvaluatePlayer()
    {
        if (_player == null) return;
        _timeSinceLastEvaluation = 0;

        int timesFired = _player.TimesFired;
        if (timesFired <= 0) return;

        int timesHit = _player.TimesHit;
        int timesMissed = timesFired - timesHit;

        float accuracy = (float)timesHit / (float)timesFired;

        int playerLevel = CalculatePlayerLevel(accuracy);

        if(playerLevel != _playerLevel)
        {
            UpdatePlayerLevel(playerLevel);
        }
    }

    private int CalculatePlayerLevel(float accuracy)
    {
        float doubleAverageAccuracy = 2 * _averageAccuracy;
        float doubleStandardDeviation = 2 * _standardDeviation;
        float amd = _averageAccuracy - _standardDeviation;
        float apd = _averageAccuracy + _standardDeviation;
        float amdd = _averageAccuracy - doubleStandardDeviation;
        float apdd = _averageAccuracy + doubleStandardDeviation;

        if (accuracy < amdd)
        {
            return 1;
        } else if (accuracy < amd)
        {
            return 2;
        } else if(accuracy < apd)
        {
            return 3;
        } else if(accuracy < apdd)
        {
            return 4;
        } else
        {
            return 5;
        }
    }

    private void UpdatePlayerLevel(int newLevel)
    {
        float currentHitboxSizeMult = GetHitboxSizeMultForLevel(_playerLevel);
        float currentTorpedoSpeedMult = GetTorpedoSpeedMultForLevel(_playerLevel);
        float currentTurretCDMult = GetTurretCDMultForLevel(_playerLevel);

        float newHitboxSizeMult = GetHitboxSizeMultForLevel(newLevel);
        float newTorpedoSpeedMult = GetTorpedoSpeedMultForLevel(newLevel);
        float newTurretCDMult = GetTurretCDMultForLevel(newLevel);

        Submarine submarine = TileMap.Instance.PlayerSubmarine;
        if (submarine == null) return;

        foreach(Unit unit in TileMap.Instance._units)
        {
            if(unit != null && unit.Type == Unit.UnitType.SUBMARINE && ((Submarine)unit) != submarine)
            {
                //Assuming that all colliders are circular, not null checking so we can instantly tell if one is not (exception will be thrown)
                CircleCollider2D collider = unit.GetComponent<CircleCollider2D>();
                collider.radius = (collider.radius / currentHitboxSizeMult) * newHitboxSizeMult;
            }
        }

        submarine.TorpedoSpeedMultiplier = newTorpedoSpeedMult;
        submarine.TurretCoolDownTime = (submarine.TurretCoolDownTime / currentTurretCDMult) * newTurretCDMult;

        _playerLevel = newLevel;
    }

    private float GetHitboxSizeMultForLevel(int level)
    {
        switch (level)
        {
            case 1:
                return _playerLevel1HitboxSizeMult;
            case 2:
                return _playerLevel2HitboxSizeMult;
            case 3:
                return _playerLevel3HitboxSizeMult;
            case 4:
                return _playerLevel4HitboxSizeMult;
            case 5:
                return _playerLevel5HitboxSizeMult;
            default:
                return 1;
        }
    }

    private float GetTorpedoSpeedMultForLevel(int level)
    {
        switch (level)
        {
            case 1:
                return _playerLevel1TorpedoSpeedMult;
            case 2:
                return _playerLevel2TorpedoSpeedMult;
            case 3:                 
                return _playerLevel3TorpedoSpeedMult;
            case 4:                 
                return _playerLevel4TorpedoSpeedMult;
            case 5:                 
                return _playerLevel5TorpedoSpeedMult;
            default:
                return 1;
        }
    }

    private float GetTurretCDMultForLevel(int level)
    {
        switch (level)
        {
            case 1:
                return _playerLevel1TurretCDMult;
            case 2:                 
                return _playerLevel2TurretCDMult;
            case 3:                 
                return _playerLevel3TurretCDMult;
            case 4:                 
                return _playerLevel4TurretCDMult;
            case 5:                 
                return _playerLevel5TurretCDMult;
            default:
                return 1;
        }
    }
}
