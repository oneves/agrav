﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using SimpleJSON;

public class DLCScript : MonoBehaviour {

    private const string ADD_USER_DLC_URL = "http://akatsua.ddns.net/uwars/addUserDLC";
    private const string GET_USER_DLC_URL = "http://akatsua.ddns.net/uwars/getUserDLC";

    public Button button1;
    public Button button2;
    public Text textBuyApply;
    private int selectedItem = 0;
    private bool[] items = new bool[]{ false, false, false, false};

    private string dir = Application.persistentDataPath + "/TorpedoSprites/";
    // Use this for initialization
    void Start () {
        getUserDLC();        
	}
	
	// Update is called once per frame
	void Update () {
        	
	}

    public void buyApplyClick()
    {
        if (selectedItem == 0) return;
        if (items[selectedItem-1])
        {
            generateBoardSides();
        }
        else
        {
            //get the img from db
            //store in  + id + ".png";
            addUserDLC(selectedItem);            
        }
    }

    public void clickBoard(int id)
    {
        selectedItem = id;
        if (items[selectedItem-1])
        {
            textBuyApply.text = "APLICAR";
        }
        else
        {
            textBuyApply.text = "COMPRAR";
        }
    }


    public void generateBoardSides()
    {
        Image img = gameObject.transform.FindChild("UI").gameObject.transform.FindChild("Conteudos").gameObject.transform.FindChild("Button" + (selectedItem)).gameObject.GetComponent<Image>();

        Sprite spriteBoard = Sprite.Create(img.sprite.texture, new Rect(), new Vector2());

        int w = spriteBoard.texture.width, h = spriteBoard.texture.height;
        bool searchingForBeggin = true;
        List<int> points = new List<int>(6);
        for (int x = 0; x < w; x++)
        {
            bool found = false;
            for (int y = 0; y < h; y++)
            {
                Color c = spriteBoard.texture.GetPixel(x, y);
                if (c.a != 0)
                {
                    found = true;
                    break;
                }
            }
            if (searchingForBeggin && found)
            {
                points.Add(x - 1);
                searchingForBeggin = false;
            }
            else
            {
                if (!searchingForBeggin && !found)
                {
                    points.Add(x + 2);
                    searchingForBeggin = true;
                }
            }
        }
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        Texture2D side1 = new Texture2D(points[1] - points[0], spriteBoard.texture.height);
        for (int i = points[0]; i < points[1]; i++)
        {
            for (int j = 0; j < side1.height; j++)
            {
                side1.SetPixel(i - points[0], j, spriteBoard.texture.GetPixel(i, j));
            }
        }
        byte[] bytes = side1.EncodeToPNG();
        string fileName = dir + "/side1.png";
        File.WriteAllBytes(fileName, bytes);

        Texture2D side2 = new Texture2D(points[5] - points[4], spriteBoard.texture.height);
        for (int i = points[4]; i < points[5]; i++)
        {
            for (int j = 0; j < side2.height; j++)
            {
                side2.SetPixel(i - points[4], j, spriteBoard.texture.GetPixel(i, j));
            }
        }
        bytes = side2.EncodeToPNG();
        fileName =dir + "side2.png";
        File.WriteAllBytes(fileName, bytes);
    }

    public void addUserDLC(int dlcId)
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            processAddUserDLCJSON(rest.Results, dlcId);
        };

        string userId = PreLoad.GetPlayer().userId;

        var postParams = new Dictionary<string, string>()
                {
                    { "userId", userId },
                    { "dlcId", dlcId.ToString() }
                };

        rest.POST(ADD_USER_DLC_URL, postParams, methodCall);
        Debug.Log("done posting");

    }
    private void processAddUserDLCJSON(string text, int id)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed addUserDLC  " + text);
        }
        else if (responseServer == "success")
        {
            items[id - 1] = true;
            gameObject.transform.FindChild("UI").gameObject.transform.FindChild("Conteudos").gameObject.transform.FindChild("Button" + (selectedItem)).gameObject.transform.FindChild("textPrice").GetComponent<Text>().text = "Comprado";
            textBuyApply.text = "APLICAR";
        }
    }

    public void getUserDLC()
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            processGetUserDLCJSON(rest.Results);
        };

        string userId = PreLoad.GetPlayer().userId;

        var postParams = new Dictionary<string, string>()
                {
                    { "userId", userId }
                };

        rest.POST(GET_USER_DLC_URL, postParams, methodCall);
        Debug.Log("done posting");

    }
    private void processGetUserDLCJSON(string text)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed getUserDLC  " + text);
        }
        else if (responseServer == "success")
        {
            JSONArray dlcIdNode = root["message"].AsArray;
            for (int i = 0; i < dlcIdNode.Count; ++i)
            {
                int dlcId = dlcIdNode[i]["dlcId"].AsInt;
                items[dlcId - 1] = true;
                Debug.Log("User has DLC: " + dlcId);
                gameObject.transform.FindChild("UI").gameObject.transform.FindChild("Conteudos").gameObject.transform.FindChild("Button" + (i + 1)).gameObject.transform.FindChild("textPrice").GetComponent<Text>().text = "Comprado";
            }
        }
    }

}
