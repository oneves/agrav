﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class GameSession : MonoBehaviour
{

    private const string FINISH_GAME_URL = "http://akatsua.ddns.net/uwars/finishGame";

    private static List<Team> _teams;
    public static int _teamIndex;
    public static bool _isOperator;
    public static bool _isBlind;

    public bool gameStarted = false;
    public bool gameFinished = false;


    //private TileMap _tileMap;

    public static List<Team> TeamList { get{ return _teams; } }
   
    // Use this for initialization
    public GameSession () {
        //loadTeams();
        //loadPlayerRole();
        //playerTeamIndex();
        //playerHandicap();

        //SceneManager.LoadScene("_Game_Operator.Side");

    }

    void Update()
    {
        GameObject [] submarines = GameObject.FindGameObjectsWithTag("Submarine");
        int countSubmarines = submarines.Length;
        int countAliveSubs = 0;
        foreach (var sub in submarines)
        {
            if (sub.GetComponent<Submarine>().IsAlive)
            {
                ++countAliveSubs;
            }
        }
        Debug.Log("Subs in game: " + countSubmarines + " | Subs still alive: " + countAliveSubs);

        if (!gameStarted && countSubmarines == PreLoad.numOfTeams)
        {
            gameStarted = true;
            gameFinished = false;
            Debug.Log("all players connected!");
        }

        if (gameStarted && countAliveSubs == 1)
        {
            StartCoroutine(finishGame2());
        }
    }

    public IEnumerator finishGame2()
    {
        if (PhotonNetwork.isMasterClient && PhotonNetwork.playerList.Length > 1)
        { 
            yield return new WaitForSeconds(1);
        }
        gameFinished = true;
        PhotonNetwork.LeaveRoom();
    }

    public void finishGame()
    {
        GamePlayer gamePlayer = TileMap.localPlayerRole == 0 ? (GamePlayer)TileMap.Instance.localTeam.TeamStrategist : TileMap.Instance.localTeam.TeamOperator;
        float score = gamePlayer.calculateScore();

        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            processFinishGameReturn(rest.Results, score);
        };

        Player player = PreLoad.GetPlayer();
        string userId = player.userId;


        var postParams = new Dictionary<string, string>()
                {
                    { "userId", userId },
                    { "score", score.ToString() }
                };

        rest.POST(FINISH_GAME_URL, postParams, methodCall);
        Debug.Log("done posting");
    }

    private void processFinishGameReturn(string text, float score)
    {

        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed finishGame  " + text);
        }
        else if (responseServer == "success")
        {
            GamePlayer gamePlayer = TileMap.localPlayerRole == 0 ? (GamePlayer)TileMap.Instance.localTeam.TeamStrategist : TileMap.Instance.localTeam.TeamOperator;
            GameStats stats = new GameStats();
            if (TileMap.localPlayerRole == 0)//strategist
            {
                Strategist strat = (Strategist)gamePlayer;
                stats.minesDeployed = strat.minesDropped;
            }
            stats.shotsFired = gamePlayer.TimesFired;
            stats.minesDestroyed = gamePlayer._minesDestroyed;
            stats.shotsHit = gamePlayer.TimesHit;
            stats.precision = gamePlayer.getAccuracy();
            stats.minesDestroyed = gamePlayer._minesDestroyed;
            stats.subsDestroyed = gamePlayer.killingBlows;
            stats.score = score;
            bool victory = TileMap.Instance.localTeam.submarine.IsAlive;

            VictoryUIScript.gameStats = stats;

            if (PreLoad.GetPlayer().type == PlayerType.BLIND)
            {
                SceneManager.LoadScene( victory ? "9.1_B_Victory" : "10.1_B_Defeat");
            }
            else if (PreLoad.GetPlayer().type == PlayerType.DEAF || PreLoad.GetPlayer().type == PlayerType.GENERAL)
            {
                SceneManager.LoadScene( victory ? "9.2_DG_Victory" : "10.2_DG_Defeat");
            }
        }
    }


    public void loadTeams()
    {
        _teams = new List<Team>();
        //DUMMY_DATA
        _teams.Add(new Team(new Submarine(), "Benjamim", "Tony"));
        _teams.Add(new Team(new Submarine(), "The Manel", "Chico Fininho"));
    }

    public void playerTeamIndex()
    {
        for (int i= 0;i< _teams.Count; i++)
        {
            
            Team team = _teams[i];
            if (_isOperator && team.TeamOperator.PlayerUserName == PreLoad.GetPlayer().name)
            {
                _teamIndex = i;
                break;
            }
            else if (!_isOperator && team.TeamStrategist.PlayerUserName == PreLoad.GetPlayer().name)
            {
                _teamIndex = i;
                break;
            }
        }
    }



    private void loadPlayerRole()
    {
        //DUMMY_DATA
        _isOperator=false; 
    }

    private void playerHandicap()
    {
        if (PreLoad.GetPlayer().type == PlayerType.BLIND)
            _isBlind = true;
        else
            _isBlind = false;
    }
    

    void updateData()
    {
    }
	
}
