﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System;
using System.Collections.Generic;

public class UIController : MonoBehaviour
{
    public bool isBlind = false;
    public bool isOperatorMode = true;

    private bool health_75 = false;
    private bool health_50 = false;
    private bool health_25 = false;

    Vector2 touchAnalogVector;
    UI_Actions uiActions;
    GameObject helpPanel;
    GameObject tileMap;

    private bool fire = false;
    float a;
    private float step = 5;
    private float x;
    private float y;
    private float xDumping = 0.5f;
    private float yDumping = 0.2f;
    private bool click = false;

    public AudioManager audio;

    public Team team;

    public bool leavingToMenu = false;



    public void setTeam(Team team)
    {
        this.team = team;
    }

    void Start()
    {
        HelpPanelSetActive(false);

        //PROVISORIO
        GameSession gameSession = new GameSession();


        //isOperatorMode=GameSession._isOperator;
        //isBlind = GameSession._isBlind;

        //team = GameSession.TeamList[GameSession._teamIndex];
        //team = TileMap.Instance.teams[0];

        GameObject am = GameObject.Find("AudioManager");
        if (am != null)
            audio = am.GetComponent<AudioManager>();
        team = TileMap.Instance.localTeam;

        isBlind = PreLoad.GetPlayer().type == PlayerType.BLIND;
        isOperatorMode = TileMap.localPlayerRole == 1;

        if (isBlind)
            uiActions = new UI_Actions_Blind(audio);
        else
            uiActions = new UI_Actions_DG();


        uiActions.init(this, isOperatorMode);
        uiActions.setCallbacks();

        int nrOfTeams = 2; //default
        GameObject obj = GameObject.Find("PreLoad");
        if (obj != null)
        {
            PreLoad pl = obj.GetComponent<PreLoad>();
            if (pl != null)
            {
                nrOfTeams = PreLoad.numOfTeams;
            }
        }
        initSonar(nrOfTeams);
    }


    public void sendPing(Vector2 pingCoord, string msgStrat)
    {
        // this method is called by TilePing
        // send ping to operator player
        UnityEngine.Debug.Log(pingCoord);
        team.submarine.sendPing(pingCoord, msgStrat, TileMap.localTeamId);
    }

    public void sendPing(Vector2 pingCoord, string msgStrat, int localTeamId)
    {
        // this method is called by TilePing
        // send ping to operator player
        team.submarine.sendPing(pingCoord, msgStrat, localTeamId);
    }

    public void receivePing(Vector2 pingCoord)
    {
        uiActions.pingSonar(pingCoord); //pings the operator's sonar
    }

    public void fireTorp()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("FIRE TORP");
        //setCombatInfo_damage(1);
        team.fireTorpedo();
    }
    public void fireTurret()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("FIRE TURRET");
        //setCombatInfo_damage(2);
        team.turretToogle();
    }

    public void dropMine()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("MINE PLACED");
        team.dropMine();
    }

    public void help()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("HELP");
        HelpPanelSetActive(true);
    }

    public void moveDown()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("MOVE DOWN");
        //setCombatInfo_damage(4);
        //if (isOperatorMode)
        //{

        //    team.speedSubmarine(-1*step);
        //}
        click = true;
        y = -1 * step;
    }

    public void moveDown2nd()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("SCROLL DOWN");
        setCombatInfo_damage(50);
    }

    public void moveLeft()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("MOVE LEFT");
        //setCombatInfo_damage(99);
        //setCombatInfo_damage_bar(99);

        //if (isOperatorMode)
        //{
        //    team.turnSubmarine(-1* step);
        //}
        //else
        //{
        //    team.turnTurret(-1* step);
        //}
        click = true;
        x = -1 * step;
    }

    public void moveRight()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("MOVE RIGHT");
        //setCombatInfo_damage(7);
        //if (isOperatorMode)
        //{
        //    team.turnSubmarine(step);
        //}
        //else
        //{
        //    team.turnTurret(step);
        //}
        click = true;
        x = step;
    }

    public void moveUp()
    {
        // this method is called by uiActions
        // send action to core
        // TODO
        print("MOVE UP");
        //setCombatInfo_damage(8);

        //if (isOperatorMode)
        //{
        //    team.speedSubmarine(step);
        //}
        click = true;
        y = step;

    }

    public void moveUp2nd()
    {
        // this method is called by uiActions
        // send action to core
        // TODO

        print("SCROLL UP");
        //setCombatInfo_damage(9);
    }

    public void toMenu()
    {
        // this method is called by uiActions

        print("TO MENU");
        leavingToMenu = true;
        PhotonNetwork.LeaveRoom();
    }





    Vector2 getAnalogVector()
    {
        return uiActions.getAnalogVector();
    }

    void setTitle_map(string mapName)
    {
        uiActions.setTitle_map(mapName);
    }

    public void setTitle_MsgStrat(string stratMsg)
    {
        uiActions.setTitle_MsgStrat(stratMsg);
    }

    void setCombatInfo_ammo(float ammoAmount)
    {
        if (TileMap.localPlayerRole != 0)
            uiActions.setCombatInfo_ammo(ammoAmount.ToString());
        else
            uiActions.setCombatInfo_ammo(team.submarine.MineCount.ToString());
    }

    void setCombatInfo_damage(int dmg)
    {
        if (dmg < 10)
            uiActions.setCombatInfo_damage("0" + dmg + "%");
        else
            uiActions.setCombatInfo_damage(dmg + "%");

        if (audio && isBlind)
            if (dmg < 76 && !health_75)
            {
                health_75 = true;
                audio.PlayVoice("health_75");
            }
            else if (dmg < 51 && !health_50)
            {
                health_50 = true;
                audio.PlayVoice("health_50");
            }
            else if (dmg < 25 && !health_25)
            {
                health_25 = true;
                audio.PlayVoice("health_25");
            }
    }

    void setCombatInfo_damage_bar(int dmg)
    {
        uiActions.setCombatInfo_damage_bar(dmg);
    }

    void setCombatInfo_dir(float degrees)
    {
        uiActions.setCombatInfo_dir(degrees + "º");
        uiActions.setCombatInfo_dirCompass(degrees);
    }

    void setMarineCurrentInfo(int wavesX, int wavesY, int dirX, int dirY, float str)
    {
        uiActions.setMarineCurrentInfo("" + wavesX, "" + wavesY, "" + dirX, "" + dirY, "" + str);
    }

    public void setMarineCurrentInfo(float dir, float str)
    {
        uiActions.setMarineCurrentInfo("" + dir, "" + str);
    }

    public void initSonar(int nrOfPlayers)
    {
        uiActions.initSonar(nrOfPlayers);
    }
    public void updateSonarInfo()
    {
        int unitCounter = TileMap.Instance._units.Count;
        List<Vector2> playersPos = new List<Vector2>();
        Unit a;
        for (int i = 0; i < unitCounter; i++)
        {
            a = TileMap.Instance._units[i];
            if (a.Type == Unit.UnitType.SUBMARINE)
            {
                if (!((Submarine)a).clientOwnsSub())
                    playersPos.Add(new Vector2(a.transform.position.x, a.transform.position.y));
            }
        }
        updateSonarInfo(playersPos, team.submarine.transform.position, team.submarine.transform.rotation.eulerAngles.z);
    }
    public void updateSonarInfo(List<Vector2> playersPos, Vector2 localPlayerPos, float playerAngl_degrees)
    {
        uiActions.feedSonarWithPlayersPos(playersPos, localPlayerPos, playerAngl_degrees);
    }

    public void fireAR(float deltaTime)
    {
        a = Input.acceleration.z;
        if (Math.Abs(a) > 1.8)
        {
            fire = true;
        }
    }

    void Update()
    {
        uiActions.update();
        touchAnalogVector = getAnalogVector();
        float aX = touchAnalogVector.x;
        float aY = touchAnalogVector.y;
        if (click)
            click = false;
        else
        {
            if (Math.Abs(aX) > Math.Abs(x)) x = aX;
            if (Math.Abs(aY) > Math.Abs(y)) y = aY;
        }
        if (team != null)
        {

            if (isOperatorMode)
            {
                updateSonarInfo();
                team.turnSubmarine(x);
                team.speedSubmarine(y);

                setCombatInfo_dir((int)team.submarine.transform.rotation.eulerAngles.z);
                setCombatInfo_ammo(team.submarine.TorpedoCount);
            }
            else
            {
                team.turnTurret(x);

                setCombatInfo_dir((int)team.submarine.TurretRotation);
                setCombatInfo_ammo(team.submarine.MineCount);
            }
            if (x < xDumping)
            {
                if (x > -xDumping)
                {
                    x = 0;
                }
                else
                {
                    x += xDumping;
                }
            }
            else
            {
                x -= xDumping;
            }
            if (y < yDumping)
            {
                if (y > -yDumping)
                {
                    y = 0;
                }
                else
                {
                    y += yDumping;
                }
            }
            else
            {
                y -= yDumping;
            }

            setCombatInfo_damage((int)team.submarine.Health);
            setCombatInfo_damage_bar((int)team.submarine.Health);

            float deltaTime = Time.deltaTime;

            fireAR(deltaTime);

            if (fire && isOperatorMode)
            {
                fireTorp();
                fire = false;
            }
            else if (fire && !isOperatorMode)
            {
                dropMine();
                fire = false;
            }
        }
    }

    internal void hearStatusInfo()
    {
        //play status info
        print("STATUS INFO!");
    }



    public void HelpPanelSetActive(bool isActive)
    {
        if (!helpPanel)
            helpPanel = GameObject.Find("Help");
        if (helpPanel)
        {
            helpPanel.SetActive(isActive);

            try
            {
                if (isBlind)
                {
                    if (audio)
                    {
                        if (isActive)
                            audio.PlayVoice("helpMenu_blind_strat");
                        else
                            audio.StopVoice("helpMenu_blind_strat");
                    }
                    GameObject.Find("Text_DG_Strat").SetActive(false);
                    GameObject.Find("Text_DG_Op").SetActive(false);
                }
                else
                {
                    if (isOperatorMode)
                    {
                        GameObject.Find("Text_B_Strat").SetActive(false);
                        GameObject.Find("Text_DG_Strat").SetActive(false);
                    }
                    else
                    {
                        GameObject.Find("Text_B_Strat").SetActive(false);
                        GameObject.Find("Text_DG_Op").SetActive(false);
                    }
                }
            }
            catch (Exception) { }
        }

        if (!tileMap)
            tileMap = GameObject.Find("MainTileMap");
        if (tileMap)
            tileMap.SetActive(!isActive);

    }





    public void startCoroutine_resetBlindBtn(float time, ref BlindButton target)
    {
        StartCoroutine(resetBlindBtn(time, target));
    }
    private IEnumerator resetBlindBtn(float time, BlindButton target)
    {
        yield return new WaitForSeconds(time);
        target.flag = false;
    }
}
