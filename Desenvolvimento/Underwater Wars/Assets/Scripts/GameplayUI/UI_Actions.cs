﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    interface UI_Actions
    {
        void init(UIController _ctrl, bool isOperator);

        void setCallbacks();


        UnityEngine.Vector2 getAnalogVector();

        void setTitle_map(string mapName);
        void setTitle_MsgStrat(string stratMsg);

        void setCombatInfo_ammo(string ammoAmount);
        void setCombatInfo_damage(string dmg);
        void setCombatInfo_damage_bar(int dmg);
        void setCombatInfo_dir(string dir);
        void setCombatInfo_dirCompass(float dir);

        void initSonar(int nrOfPlayers);
        void feedSonarWithPlayersPos(List<UnityEngine.Vector2> playersPos, UnityEngine.Vector2 localPlayerPos, float playerAngl_degrees);
        void pingSonar(UnityEngine.Vector2 pingPos);


        void setMarineCurrentInfo(string wavesX, string wavesY, string dirX, string dirY, string str);
        void setMarineCurrentInfo(string dir, string str);

        void update();
    }
}
