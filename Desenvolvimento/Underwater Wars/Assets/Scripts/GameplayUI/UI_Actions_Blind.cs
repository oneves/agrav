﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class BlindButton
{
    public Button btn;
    public bool flag; //false reads info | true executes the action
}

public class gameplayUIComponents_Blind
{
    //public BlindButton btn_menu = new BlindButton();
    public BlindButton btn_help = new BlindButton();

    public BlindButton touchAnalog_btn_up = new BlindButton();
    public BlindButton touchAnalog_btn_down = new BlindButton();
    public BlindButton touchAnalog_btn_left = new BlindButton();
    public BlindButton touchAnalog_btn_right = new BlindButton();
    public BlindButton scroll_up = new BlindButton();
    public BlindButton scroll_down = new BlindButton();
    public BlindButton btn_fire = new BlindButton();
   //public BlindButton btn_dropMine = new BlindButton();
   //public BlindButton btn_hearStatusInfo = new BlindButton();
    public touchAnalogControl touchAnalog_analogNip;
    public Text title_map;

    public GameObject tileMap;
}

public class UI_Actions_Blind : UI_Actions
{

    bool compAreLoaded = false;
    gameplayUIComponents_Blind c;
    BlindButton LAST_PRESSED;
    BlindSonar blindBeepSonar;


    UIController ctrl;
    bool isOperator;
    private AudioManager audio;

    public UI_Actions_Blind(AudioManager audio)
    {
        this.audio = audio;
    }

    void UI_Actions.init(UIController _ctrl, bool _isOperator)
    {
        LAST_PRESSED = new BlindButton();
        LAST_PRESSED.flag = false;

        c = new gameplayUIComponents_Blind();
        //c.btn_menu.flag = false;
        c.btn_help.flag = false;
        c.touchAnalog_btn_up.flag = false;
        c.touchAnalog_btn_down.flag = false;
        c.touchAnalog_btn_left.flag = false;
        c.touchAnalog_btn_right.flag = false;
        c.scroll_up.flag = false;
        c.scroll_down.flag = false;
        c.btn_fire.flag = false;
        //c.btn_dropMine.flag = false;
        //c.btn_hearStatusInfo.flag = false;


        ctrl = _ctrl;
        isOperator = _isOperator;

        try
        {
            GameObject uiDg = GameObject.Find("UI_dg");
            if (uiDg != null)
                uiDg.SetActive(false);

            c.tileMap = GameObject.Find("MainTileMap");

            if (isOperator)
            {
                GameObject uiStrat = GameObject.Find("UI_strat");
                if (uiStrat != null)
                    uiStrat.SetActive(false);

                if (c.tileMap != null)
                    c.tileMap.SetActive(false);
                //c.scroll_up.btn = GameObject.Find("scroll_up").GetComponent<Button>();
                //c.scroll_down.btn = GameObject.Find("scroll_down").GetComponent<Button>();
            }
            else
            {
                GameObject uiOp = GameObject.Find("UI_operator");
                if (uiOp != null)
                    uiOp.SetActive(false);

                c.btn_fire.btn = GameObject.Find("btn_fire").GetComponent<Button>();
                //c.btn_dropMine.btn = GameObject.Find("btn_dropMine").GetComponent<Button>();
                blindBeepSonar = GameObject.Find("BlindSonar").GetComponent<BlindSonar>();
            }

            c.title_map = GameObject.Find("title_map").GetComponent<Text>();

            //c.btn_menu.btn = GameObject.Find("btn_menu").GetComponent<Button>();
            c.btn_help.btn = GameObject.Find("btn_help").GetComponent<Button>();
            //c.btn_hearStatusInfo.btn = GameObject.Find("btn_hearStatusInfo").GetComponent<Button>();

            c.touchAnalog_btn_up.btn = GameObject.Find("touchAnalog_btn_up").GetComponent<Button>();
            c.touchAnalog_btn_down.btn = GameObject.Find("touchAnalog_btn_down").GetComponent<Button>();
            c.touchAnalog_btn_left.btn = GameObject.Find("touchAnalog_btn_left").GetComponent<Button>();
            c.touchAnalog_btn_right.btn = GameObject.Find("touchAnalog_btn_right").GetComponent<Button>();
            c.touchAnalog_analogNip = GameObject.Find("touchAnalog_analogNip").GetComponent<touchAnalogControl>();

            GameObject obj = GameObject.Find("Help");
            if (obj != null)
                obj.SetActive(false);

            compAreLoaded = true;
        }
        catch (Exception e01)
        {
            Debug.Log("UI_Actions_mobile, init(): Failed to load interface components\n" + e01);
            compAreLoaded = false;
        }
    }

    void UI_Actions.setCallbacks()
    {
        if (compAreLoaded)
        {
            if (isOperator)
            {
                //operator only buttons
            }
            else
            {
                c.btn_fire.btn.onClick.AddListener(() => fire());
                //c.btn_dropMine.btn.onClick.AddListener(() => dropMine());
            }

            c.touchAnalog_btn_up.btn.onClick.AddListener(() => moveUp());
            c.touchAnalog_btn_down.btn.onClick.AddListener(() => moveDown());
            c.touchAnalog_btn_left.btn.onClick.AddListener(() => moveLeft());
            c.touchAnalog_btn_right.btn.onClick.AddListener(() => moveRight());
            //c.scroll_up.btn.onClick.AddListener(() => moveUp2nd());
            //c.scroll_down.btn.onClick.AddListener(() => moveDown2nd());

            //c.btn_hearStatusInfo.btn.onClick.AddListener(() => hearStatusInfo());
            //c.btn_menu.btn.onClick.AddListener(() => toMenu());
            c.btn_help.btn.onClick.AddListener(() => help());
        }
    }


    private static bool blindBtnChecker(ref BlindButton btn, ref BlindButton last)
    {
        if (!btn.flag)
        {
            last.flag = false;
            last = btn;
            btn.flag = true;

            return false;
        }

        btn.flag = false;
        return true;
    }


    public void fire()
    {
        //extra blind specific stuff
        if (blindBtnChecker(ref c.btn_fire, ref LAST_PRESSED))
        {
            if (isOperator)
                ctrl.fireTorp();
            else
                ctrl.fireTurret();
        }
        else
        {
            ctrl.startCoroutine_resetBlindBtn(0.9f, ref c.btn_fire);
            if (audio != null)
                audio.PlayVoice(audio.Disparar);
        }
    }


    public void dropMine()
    {
        //extra blind specific stuff
        //if (blindBtnChecker(ref c.btn_dropMine, ref LAST_PRESSED))
        //{
        //    if (!isOperator)
        //        ctrl.dropMine();
        //}
        //else
        //{
        //    if (audio != null)
        //        audio.PlayVoice(audio.Minar);
        //}
    }

    public void help()
    {
        //extra blind specific stuff
        if (blindBtnChecker(ref c.btn_help, ref LAST_PRESSED))
        {
            ctrl.help();
            GameObject obj = GameObject.Find("Help");
            if (obj != null)
                obj.SetActive(true);
        }
        else
        {
            ctrl.startCoroutine_resetBlindBtn(0.9f, ref c.btn_help);
            if (audio != null)
                audio.PlayVoice(audio.Ajuda);
        }
    }

    public void moveDown()
    {
        //extra blind specific stuff
        if (blindBtnChecker(ref c.touchAnalog_btn_down, ref LAST_PRESSED))
        {
            ctrl.moveDown();
        }
        else
        {
            if (audio != null)
                audio.PlayVoice(audio.Sul);
        }
    }

    public void moveDown2nd()
    {
        //extra blind specific stuff
        if (blindBtnChecker(ref c.scroll_down, ref LAST_PRESSED))
        {
            ctrl.moveDown2nd();
        }
        else
        {
            if (audio != null)
                audio.PlayVoice(audio.Sul);
        }
    }

    public void moveLeft()
    {
        //extra blind specific stuff
        if (blindBtnChecker(ref c.touchAnalog_btn_left, ref LAST_PRESSED))
        {
            ctrl.moveLeft();
        }
        else
        {
            if (audio != null)
                audio.PlayVoice(audio.Oeste);
        }
    }

    public void moveRight()
    {
        //extra blind specific stuff
        if (blindBtnChecker(ref c.touchAnalog_btn_right, ref LAST_PRESSED))
        {
            ctrl.moveRight();
        }
        else
        {
            if (audio != null)
                audio.PlayVoice(audio.Este);
        }
    }

    public void moveUp()
    {
        //extra blind specific stuff
        if (blindBtnChecker(ref c.touchAnalog_btn_up, ref LAST_PRESSED))
        {
            ctrl.moveUp();
        }
        else
        {
            if (audio != null)
                audio.PlayVoice(audio.Norte);
        }
    }

    public void moveUp2nd()
    {
        //extra blind specific stuff
        if (blindBtnChecker(ref c.scroll_up, ref LAST_PRESSED))
        {
            ctrl.moveUp2nd();
        }
        else
        {
            if (audio != null)
                audio.PlayVoice(audio.Norte);
        }
    }

    public void toMenu()
    {
        ////extra blind specific stuff
        //if (blindBtnChecker(ref c.btn_menu, ref LAST_PRESSED))
        //{
        //    ctrl.toMenu();
        //}
        //else
        //{
        //    if (audio != null)
        //        audio.PlayVoice(audio.Menu);
        //}
    }

    public void hearStatusInfo()
    {
        ////extra blind specific stuff
        //if (blindBtnChecker(ref c.btn_hearStatusInfo, ref LAST_PRESSED))
        //{
        //    ctrl.hearStatusInfo();
        //}
        //else
        //{
        //    if (audio != null)
        //        audio.PlayVoice(audio.OuvirEstado);
        //}
    }

    void UI_Actions.setTitle_map(string mapName)
    {
        if (c.title_map != null)
            c.title_map.text = mapName.ToUpper();
    }

    void UI_Actions.setTitle_MsgStrat(string stratMsg)
    {
        //empty
    }

    void UI_Actions.setCombatInfo_ammo(string ammoAmount)
    {
        //empty
    }

    void UI_Actions.setCombatInfo_damage(string dmg)
    {
        //empty
    }

    void UI_Actions.setCombatInfo_damage_bar(int dmg)
    {
        //empty
    }

    void UI_Actions.setCombatInfo_dir(string dir)
    {
        //empty
    }

    void UI_Actions.setCombatInfo_dirCompass(float dir)
    {
        //empty
    }

    void UI_Actions.setMarineCurrentInfo(string wavesX, string wavesY, string dirX, string dirY, string str)
    {
        //empty
    }

    void UI_Actions.setMarineCurrentInfo(string dir, string str)
    {
        //empty
    }

    void UI_Actions.pingSonar(UnityEngine.Vector2 pingPos)
    {
        //empty
    }

    Vector2 UI_Actions.getAnalogVector()
    {
        if (c.touchAnalog_analogNip != null)
            return c.touchAnalog_analogNip.resultVec;
        return Vector2.zero;
    }

    void UI_Actions.feedSonarWithPlayersPos(List<Vector2> playersPos, Vector2 localPlayerPos, float playerAngl_degrees)
    {
        //empty
    }
    void UI_Actions.initSonar(int nrOfPlayers)
    {
        //empty
    }



    void UI_Actions.update()
    {
        if (blindBeepSonar && ctrl.team!=null) blindBeepSonar._submarine = ctrl.team.submarine;
    }
}
