﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;

struct gameplayUIComponents_DG
{
    public Button btn_menu;
    public Button btn_help;

    public Button touchAnalog_btn_up;
    public Button touchAnalog_btn_down;
    public Button touchAnalog_btn_left;
    public Button touchAnalog_btn_right;
    public Button scroll_up;
    public Button scroll_down;
    public Button btn_fire;
    public Button btn_dropMine;
    public touchAnalogControl touchAnalog_analogNip;

    public Text title_map;
    public Text title_MsgStrat;

    public Text combatInfo_ammo;
    public Text combatInfo_damage;
    public Text combatInfo_dir;

    public Text marineCurrentInfo;

    public Slider healthSlider;

    public Sonar sonar;

    public UICompass uiCompass;

    public GameObject tileMap;
}

public class UI_Actions_DG : UI_Actions
{

    bool compAreLoaded = false;
    gameplayUIComponents_DG c;
    UIController ctrl;
    bool isOperator;

    void UI_Actions.init(UIController _ctrl, bool _isOperator)
    {
        c = new gameplayUIComponents_DG();
        ctrl = _ctrl;
        isOperator = _isOperator;

        try
        {
            GameObject uiblind = GameObject.Find("UI_blind");
            if (uiblind != null)
                uiblind.SetActive(false);

            c.tileMap = GameObject.Find("MainTileMap");

            if (isOperator)
            {
                GameObject uiStrat = GameObject.Find("UI_strat");
                if (uiStrat != null)
                    uiStrat.SetActive(false);

                if (c.tileMap != null)
                {
                    //c.tileMap.SetActive(false);
                    var camera = GameObject.Find("Main Camera").GetComponent<Camera>();
                    int layer = 1 << 8;
                    camera.cullingMask = camera.cullingMask & ~layer;
                }
                
                //c.scroll_up = GameObject.Find("scroll_up").GetComponent<Button>();
                //c.scroll_down = GameObject.Find("scroll_down").GetComponent<Button>();
                c.title_MsgStrat = GameObject.Find("title_MsgStrat").GetComponent<Text>();
                c.marineCurrentInfo = GameObject.Find("marineCurrentInfo").GetComponent<Text>();

                c.sonar = GameObject.Find("Sonar").GetComponent<Sonar>();
            }
            else
            {
                GameObject uiOp = GameObject.Find("UI_operator");
                if (uiOp != null)
                    uiOp.SetActive(false);

                c.btn_dropMine = GameObject.Find("btn_dropMine").GetComponent<Button>();
            }

            c.btn_fire = GameObject.Find("btn_fire").GetComponent<Button>();
            c.healthSlider = GameObject.Find("healthSlider").GetComponent<Slider>();
            c.healthSlider.maxValue = 100;
            c.healthSlider.minValue = 0;

            c.combatInfo_ammo = GameObject.Find("combatInfo_ammo").GetComponent<Text>();
            c.combatInfo_damage = GameObject.Find("combatInfo_damage").GetComponent<Text>();
            c.combatInfo_dir = GameObject.Find("combatInfo_dir").GetComponent<Text>();
            c.title_map = GameObject.Find("title_map").GetComponent<Text>();

            c.btn_menu = GameObject.Find("btn_menu").GetComponent<Button>();
            c.btn_help = GameObject.Find("btn_help").GetComponent<Button>();

            c.touchAnalog_btn_up = GameObject.Find("touchAnalog_btn_up").GetComponent<Button>();
            c.touchAnalog_btn_down = GameObject.Find("touchAnalog_btn_down").GetComponent<Button>();
            c.touchAnalog_btn_left = GameObject.Find("touchAnalog_btn_left").GetComponent<Button>();
            c.touchAnalog_btn_right = GameObject.Find("touchAnalog_btn_right").GetComponent<Button>();
            c.touchAnalog_analogNip = GameObject.Find("touchAnalog_analogNip").GetComponent<touchAnalogControl>();

            c.uiCompass = GameObject.Find("Compass").GetComponent<UICompass>();


            GameObject obj = GameObject.Find("Help");
            if (obj != null)
                obj.SetActive(false);

            compAreLoaded = true;
        }
        catch (Exception e01)
        {
            Debug.Log("UI_Actions_mobile, init(): Failed to load interface components\n" + e01);
            compAreLoaded = false;
        }
    }

    void UI_Actions.setCallbacks()
    {
        if (compAreLoaded)
        {
            if (isOperator)
            {
                //operator only buttons
            }
            else
            {
                c.btn_dropMine.onClick.AddListener(() => dropMine());
            }

            c.btn_fire.onClick.AddListener(() => fire());
            c.touchAnalog_btn_up.onClick.AddListener(() => moveUp());
            c.touchAnalog_btn_down.onClick.AddListener(() => moveDown());
            c.touchAnalog_btn_left.onClick.AddListener(() => moveLeft());
            c.touchAnalog_btn_right.onClick.AddListener(() => moveRight());
            //c.scroll_up.onClick.AddListener(() => moveUp2nd());
            //c.scroll_down.onClick.AddListener(() => moveDown2nd());

            c.btn_menu.onClick.AddListener(() => toMenu());
            c.btn_help.onClick.AddListener(() => help());
        }
    }




    public void fire()
    {
        //extra deaf specific stuff
        if (isOperator)
            ctrl.fireTorp();
        else
            ctrl.fireTurret();
    }
    public void dropMine()
    {
        //extra deaf specific stuff
        if (!isOperator)
            ctrl.dropMine();
    }

    public void help()
    {
        //extra deaf specific stuff
        ctrl.help();
        GameObject obj = GameObject.Find("Help");
        if (obj != null)
            obj.SetActive(true);
    }

    public void moveDown()
    {
        //extra deaf specific stuff
        ctrl.moveDown();
    }

    public void moveDown2nd()
    {
        //extra deaf specific stuff
        ctrl.moveDown2nd();
    }

    public void moveLeft()
    {
        //extra deaf specific stuff
        ctrl.moveLeft();
    }

    public void moveRight()
    {
        //extra deaf specific stuff
        ctrl.moveRight();
    }

    public void moveUp()
    {
        //extra deaf specific stuff
        ctrl.moveUp();
    }

    public void moveUp2nd()
    {
        //extra deaf specific stuff
        ctrl.moveUp2nd();
    }

    public void toMenu()
    {
        //extra deaf specific stuff
        ctrl.toMenu();
    }

    void UI_Actions.setTitle_map(string mapName)
    {
        if (c.title_map != null)
            c.title_map.text = mapName.ToUpper();
    }

    void UI_Actions.setTitle_MsgStrat(string stratMsg)
    {
        if (c.title_MsgStrat != null)
            c.title_MsgStrat.text = "Mensagem Estratéga: " + stratMsg;
    }

    void UI_Actions.setCombatInfo_ammo(string ammoAmount)//should be ammoState but doesnt matter
    {
        if (c.combatInfo_ammo != null)
        {
            if (TileMap.localPlayerRole == 0)
                c.combatInfo_ammo.text = "Nr. Minas: " + ammoAmount;
            else
                c.combatInfo_ammo.text = "Nr. Torpedos: " + ammoAmount;
        }
    }

        void UI_Actions.setCombatInfo_damage(string dmg)
    {
        if (c.combatInfo_damage != null)
            c.combatInfo_damage.text = "Dano: " + dmg;
    }

    void UI_Actions.setCombatInfo_damage_bar(int dmg)
    {
        if (c.healthSlider != null)
            c.healthSlider.value = dmg;
    }

    void UI_Actions.setCombatInfo_dir(string dir)
    {
        if (c.combatInfo_dir != null)
            c.combatInfo_dir.text = "Orientação: " + dir;
    }

    void UI_Actions.setCombatInfo_dirCompass(float dir)
    {
        if (c.uiCompass != null)
            c.uiCompass.updateNeedle(dir);
    }

    void UI_Actions.setMarineCurrentInfo(string wavesX, string wavesY, string dirX, string dirY, string str)
    {
        if (c.marineCurrentInfo != null)
            c.marineCurrentInfo.text = "MARÉS - Ondas: " + wavesX + "," + wavesY + "; Corrente: ?; Direção: " + dirX + "," + dirY + "; Força: " + str;
    }

    void UI_Actions.setMarineCurrentInfo(string dir, string str)
    {
        if (c.marineCurrentInfo != null)
            c.marineCurrentInfo.text = "CORRENTE - Direção: " + dir + "º; Força: " + str;
    }

    Vector2 UI_Actions.getAnalogVector()
    {
        if (c.touchAnalog_analogNip != null)
            return c.touchAnalog_analogNip.resultVec;
        return Vector2.zero;
    }

    void UI_Actions.feedSonarWithPlayersPos(List<Vector2> playersPos, Vector2 localPlayerPos, float playerAngl_degrees)
    {
        if (c.sonar != null)
        {
            c.sonar.playerPos = localPlayerPos;
            c.sonar.targetPos = playersPos;
            c.sonar.playerAngl = playerAngl_degrees;
        }
    }

    void UI_Actions.initSonar(int nrOfPlayers)
    {
        if (c.sonar != null)
        {
            c.sonar.nrOfPlayers = nrOfPlayers;
            c.sonar.initDots();
        }
    }

    void UI_Actions.pingSonar(UnityEngine.Vector2 pingPos)
    {
        if (c.sonar != null)
            c.sonar.pingSonar(pingPos);
    }


    

    void UI_Actions.update()
    {

    }
}
