﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class touchAnalogControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public Vector2 resultVec;
    public RawImage nipContainer;
    public Camera MainCam;
    public UIController uiCtrl;

    bool mouseDown = false;
    bool analogInRestPos = false;
    float maxRadius;
    float lerpSpeed = 0.4f;
    public Vector3 initPos;
    public Vector3 point;

    // Use this for initialization
    void Start()
    {
        int i = 0;
        maxRadius = 1.2f*(float)nipContainer.rectTransform.sizeDelta.y / 2.0f;
        initPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        transform.position = initPos+Vector3.up;
    }


    public void OnPointerDown(PointerEventData ped)
    {
        if (!mouseDown && uiCtrl && uiCtrl.audio)
            uiCtrl.audio.PlayVoice("analogPressed");
        mouseDown = true;
    }
    public void OnPointerUp(PointerEventData ped)
    {
        if (mouseDown && uiCtrl && uiCtrl.audio)
            uiCtrl.audio.PlayVoice("analogReleased");
        mouseDown = false;
    }


    void OnMouseDrag()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!analogInRestPos && !mouseDown)
        {
            this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, initPos, lerpSpeed);
            if ((this.gameObject.transform.position - initPos).magnitude < 0.01f)
            {
                this.gameObject.transform.position = initPos;
                analogInRestPos = true;
            }
            resultVec = Vector2.zero;
        }
        else if (mouseDown)
        {
            point = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1000));
            point = new Vector3(point.x, point.y, transform.position.z);

            if (Vector3.Distance(point , initPos) < maxRadius)
            {
                transform.position = point;
                resultVec = ((point - initPos)) / maxRadius;
                analogInRestPos = false;
            }
            else
            {
                transform.position = (initPos + (point - initPos).normalized * maxRadius);
                resultVec = (point - initPos).normalized;
            }
        }
    }
}