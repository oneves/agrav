﻿using UnityEngine;
using System.Collections;

public class GeolocationService : MonoBehaviour
{

    private string log = "";
    public bool displayDebugLog = false;


    public bool coroutine_coordsFound = true;
    public double lat = 0;
    public double lon = 0;


    public void findCoords()
    {
        coroutine_coordsFound = false;
        StartCoroutine(_findCoords());
    }
    private IEnumerator _findCoords()
    {
        log += "checking gps...\n";
        if (!Input.location.isEnabledByUser)
            yield break;

        Input.location.Start();
        int maxWait = 20;
        log += "waiting for coords...\n";
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            log += maxWait + "\n";
            maxWait--;
        }

        if (maxWait < 1)
        {
            log += "Failed: Timed out\n";
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            log += "Failed: Unable to determine device location\n";
            yield break;
        }
        else
        {
            log += "Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp + "\n";
            lat = Input.location.lastData.latitude;
            lon = Input.location.lastData.longitude;
        }

        coroutine_coordsFound = true;
        Input.location.Stop();
    }


    void OnGUI()
    {
        if (displayDebugLog)
        {
            Debug.Log("GUI script running");
            float scalex = Screen.height / 1280.0f;
            float scaley = Screen.width / 720.0f;
            GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(scalex, scaley, 1));
            GUI.color = Color.black;

            GUI.Label(new Rect(20, 20, Screen.width, Screen.height), log);
        }
    }


}
