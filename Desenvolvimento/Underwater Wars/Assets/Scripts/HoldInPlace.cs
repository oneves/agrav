﻿using UnityEngine;
using System.Collections;

public class HoldInPlace : MonoBehaviour {

    [SerializeField]
    private Vector2 anchor;
    
    void Update()
    {
        float widthPos = Camera.main.pixelWidth * anchor.x;
        float heightPos = Camera.main.pixelHeight * anchor.y;
        Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(widthPos, heightPos, -Camera.main.transform.position.z));
        this.transform.position = position;
    }
}
