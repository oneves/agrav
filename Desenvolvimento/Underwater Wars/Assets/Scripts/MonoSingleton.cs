﻿using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour
    where T : MonoSingleton<T>
{
    #region Static Fields

    private static T instance;

    private static bool shuttingDown;

    #endregion

    #region Public Properties

    public static T Instance
    {
        get
        {
            if (shuttingDown)
            {
                return null;
            }

            if (instance == null)
            {
                instance = FindObjectOfType(typeof(T)) as T;
                if (instance == null)
                {
                    var gameObject = new GameObject(typeof(T).ToString());
                    instance = gameObject.AddComponent<T>();
                }
            }

            return instance;
        }
    }

    public static T InstanceWithoutCreate
    {
        get
        {
            return instance;
        }
    }

    #endregion

    #region Properties

    protected virtual bool IsPersistent
    {
        get
        {
            return false;
        }
    }

    #endregion

    #region Public Methods and Operators

    public static T Create()
    {
        return Instance;
    }

    #endregion

    #region Methods

    protected virtual void Awake()
    {
        if (instance == null)
        {
            instance = this as T;
            if (instance != null)
            {
                if (instance.IsPersistent)
                {
                    DontDestroyOnLoad(instance);
                }
            }
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    protected virtual void OnDestroy()
    {
        if (instance == null)
        {
            return;
        }

        if (instance.IsPersistent)
        {
            return;
        }

        instance = null;
    }

    private void OnApplicationQuit()
    {
        shuttingDown = true;
        instance = null;
    }

    #endregion
}