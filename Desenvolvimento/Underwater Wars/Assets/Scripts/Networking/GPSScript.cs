﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GPSScript
{
    public LocationInfo location = new LocationInfo();
    private bool started = false;
    public bool startLocationService()
    {
        if (started)
        {
            return true;
        }
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("GPS is not enabled");
            started = false;
            return false;
        }

        // Start service before querying location
        Input.location.Start();
        started = true;

        return true;
    }

    public bool updateLocation(GameObject label)
    {
        if (!started) return false;
        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            --maxWait;
        }

        if (maxWait < 1)
        {
            Debug.Log("Location Service Timed out");
            label.GetComponent<Text>().text = "Location Service Timed out";

            return false;
        }
        Debug.Log(Input.location.status);
        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Unable to determine device location");
            label.GetComponent<Text>().text = "Unable to determine device location";
            started = false;
            return false;
        }
        else
        {
            // Access granted and location value could be retrieved
            Debug.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
            location = Input.location.lastData;
            label.GetComponent<Text>().text = "Success";
            return true;
        }
        return false;
    }

    public void stopLocationService()
    {
        Input.location.Stop();
    }

}