﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;
using System.Collections;
using UnityEngine.UI;

namespace UnderwaterWars.Networking
{
    public class Launcher : Photon.PunBehaviour
    {
        private const string MATCHMAKING_URL = "http://akatsua.ddns.net/uwars/searchPlayers";
        
        /// <summary>
        /// The PUN loglevel. 
        /// </summary>
        public PhotonLogLevel loglevel = PhotonLogLevel.Informational;

        /// <summary>
        /// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
        /// </summary>   
        [Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
        public byte maxPlayersPerRoom = 20;

        [Tooltip("The UI Label to inform the user that the connection is in progress")]
        public GameObject progressLabel;

        [Tooltip("The Label to inform the user that there was a problem with matchmaking")]
        public GameObject matchmakingErrorLabel;

        /// <summary>
        /// This client's version number. Users are separated from each other by gameversion (which allows you to make breaking changes).
        /// </summary>
        private string gameVersion = "1";

        /// <summary>
        /// Keep track of the current process. Since connection is asynchronous and is based on several callbacks from Photon, 
        /// we need to keep track of this to properly adjust the behavior when we receive call back by Photon.
        /// Typically this is used for the OnConnectedToMaster() callback.
        /// </summary>
        private bool isConnecting;


        private bool isSearchingForRoom = false;
        private bool processedJSON = false;

        private GPSScript gps = new GPSScript();



        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        void Awake()
        {
            // #NotImportant
            // Force Full LogLevel
            PhotonNetwork.logLevel = loglevel;


            // #Critical
            // we don't join the lobby. There is no need to join a lobby to get the list of rooms.
            PhotonNetwork.autoJoinLobby = false;


            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.automaticallySyncScene = true;
        }


        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        void Start()
        {
            matchmakingErrorLabel.SetActive(true);
            progressLabel.SetActive(false);
            initGPS();
            //Connect();
        }

        private void initGPS()
        {
#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1)
            bool gpsEnabled = gps.startLocationService();
            if (gpsEnabled)
            {
                //matchmakingErrorLabel.SetActive(false);
            }
            else
            {
                matchmakingErrorLabel.GetComponent<Text>().text = "Por favor ligue o GPS";
                matchmakingErrorLabel.SetActive(true);
            }
#endif
        }

        void Update()
        {
            initGPS();
            if (isConnecting)
            {
                if (isSearchingForRoom)
                {
                    if (processedJSON)
                    {
                        processedJSON = false;
                        StartCoroutine(searchAfterDelay(1));
                    }
                    Debug.Log("Searching...");
                }
                else
                {
                    Debug.Log("Not Searching!");
                }
            }
        }

        /// <summary>
        /// Start the connection process. 
        /// - If already connected, we attempt joining a random room
        /// - if not yet connected, Connect this application instance to Photon Cloud Network
        /// </summary>
        public void Connect()
        {
            progressLabel.SetActive(true);
            progressLabel.GetComponent<Text>().text = "A procurar jogadores...";

            // keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
            isConnecting = true;

            // we check if we are connected or not, we join if we are, else we initiate the connection to the server.
            if (PhotonNetwork.connected)
            {
                // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnPhotonRandomJoinFailed() and we'll create one.
                //PhotonNetwork.JoinRandomRoom();
                //Debug.Log("CONNECT RANDOM ROOM");
                startMatchmaking();
            }
            else
            {
                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.ConnectUsingSettings(gameVersion);
                Debug.Log("CONNECT USING SETTINGS");
            }
        }

#region Photon overrides
        public override void OnConnectedToMaster()
        {
            Debug.Log("Launcher: OnConnectedToMaster() was called by PUN");

            // we don't want to do anything if we are not attempting to join a room. 
            // this case where isConnecting is false is typically when you lost or quit the game, when this level is loaded, 
            // OnConnectedToMaster will be called, in that case we don't want to do anything.
            if (isConnecting)
            {
                // This will be removed later:
                // #Critical: The first we try to do is to join a potential existing room. If there is, good, else, we'll be called back with OnPhotonRandomJoinFailed()
                //PhotonNetwork.JoinRandomRoom();

                startMatchmaking();

            }
        }

        public override void OnDisconnectedFromPhoton()
        {
            progressLabel.SetActive(false);
            Debug.LogWarning("Launcher: OnDisconnectedFromPhoton() was called by PUN");
        }

        public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
        {
            Debug.Log("Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.");

            // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
            PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = maxPlayersPerRoom }, null);
        }
               

        public override void OnJoinedRoom()
        {
            Debug.Log("Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");

            // #Critical: We only load if we are the first player, else we rely on  PhotonNetwork.automaticallySyncScene to sync our instance scene.
            if (PhotonNetwork.room.playerCount == 1)
            {
                Debug.Log("Loading room");
                // #Critical
                // Load the Room Level. 
                PhotonNetwork.LoadLevel("_Game_OperatorSide");
            }
        }

#endregion

        /// <summary>
        /// Sets the name of the player over the network
        /// </summary>
        /// <param name="value">The name of the Player</param>
        public void SetPlayerName(string value)
        {
            // #Important
            PhotonNetwork.playerName = value + " "; // force a trailing space string in case value is an empty string, else playerName would not be updated.
        }

        private Dictionary<string, string> getPostParams()
        {
            Player player = PreLoad.GetPlayer();
            string userId = player.userId;
            string deviceType = player.deviceType == DeviceType.COMPUTER ? "0" : "1";
            string latitude = "0";
            string longitude = "0";
            string playerType = player.type == PlayerType.GENERAL ? "0" : player.type == PlayerType.DEAF ? "1" : "2";
#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1)
            
            bool gpsSuccess = gps.updateLocation(matchmakingErrorLabel);
            Debug.Log(gpsSuccess ? "GPS Success" : "GPS Failure");
            
            if (gpsSuccess)
            {
                latitude = gps.location.latitude.ToString();
                longitude = gps.location.longitude.ToString();
            }

            //matchmakingErrorLabel.GetComponent<Text>().text = gpsSuccess ? "GPS Success" : "GPS Failure";
            //matchmakingErrorLabel.SetActive(true);
#endif

            string gameType = PreLoad.numOfTeams.ToString();

            var postParams = new Dictionary<string, string>()
                {
                    { "userId", userId },
                    { "deviceType", deviceType },
                    { "latitude", latitude },
                    { "longitude", longitude },
                    { "gameType", gameType },
                    { "playerType", playerType }                
                };
            return postParams;
        }

        /// <summary>
        /// Connects to the server to try and find a room
        /// </summary>
        private void searchForRoom()
        {
            RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
            System.Action methodCall = delegate ()
            {
                Debug.Log(rest.Results);
                processJSON(rest.Results);
            };

            var postParams = getPostParams();

            rest.POST(MATCHMAKING_URL, postParams, methodCall);
            Debug.Log("done posting");

        }

        private IEnumerator searchAfterDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            searchForRoom();
        }

        private void startMatchmaking()
        {
            isSearchingForRoom = true;
            searchForRoom();
        }

        private void processJSON(string text)
        {
               // matchmakingErrorLabel.GetComponent<Text>().text += text; 
            JSONNode root = JSON.Parse(text);
            string responseServer = root["type"];
            if (responseServer == "error")
            {
                Debug.Log("Failed searchForPlayers  " + text);
            }
            else if (responseServer == "success")
            {
              //  matchmakingErrorLabel.GetComponent<Text>().text += (string)root["message"];

                if ((string)root["message"] == "@no_players_close")
                {
                    Debug.Log("No players close");
                }
                else if ((string)root["message"] == "@no_computer_players")
                {
                    Debug.Log("No computer players available");
                }
                else if ((string)root["message"] == "@not_enough_players")
                {
                    Debug.Log("Not enough players");
                }
                else if ((string)root["message"] == "@no_rooms")
                {
                    Debug.Log("No rooms... this one shouldn't happen");
                }
                else if ((string)root["message"] == "@successfuly_updated_player_status")
                {
                    Debug.Log("I'm a PC player and my status was updated. Cool.");
                }
                else 
                {
                    isSearchingForRoom = false;
                    int team = root["message"]["team"].AsInt;
                    string room = root["message"]["room"].Value;
                    int role = root["message"]["role"].AsInt;
                    Debug.Log("Joining Room " + room + " in Team " + team + " as a " + role);
                    TileMap.localTeamId = team;
                    TileMap.localPlayerRole = role;
                    // Test
                    SetPlayerName("Room " + room + " in Team " + team + " as a " + role);

                    RoomOptions roomOptions = new RoomOptions();
                    roomOptions.IsVisible = true;
                    TypedLobby typedLobby = TypedLobby.Default;
                    PhotonNetwork.JoinOrCreateRoom(room, roomOptions, typedLobby);
                }
            }
            processedJSON = true;
        }
    }
}