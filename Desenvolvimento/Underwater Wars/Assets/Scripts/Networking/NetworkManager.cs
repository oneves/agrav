﻿using UnityEngine;
using UnityEngine.SceneManagement;


namespace UnderwaterWars.Networking
{
    public class NetworkManager : Photon.PunBehaviour
    {

        [Tooltip("The prefab to use for representing the player")]
        public GameObject playerPrefab;

        // Use this for initialization
        void Start()
        {
            if (playerPrefab == null)
            {
                Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
            }
            else
            {
                // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                if (PlayerNetworkingManager.LocalPlayerInstance == null)
                {
                    Debug.Log("We are Instantiating Player");
                    // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
                    PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
                }
                else
                {
                    Debug.Log("Ignoring scene load");
                }
            }
        }

        #region Photon overrides

        /// <summary>
        /// Called when the local player left the room. We need to load the launcher scene.
        /// </summary>
        public override void OnLeftRoom()
        {
            GameObject uiControllerObj = GameObject.Find("UIController");
            if (uiControllerObj != null)
            {
                UIController uiController = uiControllerObj.GetComponent<UIController>();
                if (uiController.leavingToMenu)
                {
                    uiController.leavingToMenu = false;
                    UIManager uiManager = uiControllerObj.GetComponent<UIManager>();
                    if (uiManager != null)
                    { 
                        uiManager.SetScene("MainMenu");
                    }
                }
            }

            GameObject gameSessionObj = GameObject.Find("GameSession");
            if (gameSessionObj != null)
            {
                GameSession gameSession = gameSessionObj.GetComponent<GameSession>();
                if (gameSession.gameFinished)
                {
                    gameSession.finishGame();
                }
            }
        }

        public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
        {
            Debug.Log("OnPhotonPlayerConnected() " + newPlayer.name); // not seen if you're the player connecting

            if (PhotonNetwork.isMasterClient)
            {
                Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected
                //LoadRoom();
            }
        }

        public override void OnPhotonPlayerDisconnected(PhotonPlayer other)
        {
            Debug.Log("OnPhotonPlayerDisconnected() " + other.name); // seen when other disconnects


            if (PhotonNetwork.isMasterClient)
            {
                Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected
                //LoadRoom();
            }
        }

        #endregion


        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        /// <summary>
        /// Loads the room over the network
        /// </summary>
        private void LoadRoom()
        {
            if (!PhotonNetwork.isMasterClient)
            {
                Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
            }
            Debug.Log("PhotonNetwork : Loading room for player count : " + PhotonNetwork.room.playerCount);
            PhotonNetwork.LoadLevel("_Game_OperatorSide");
        }


    }
}