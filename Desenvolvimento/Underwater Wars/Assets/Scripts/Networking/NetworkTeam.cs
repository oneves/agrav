﻿using UnityEngine;
using System.Collections;

namespace UnderwaterWars.Networking
{
    public class NetworkTeam
    {
        public static readonly string TeamPlayerProp = "";

        public int number = -1;

        public NetworkTeam()
        {
            number = -1;
        }

        public NetworkTeam(int number)
        {
            this.number = number;
        }

    }
}