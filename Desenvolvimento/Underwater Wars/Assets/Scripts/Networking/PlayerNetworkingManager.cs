﻿using UnityEngine;
using System.Collections;
using System;

namespace UnderwaterWars.Networking
{
    public class PlayerNetworkingManager : Photon.PunBehaviour, IPunObservable
    {
        [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
        public static GameObject LocalPlayerInstance;

        public int exampleValue = 1;


        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.isWriting)
            {
                // We own this player: send the others our data
                stream.SendNext(exampleValue);
            }
            else
            {
                // Network player, receive data
                this.exampleValue = (int)stream.ReceiveNext();
            }
        }

        void Awake()
        {
            // #Important
            // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
            if (photonView.isMine)
            {
                PlayerNetworkingManager.LocalPlayerInstance = this.gameObject;
            }
            // #Critical
            // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
            DontDestroyOnLoad(this.gameObject);
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (photonView.isMine == false && PhotonNetwork.connected == true)
            {
                return;
            }

            ++exampleValue;
            Debug.Log(PhotonNetwork.room.playerCount + " players in the room.");
        }
    }

}