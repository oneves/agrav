﻿using UnityEngine;
using ExitGames.Client.Photon;
using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace UnderwaterWars.Networking
{
    /// <summary>Extension used for PhotonPlayer class. Wraps access to the player's custom property.</summary>
    public static class TeamExtensions
    {
        /// <summary>Extension for PhotonPlayer class to wrap up access to the player's custom property.</summary>
        /// <returns>Team.none if no team was found (yet).</returns>
        public static NetworkTeam GetTeam(this PhotonPlayer player)
        {
            object teamId;
            if (player.customProperties.TryGetValue(NetworkTeam.TeamPlayerProp, out teamId))
            {
                return new NetworkTeam((int)teamId);
            }

            return new NetworkTeam();
        }

        /// <summary>Switch that player's team to the one you assign.</summary>
        /// <remarks>Internally checks if this player is in that team already or not. Only team switches are actually sent.</remarks>
        /// <param name="player"></param>
        /// <param name="team"></param>
        public static void SetTeam(this PhotonPlayer player, NetworkTeam team)
        {
            if (!PhotonNetwork.connectedAndReady)
            {
                Debug.LogWarning("SetTeam was called in state: " + PhotonNetwork.connectionStateDetailed + ". Not connectedAndReady.");
                return;
            }

            NetworkTeam currentTeam = player.GetTeam();
            if (currentTeam.number != team.number)
            {
                player.SetCustomProperties(new Hashtable() { { NetworkTeam.TeamPlayerProp, (byte)team.number } });
            }
        }
    }
}