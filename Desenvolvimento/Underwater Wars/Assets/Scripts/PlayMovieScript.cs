﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayMovieScript : MonoBehaviour {

#if UNITY_EDITOR || !(UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1)
    public MovieTexture movie;
    public RawImage img;
#endif
    bool playing = false;


    void Start () {
#if UNITY_EDITOR || !(UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1)
        img.texture = movie as MovieTexture;
#endif

    }

    public void playButton()
    {
#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1)
         StartCoroutine(PlayVideoCoroutine("subcutscene.mp4"));
#else
        playing = true;
        img.gameObject.SetActive(true);
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVideoSound(movie.audioClip);
        //audio = GetComponent<AudioSource>();
        //audio.clip = movie.audioClip;
        //audio.Play();
        movie.Play();
#endif

    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR || !(UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1)

        if (playing)
        {
            if (!movie.isPlaying)
            {
                img.gameObject.SetActive(false);
                playing = false;
                SceneManager.LoadScene("3.2_DG_GameIntro");
                GameObject.Find("AudioManager").GetComponent<AudioManager>().StopMusic();
                GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayMusic("music");
            }
        }
#endif

    }

#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1)

    IEnumerator PlayVideoCoroutine(string videoPath)
    {
        Handheld.PlayFullScreenMovie(videoPath);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        Debug.Log("Video playback completed.");

        SceneManager.LoadScene("3.2_DG_GameIntro");
        GameObject.Find("AudioManager").GetComponent<AudioManager>().StopMusic();
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayMusic("music");
    }
#endif
}
