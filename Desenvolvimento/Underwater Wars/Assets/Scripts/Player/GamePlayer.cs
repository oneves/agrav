﻿using UnityEngine;
using System.Collections;

public abstract class GamePlayer{
    private int assist = 0;


    public enum GamePlayerTypes
    {
        NONE,
        OPERATOR,
        STRATEGIST
    }

    public int _timesFired = 0;
    public int _timesHit = 0;
    public int _minesDestroyed = 0;
    public int killingBlows = 0;
    protected float hitAccuracy = 0;
    protected float damageDealt = 0;
    protected GamePlayerTypes _gamePlayerType = GamePlayerTypes.NONE;
    protected Submarine submarine;
    private string playerUser;
    private string user;

    public string PlayerUserName { get { return playerUser; } }
    public Submarine Submarine { get { return submarine; } }
    public int TimesFired { get { return _timesFired; } }
    public int TimesHit { get { return _timesHit; } }
    public int KillingBlows { get { return killingBlows; } }
    public float HitAccuracy { get { return hitAccuracy; } }
    public float DamageDealt { get { return damageDealt; } }
    public GamePlayerTypes GamePlayerType { get { return _gamePlayerType; } }

    public abstract float calculateScore();

    public GamePlayer(string user, Submarine _sub)
    {
        submarine = _sub;
        playerUser = user;

    }

    public GamePlayer(string user)
    {
        this.user = user;
    }

    public void addDamage(float damage)
    {
        damageDealt += damage;
        _timesHit++;
        UnityEngine.Debug.Log("damage dealt = " + damageDealt);
        UnityEngine.Debug.Log("torpedos fired = " + TimesFired);
        UnityEngine.Debug.Log("torpedos hit = " + TimesHit);
    }

    public float getAccuracy()
    {
        if(TimesFired == 0)
        {
            return 0F;
        }
        return ((float)TimesHit / TimesFired) * 100;
    }
}