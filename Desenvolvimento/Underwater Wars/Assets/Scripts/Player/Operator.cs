﻿using UnityEngine;
using System.Collections;
using System;



public class Operator:GamePlayer{
    private static int hitPoints = 3;
    private static int killPoints = 2;
    private static int mineDestructionPoints = 2;


    public Operator(string user,Submarine _sub) : base(user,_sub)
    {
        _gamePlayerType = GamePlayerTypes.OPERATOR;
        submarine.SetOperator(this);
    }


    public void fireTorpedo()
    {
        if (submarine.FireTorpedo())
            _timesFired++;
    }

    public void turnSumarine(float turn)
    {
        submarine.Rotate(turn);
    }

    public void speedSumarine(float speed)
    {
        submarine.Accelerate(speed);
    }

    
    public override float calculateScore()
    {
        float score = 0;
        score += TimesHit * hitPoints;
        score += KillingBlows * killPoints;
        score += _minesDestroyed * mineDestructionPoints;
        return score;
    }

}
