﻿using UnityEngine;
using System.Collections;
using System;

public class Strategist : GamePlayer {

    private static int hitPoints = 3;
    private static int killPoints = 2;
    private static int mineDestructionPoints = 2;
    private static int ownMineHitPoints = 3;

    public int minesDropped = 0;
    public int _ownMineDetonations = 0;

    public Strategist(string user, Submarine _sub) : base(user,_sub)
    {
        _gamePlayerType = GamePlayerTypes.STRATEGIST;
        submarine.SetStrategist(this);
    }



    public void dropMine()
    {
        if (submarine.FireMine())
            minesDropped++;
    }

    public void turretToogle()
    {
        submarine.ToggleTurret();
    }

    public void turnTurret(float turn)
    {
        submarine.RotateTurret(turn);
    }

    public override float calculateScore()
    {
        float score = 0;
        score += TimesHit * hitPoints;
        score += KillingBlows * killPoints;
        score += _minesDestroyed * mineDestructionPoints;
        score += _ownMineDetonations * ownMineHitPoints;
        return score;
    }

    public void addMineDamage(float damage)
    {
        damageDealt += damage;
        _ownMineDetonations++;
    }
}
