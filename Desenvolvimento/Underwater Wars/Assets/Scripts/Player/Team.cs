﻿    using UnityEngine;
using System.Collections;

public class Team {

    [SerializeField]
    public Submarine submarine;
    private Operator oper;
    private Strategist strat;

    public Operator TeamOperator { get { return oper; } }
    public Strategist TeamStrategist { get { return strat; } }

    public Team(Submarine sub, string op, string str)
    {
        submarine = sub;
        oper = new Operator(op,sub);
        strat = new Strategist(str, sub);
    }

    public void fireTorpedo()
    {
        oper.fireTorpedo();
    }
    public void dropMine()
    {
        strat.dropMine();
    }

    public void turretToogle()
    {
        strat.turretToogle();
    }

    public void turnSubmarine(float turn)
    {
        oper.turnSumarine(turn);
    }
    public void speedSubmarine(float speed)
    {
        oper.speedSumarine(speed);
    }

    public void turnTurret(float turn)
    {
        strat.turnTurret(turn);
    }

}
