﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class RESTClient : MonoBehaviour {

    private string results;
    private WWW wwwResult;

    public String Results
    {
        get
        {
            return results;
        }
    }

    public WWW WWWResults
    {
        get
        {
            return wwwResult;
        }
    }

    public WWW GET(string url, System.Action onComplete)
    {

        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www, onComplete));
        return www;
    }

    public WWW POST(string url, Dictionary<string, string> post, System.Action onComplete)
    {
        WWWForm form = new WWWForm();

        foreach (KeyValuePair<string, string> post_arg in post)
        {
            form.AddField(post_arg.Key, post_arg.Value);
        }

        WWW www = new WWW(url, form);

        StartCoroutine(WaitForRequest(www, onComplete));
        return www;
    }

    public WWW POSTBinaryData(string url, Dictionary<string, string> post, byte[] binaryData, string fileName, System.Action onComplete)
    {
        WWWForm form = new WWWForm();

        foreach (KeyValuePair<string, string> post_arg in post)
        {
            form.AddField(post_arg.Key, post_arg.Value);
        }

        form.AddBinaryData("image", binaryData, fileName);

        WWW www = new WWW(url, form);

        StartCoroutine(WaitForRequest(www, onComplete));
        return www;
    }

    private IEnumerator WaitForRequest(WWW www, System.Action onComplete)
    {
        yield return www;
        // check for errors
        if (www.error == null)
        {
            results = www.text;
            wwwResult = www;
            onComplete();
        }
        else
        {
            Debug.Log(www.text);
            Debug.Log(www.error);
        }
    }
}
