﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Sonar : MonoBehaviour
{


    class DOT
    {
        public float angl;
        public Image img;
        public PingSpriteCtrl ping; //optional
    }


    public PingSpriteCtrl ping;
    public SonarProximityAlert proximityAlert;

    private DOT _ping;

    public RawImage cone;
    public List<GameObject> gameObjDots;
    public Image dot;
    public Color dotColor;
    public Color dotColorTransp;
    public GameObject dotsContainer;
    public Vector3 playerPos;
    public float playerAngl = 0;

    public int nrOfPlayers = 10;
    public float sonarMaxVisibility = 2.5f;
    public int sonarRadius = 120;
    public float dotSize = 0.1f;
    public float coneAngle = 0;
    
    float coneSpeed = -200.0f;

    private List<DOT> dots;
    public UICompass sonarCompass;


    //TEST ONLY
    public List<Vector2> targetPos;
    void fillList()
    {
        nrOfPlayers = 2;
        targetPos = new List<Vector2>();
        targetPos.Add(new Vector2(8, 8));
        for (int i = 1; i < nrOfPlayers; i++)
        {
            int x = Random.Range(0, 8);
            int y = Random.Range(0, 8);
            targetPos.Add(new Vector2(3, 5));
        }
    }
    /////////////


    // Use this for initialization
    void Start()
    {

        initPing();

        //initDots();
        //TEST ONLY
        //fillList();
        ///////////
    }

    public void testPing()
    {
        pingSonar(Vector2.zero);
    }

    private void initPing()
    {
        _ping = new DOT();
        _ping.angl = 0;
        _ping.img = ping.img;
        _ping.ping = ping;
    }

    public void initDots()
    {
        targetPos = new List<Vector2>(nrOfPlayers);
        dots = new List<DOT>(nrOfPlayers);

        for (int i = 0; i < nrOfPlayers; i++)
        {

            GameObject obj = Instantiate<GameObject>(dot.gameObject);
            obj.name = "dot0" + (i + 1);
            obj.transform.SetParent(dotsContainer.transform);
            obj.transform.localPosition = Vector3.zero;
            Image img = obj.GetComponent<Image>();

            DOT d = new DOT();
            d.angl = 0;
            d.img = img;
            d.ping = null;
            d.img.color = new Color(dotColor.r, dotColor.g, dotColor.b, dotColor.a);

            dots.Add(d);
        }

        sonarRadius = (int)cone.rectTransform.sizeDelta.x / 2;
        dotSize = (int)dot.rectTransform.sizeDelta.x;
        dotColorTransp = new Color(dotColor.r, dotColor.g, dotColor.b, 0);
        dot.gameObject.SetActive(false);
    }

    Vector3 calcPositionInSonarMap(Vector3 pos, ref DOT dot)
    {
        Vector3 adjustedPos = new Vector3(pos.x, 9 - pos.y, pos.z);
        Vector3 adjustedPlayerPos = new Vector3(playerPos.x, 9 - playerPos.y, playerPos.z);
        float anglAdjustment = 180;

        Vector3 vec = ((adjustedPos - adjustedPlayerPos) / sonarMaxVisibility) * sonarRadius;
        if (dot.ping == null)
            dot.img.gameObject.transform.localPosition = RotatePointAroundPivot(vec, adjustedPlayerPos, playerAngl+ anglAdjustment);
        else
        {
            if (vec.magnitude > sonarRadius)
                dot.img.gameObject.transform.localPosition = RotatePointAroundPivot(vec, adjustedPlayerPos, playerAngl + anglAdjustment).normalized* sonarRadius;
            else
                dot.img.gameObject.transform.localPosition = RotatePointAroundPivot(vec, adjustedPlayerPos, playerAngl + anglAdjustment);
        }
        dot.angl = Quaternion.FromToRotation(Vector3.up, dot.img.gameObject.transform.localPosition).eulerAngles.z;

        return vec;
    }

    // Update is called once per frame
    void Update()
    {
        rotateCone_update();
        dotsPos_update();
        if (sonarCompass)
            sonarCompass.updateNeedle(playerAngl);
    }

    private void rotateCone_update()
    {
        cone.transform.Rotate(0, 0, coneSpeed * Time.deltaTime);
        coneAngle = cone.transform.rotation.eulerAngles.z;
    }
    private void dotsPos_update()
    {
        if (targetPos != null)
            for (int i = 0; i < targetPos.Count; i++)
            {
                DOT dot = dots[i];
                Vector3 vec = calcPositionInSonarMap(new Vector3(targetPos[i].x, targetPos[i].y, 0), ref dot);

                if ((int)dots[i].angl > (int)coneAngle - 3 && (int)dots[i].angl < (int)coneAngle + 3)
                {
                    dots[i].img.color = new Color(dotColor.r, dotColor.g, dotColor.b, 255);
                }
                else
                {
                    dots[i].img.color = Color.Lerp(dots[i].img.color, dotColorTransp, 15 * Time.deltaTime);// (Time.deltaTime*1000));
                }

                if (vec.magnitude > sonarRadius - dotSize)
                    dots[i].img.gameObject.SetActive(false);
                else
                    dots[i].img.gameObject.SetActive(true);
    


                if (proximityAlert)
                    proximityAlert.updateProximity((vec.magnitude* sonarMaxVisibility) / sonarRadius);
            }

        if (proximityAlert)
            if (proximityAlert.resetProximityCounter() == 0)
                proximityAlert.isInProximity = false;
    }




    public void pingSonar(Vector2 pos)
    {
        if (ping != null)
        {
            if (_ping.ping.isPlaying())
                _ping.ping.killAnim();

            Vector3 vec = calcPositionInSonarMap(new Vector3(pos.x, pos.y, 0), ref _ping);
            _ping.ping.playAnim();
        }
    }

    private Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, float angles)
    {
        Vector3 dir = point - pivot;
        dir = Quaternion.Euler(0, 0, angles) * dir;
        point = dir + pivot;
        return point;
    }

}

