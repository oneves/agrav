﻿using UnityEngine;
using SimpleJSON;
using System;

public class TideAPI : MonoSingleton<TideAPI>
{

    private const string URL = "http://api.worldweatheronline.com/premium/v1/marine.ashx?key=a6118a1d512f48469b903406161511&format=json";

    private float swellDir;
    private float swellStrength;

    public float Direction { get { return swellDir; } }
    public float Strength { get { return swellStrength; } }

    // Use this for initialization
    void Start () {
        //GetTideData((float)41.194564,(float) -8.6027957);
        //GetTideData(45, -2);
        float latitude = UnityEngine.Random.Range(-90f, 90f);
        float longitude = UnityEngine.Random.Range(-180f, 180f);
        GetTideData(latitude, longitude);
    }
	
	// Update is called once per frame
	void Update () {
	
	}



    public void GetTideData(float lat, float lon)
    {
        RESTClient rest;
        rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate(){
            System.DateTime time = System.DateTime.Now;
            print(time.Hour);
            var n = JSONNode.Parse(rest.Results);
            n = n["data"]["weather"][0]["hourly"][(int)Math.Round((double)time.Hour / 3)];
            swellDir = n["swellDir"].AsFloat;
            swellStrength = n["swellHeight_m"].AsFloat;


            print(n["time"]+" "+swellDir);
        };
        rest.GET(URL + "&q=" + lat + "," + lon, methodCall);
    }
}