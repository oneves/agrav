﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TideAPI))] 
public class TideApiToUiController : MonoBehaviour //Later might connect GPS to TideAPI 
{
    [SerializeField]
    private UIController _uiController;
    private TideAPI _tideApi;

	void Start () {
        _tideApi = TideAPI.Instance;
    }

    void Update()
    {
        if (_uiController != null)
        {
            _uiController.setMarineCurrentInfo(_tideApi.Direction, _tideApi.Strength);
        }
    }
}
