﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using SimpleJSON;

public class TileMap : MonoSingleton<TileMap>
{
    [SerializeField]
    private Transform _tileFolder;
    [SerializeField]
    private Transform _rockFolder;
    [SerializeField]
    private Transform _unitsFolder;

    [SerializeField]
    private GameObject _tilePrefab;
    [SerializeField]
    private GameObject _rockPrefab;
    [SerializeField]
    private GameObject _sideTilePrefab;
    [SerializeField]
    private GameObject _wallPrefab;

    [SerializeField]
    private GameObject _minePrefab;
    [SerializeField]
    private GameObject _submarinePrefab;
    [SerializeField]
    private GameObject _torpedoPrefab;

    [SerializeField]
    private int _xSize = 10;
    [SerializeField]
    private int _ySize = 10;
    [SerializeField]
    private Sprite[] _rowSprites;
    [SerializeField]
    private Sprite[] _columnSprites;
    [SerializeField]
    private float _maxDiffSpawn;

    public List<Unit> _units;

    public int XSize { get { return _xSize; } }
    public int YSize { get { return _ySize; } }

    public Team localTeam;
    public static int localTeamId = -1;
    public static int localPlayerRole = -1;


    public List<Team> teams;
    private Submarine _playerSubmarine;
    private Random rndRotation = new Random();
    private string[][] map;
    private List<Vector3> positions;

    public UIController uiCtrl;

    public Submarine PlayerSubmarine { get { return _playerSubmarine; } }
    void Awake()
    {

        Debug.Log("Poop created");


    }
    void Start()
    {
        teams = GameSession.TeamList;
        _units = new List<Unit>();

        int teamSize = 2;
        GameObject obj = GameObject.Find("PreLoad");
        if(obj != null)
        {
            PreLoad pl = obj.GetComponent<PreLoad>();
            if(pl != null)
            {
                teamSize = PreLoad.numOfTeams;
            }
        }
        
        int mapIndex = 0;
        switch (teamSize)
        {
            case 2:
                mapIndex = 0;
                break;
            case 5:
                mapIndex = 1;
                break;
            case 10:
                mapIndex = 2;
                break;
        }

        map = loadMap(mapIndex);
        InitTiles();
        InitWalls();
        //spawnTeams(teamSize);

        if (localPlayerRole == 1) //operator
        {
            spawnTeam(localTeamId);
        }

        // To force the UI Controller to only init after tilemap
        if (uiCtrl != null)
        { 
            uiCtrl.enabled = true;
        }

    }

    void Update()
    {
        UpdateUnitsList();
        UpdateUnitVisibility();
    }

    private string[][] loadMap(int mapIndex)
    {
        TextAsset targetFile = Resources.Load<TextAsset>("Maps");
        string jsonMapData = targetFile.text;
        var maps = JSONNode.Parse(jsonMapData);
        var mapInfo = maps[mapIndex];
        _ySize = int.Parse(mapInfo["lenght"]);
        _xSize = int.Parse(mapInfo["width"]);
        string[][] map = new string[YSize][];
        for (int y = 0; y < YSize; y++)
        {
            string line = mapInfo["map"][y];
            map[y] = line.Split(' ');
        }


        return map;
    }

    public void setStrategistTeam(Submarine submarine)
    {
        _playerSubmarine = submarine;
        localTeam = new Team(_playerSubmarine, "oper", "strat");
        uiCtrl.setTeam(localTeam);
    }

    public void spawnTeam(int teamIndex)
    {
        int numTeams = PreLoad.numOfTeams;
        //List<Vector3> positions = generatePositions(numTeams);
        List<Quaternion> rotations = generateRotations(numTeams);
        _playerSubmarine = (Submarine)SpawnUnit(Unit.UnitType.SUBMARINE, positions[teamIndex], rotations[teamIndex]);
        localTeam = new Team(_playerSubmarine, "oper", "strat");
    }

    public void spawnTeams(int numTeams)
    {
        teams = new List<Team>();
        //List<Vector3> positions = generatePositions(numTeams);
        List<Quaternion> rotations = generateRotations(numTeams);
        for (int i = 0; i < numTeams; i++)
        {
            Submarine sub = (Submarine)SpawnUnit(Unit.UnitType.SUBMARINE, positions[i], rotations[i]);
            //Submarine sub = (Submarine)SpawnUnit(Unit.UnitType.SUBMARINE, new Vector3(9,9), new Quaternion(15.3f, 15.3f, 0, 0));
            //sub.transform.position = new Vector3(0, 0, 0);
            //sub.transform.rotation = new Quaternion(0, 0, 0, 0);
            teams.Add(new Team(sub, "oper", "strat"));
            if (i == 0)
            {
                _playerSubmarine = sub;
            }
        }
        //GameObject btns = gameObject.transform.parent.transform.FindChild("Buttons").gameObject;
        //Button btn = btns.transform.FindChild("Accel").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].speedSubmarine(1.0f));
        //btn = btns.transform.FindChild("Brake").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].speedSubmarine(-1.0f));
        //btn = btns.transform.FindChild("Right").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].turnSubmarine(-1F));
        //btn = btns.transform.FindChild("Left").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].turnSubmarine(1F));
        //btn = btns.transform.FindChild("Right T").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].turnTurret(-1F));
        //btn = btns.transform.FindChild("Left T").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].turnTurret(1F));
        //btn = btns.transform.FindChild("Fire Mine").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].dropMine());
        //btn = btns.transform.FindChild("Fire Torpedo").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].fireTorpedo());
        //btn = btns.transform.FindChild("Fire Machine").gameObject.GetComponent<Button>();
        //btn.onClick.AddListener(() => teams[0].turretToogle());

    }

    //Position is relative to TileMap
    public Unit SpawnUnit(Unit.UnitType type, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion())
    {
        Unit obj = null;
        switch (type)
        {
            case Unit.UnitType.MINE:
                obj = ((GameObject)Instantiate(_minePrefab, position + transform.position, rotation, _unitsFolder)).GetComponent<Unit>();
                break;
            case Unit.UnitType.SUBMARINE:
                GameObject sub;
                if (PhotonNetwork.connected)
                {
                    object[] data = new object[1];
                    data[0] = localTeamId;

                    sub = PhotonNetwork.Instantiate(_submarinePrefab.name, position + transform.position, rotation,0, data);
                }
                else
                {
                    sub = (GameObject)Instantiate(_submarinePrefab, position + transform.position, rotation, _unitsFolder);
                }
                DontDestroyOnLoad(sub);
                obj = sub.GetComponent<Unit>();

                break;
            case Unit.UnitType.TORPEDO:
                obj = ((GameObject)Instantiate(_torpedoPrefab, position + transform.position, rotation, _unitsFolder)).GetComponent<Unit>();
                break;
            default:
                obj = null;
                break;
        }
        if (obj != null)
        {
            _units.Add(obj);
        }
        return obj;
    }

    private void InitTiles()
    {
        BoxCollider2D objcollider;
        positions = new List<Vector3>();
        for (int y = 0; y < _ySize; y++)
        {
            GameObject obj = (GameObject)Instantiate(_sideTilePrefab, _tileFolder);
            obj.transform.position = new Vector2(transform.position.x - 1, transform.position.y + _ySize - 1 - y);
            if (y < _rowSprites.Length) obj.GetComponentInChildren<SpriteRenderer>().sprite = _rowSprites[y];
            for (int x = 0; x < _xSize; x++)
            {
                if (map[y][x] == "0")
                {
                    obj = (GameObject)Instantiate(_tilePrefab, _tileFolder);
                    TilePing pingScrpt = obj.AddComponent<TilePing>();
                    pingScrpt.pos = new Vector2(x, y);
                    pingScrpt.UICtrl = uiCtrl;
                }
                else if (map[y][x] == "s")
                {
                    obj = (GameObject)Instantiate(_tilePrefab, _tileFolder);
                    TilePing pingScrpt = obj.AddComponent<TilePing>();
                    pingScrpt.pos = new Vector2(x, y);
                    pingScrpt.UICtrl = uiCtrl;
                    generatePosition(x, _ySize - 1 - y);
                }
                else
                {
                    obj = (GameObject)Instantiate(_rockPrefab, _rockFolder);
                    objcollider = obj.GetComponent<BoxCollider2D>();
                    objcollider.size = new Vector2(1, 1);
                }
                obj.transform.position = new Vector2(transform.position.x + x, transform.position.y + _ySize - 1 - y);
            }
        }
        for (int x = 0; x < _xSize; x++)
        {
            GameObject obj = (GameObject)Instantiate(_sideTilePrefab, _tileFolder);
            obj.transform.position = new Vector2(transform.position.x + x, transform.position.y + _ySize);
            if (x < _columnSprites.Length) obj.GetComponentInChildren<SpriteRenderer>().sprite = _columnSprites[x];
        }
    }

    //private void InitTiles()
    //{
    //    for (int y = 0; y < _ySize; y++)
    //    {
    //        GameObject obj = (GameObject)Instantiate(_sideTilePrefab, _tileFolder);
    //        obj.transform.position = new Vector2(transform.position.x - 1, transform.position.y + y);
    //        if (y < _rowSprites.Length) obj.GetComponentInChildren<SpriteRenderer>().sprite = _rowSprites[y];
    //        for (int x = 0; x < _xSize; x++)
    //        {
    //            obj = (GameObject)Instantiate(_tilePrefab, _tileFolder);
    //            obj.transform.position = new Vector2(transform.position.x + x, transform.position.y + y);
    //        }
    //    }
    //    for (int x = 0; x < _xSize; x++)
    //    {
    //        GameObject obj = (GameObject)Instantiate(_sideTilePrefab, _tileFolder);
    //        obj.transform.position = new Vector2(transform.position.x + x, transform.position.y + _ySize);
    //        if (x < _columnSprites.Length) obj.GetComponentInChildren<SpriteRenderer>().sprite = _columnSprites[x];
    //    }
    //}

    private void InitWalls()
    {
        Vector2 horizontalSize = new Vector2(_xSize + 1, 1);
        Vector2 verticalSize = new Vector2(1, _ySize + 1);
        float halfX = _xSize * 0.5f;
        float halfY = _ySize * 0.5f;
        GameObject wall = (GameObject)Instantiate(_wallPrefab, _tileFolder);
        wall.transform.position = new Vector2(transform.position.x - 1, transform.position.y + halfY);
        BoxCollider2D wallCollider = wall.GetComponent<BoxCollider2D>();
        wallCollider.size = verticalSize;

        wall = (GameObject)Instantiate(_wallPrefab, _tileFolder);
        wall.transform.position = new Vector2(transform.position.x + _xSize, transform.position.y + halfY - 1);
        wallCollider = wall.GetComponent<BoxCollider2D>();
        wallCollider.size = verticalSize;

        wall = (GameObject)Instantiate(_wallPrefab, _tileFolder);
        wall.transform.position = new Vector2(transform.position.x + halfX - 1, transform.position.y - 1);
        wallCollider = wall.GetComponent<BoxCollider2D>();
        wallCollider.size = horizontalSize;

        wall = (GameObject)Instantiate(_wallPrefab, _tileFolder);
        wall.transform.position = new Vector2(transform.position.x + halfX, transform.position.y + _ySize);
        wallCollider = wall.GetComponent<BoxCollider2D>();
        wallCollider.size = horizontalSize;
    }

    private void UpdateUnitsList()
    {
        List<Unit> toRemove = new List<Unit>();
        Unit[] unitList = _unitsFolder.GetComponentsInChildren<Unit>();
        foreach (Unit unit in unitList)
        {
            if (!_units.Contains(unit))
            {
                _units.Add(unit);
            }
        }
        foreach (Unit unit in _units)
        {
            if (unit == null || (!unit.IsAlive && !unit.IsPlayingSound)) toRemove.Add(unit);
        }
        foreach (Unit unit in toRemove)
        {
            if (!unit.IsAlive) Destroy(unit.gameObject);
            _units.Remove(unit);
        }
    }

    private void UpdateUnitVisibility()
    {
        foreach (Unit unit in _units)
        {
            if (localPlayerRole == 1)
            {
                unit.IsVisible = false;
            }
            else
            {
                if (unit.IsForcedVisible || (unit.Type == Unit.UnitType.SUBMARINE && ((Submarine)unit) == _playerSubmarine)) continue;
                float distance = (unit.transform.position - _playerSubmarine.transform.position).magnitude;
                unit.IsVisible = (distance <= _playerSubmarine.SightRange);
            }
        }
    }
    //private List<Vector3> generatePositions(int num)
    //{
    //    List<Vector3> positions = new List<Vector3>();
    //    bool diff;
    //    while (num > 0)
    //    {
    //        diff = false;
    //        while (!diff)
    //        {
    //            float r1 = Random.Range(0, _xSize - 1);
    //            float r2 = Random.Range(0, _ySize - 1);
    //            diff = true;
    //            foreach (Vector3 pos in positions)
    //            {
    //                if (Mathf.Abs(pos.x - r1) < _maxDiffSpawn || Mathf.Abs(pos.y - r2) < _maxDiffSpawn)
    //                {
    //                    diff = false;
    //                    break;
    //                }
    //            }
    //            if (diff)
    //                positions.Add(new Vector3(r1, r2));
    //        }
    //        num--;
    //    }
    //    return positions;
    //}

    private void generatePosition(int x, int y)
    {
        float r1 = (float)x;
        float r2 = (float)y;
        positions.Add(new Vector3(r1, r2));
    }

    public List<Quaternion> generateRotations(int num)
    {
        List<Quaternion> rotations = new List<Quaternion>();
        while (num > 0)
        {
            Quaternion quat = new Quaternion();
            quat.eulerAngles = new Vector3(0, 0, Random.Range(0, 360));
            rotations.Add(quat);
            num--;
        }
        return rotations;
    }

    private bool checkGameWinningConditions()
    {
        int aliveCount = 0;
        int winnerID = -1;
        for(int i = 0; i< teams.Count;i++)
        {
            if (teams[i].submarine.IsAlive)
            {
                aliveCount++;
                winnerID = i;
            }
        }
        if (aliveCount > 1)
        {
            if(aliveCount == 0)
            {
                //draw? it can only happen if the last submarines take lethal dmg in the last frame
            }
            return true;
        }
        return false;
    }
}
