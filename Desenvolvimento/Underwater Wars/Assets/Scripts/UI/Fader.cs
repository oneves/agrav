﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fader : MonoBehaviour
{

    #region Variables

    public RawImage FadeInPanel;
    public RawImage FadeOutPanel;
    
    public float FadeSpeed = 0.3f;

    public string NextScene;
    
    private bool sceneStarting = true;

    #endregion

    void Update()
    {
        if (sceneStarting)
        {
            FadeIn();
        }
        else if (!sceneStarting)
        {
            FadeOut();
        }

        if (Input.GetKeyDown("space"))
            SceneManager.LoadScene(NextScene);

    }

    #region "Private Methods - Faders"

    void FadeIn()
    {
        Color imageColor = FadeInPanel.color;
        imageColor.a = FadeInPanel.color.a - FadeSpeed * (float)Time.deltaTime;
        FadeInPanel.color = imageColor;

        if (FadeInPanel.color.a <= 0.01f)
        {
            FadeInPanel.enabled = false;
            sceneStarting = false;
        }
    }

    void FadeOut()
    {
        Color imageColor = FadeOutPanel.color;
        imageColor.a = FadeOutPanel.color.a + FadeSpeed * (float)Time.deltaTime;
        FadeOutPanel.color = imageColor;

        if (FadeOutPanel.color.a >= 0.95f)
        {
            SceneManager.LoadScene(NextScene);
        }
    }

    #endregion
}
