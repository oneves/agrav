﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnderwaterWars.Networking;

public class GameIntroScript : MonoBehaviour
{

    private struct comb
    {
        public string name;
        public bool touch;
    }

    private List<comb> touched;

    public Button btn2;
    public Button btn5;
    public Button btn10;

    public GameObject btn1Img;
    public GameObject btn2Img;
    public GameObject btn3Img;

    public GameObject m2;
    public GameObject m5;
    public GameObject m10;

    public Text textMap;

    private struct btncomb
    {
        public string name;
        public Button btn;
    }

    private List<btncomb> btns;
    private int lastIndex = -1;

    void Start()
    {
        if (PreLoad.GetPlayer().type != PlayerType.BLIND)
        {
            btn1Img.gameObject.SetActive(true);
            btn2Img.gameObject.SetActive(false);
            btn3Img.gameObject.SetActive(false);

            m5.gameObject.SetActive(false);
            m10.gameObject.SetActive(false);
            m2.gameObject.SetActive(true);
        }
        else
        {
            touched = new List<comb>();
            touched.Add(new comb { name = "back", touch = false });
            touched.Add(new comb { name = "play", touch = false });
            touched.Add(new comb { name = "2", touch = false });
            touched.Add(new comb { name = "5", touch = false });
            touched.Add(new comb { name = "10", touch = false });

            btns = new List<btncomb>();
            btns.Add(new btncomb() { name = "2", btn = btn2 });
            btns.Add(new btncomb() { name = "5", btn = btn5 });
            btns.Add(new btncomb() { name = "10", btn = btn10 });
        }

        PreLoad.numOfTeams = 2;
        lastIndex = 0;
    }

    public void touch(string name)
    {
        for (int i = 0; i < touched.Count; i++)
        {
            if (touched[i].name.Equals(name))
            {
                if (!touched[i].touch)
                {
                    touched[i] = new comb { name = touched[i].name, touch = true };

                    GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(name);

                    for (int j = 0; j < touched.Count; j++)
                    {
                        if (!touched[j].name.Equals(name))
                        {
                            touched[j] = new comb { name = touched[j].name, touch = false };
                        }
                    }
                }
                else
                {
                    if (touched[i].name.Equals("play"))
                    {
                        //UnityEngine.SceneManagement.SceneManager.LoadScene("_Game");
                        GameObject.Find("Launcher").GetComponent<Launcher>().Connect();
                    }
                    if (touched[i].name.Equals("back")) GameObject.Find("UIManager").GetComponent<UIManager>().SetScene("MainMenu");
                }

                break;
            }
        }



    }

    public void select(int nr)
    {
        if (nr == 2)
        {
            PreLoad.numOfTeams = 2;

            if (PreLoad.GetPlayer().type != PlayerType.BLIND)
            {
                btn2Img.gameObject.SetActive(false);
                btn3Img.gameObject.SetActive(false);
                btn1Img.gameObject.SetActive(true);
                foreach (Text t in textMap.GetComponentsInChildren<Text>())
                {
                    t.text = "MAPA: NORMANDY";
                }
                m5.gameObject.SetActive(false);
                m10.gameObject.SetActive(false);
                m2.gameObject.SetActive(true);
            }

        }
        if (nr == 5)
        {
            PreLoad.numOfTeams = 5;

            if (PreLoad.GetPlayer().type != PlayerType.BLIND)
            {
                btn1Img.gameObject.SetActive(false);
                btn3Img.gameObject.SetActive(false);
                btn2Img.gameObject.SetActive(true);
                foreach (Text t in textMap.GetComponentsInChildren<Text>())
                {
                    t.text = "MAPA: ARTIC";
                }
                m5.gameObject.SetActive(true);
                m10.gameObject.SetActive(false);
                m2.gameObject.SetActive(false);
            }
        }
        if (nr == 10)
        {
            PreLoad.numOfTeams = 10;


            if (PreLoad.GetPlayer().type != PlayerType.BLIND)
            {
                btn1Img.gameObject.SetActive(false);
                btn2Img.gameObject.SetActive(false);
                btn3Img.gameObject.SetActive(true);
                foreach (Text t in textMap.GetComponentsInChildren<Text>())
                {
                    t.text = "MAPA: HAWAII";
                }
                m5.gameObject.SetActive(false);
                m10.gameObject.SetActive(true);
                m2.gameObject.SetActive(false);
            }
        }
    }
}
