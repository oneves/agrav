﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Header : MonoBehaviour
{

    #region Variables

    private string nameLabel = "     {0}";
    private string pointsLabel = "Pontuação: {0}     ";

    public Text NameText;
    public Text PointsText;

    #endregion

    void Start()
    {
        Player player = PreLoad.GetPlayer();

        try
        {
            NameText.text = string.Format(nameLabel, player.name);
            PointsText.text = string.Format(pointsLabel, player.points);
        }
        catch (Exception) { }
    }
}
