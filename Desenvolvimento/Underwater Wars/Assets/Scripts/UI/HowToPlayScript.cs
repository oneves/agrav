﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HowToPlayScript : MonoBehaviour
{

    private struct comb
    {
        public string name;
        public bool touch;
    }


    private Translator signLangTranslator;
    private List<comb> touched;

    void Start()
    {
        touched = new List<comb>();
        touched.Add(new comb { name = "back", touch = false });
        touched.Add(new comb { name = "start", touch = false });

        if (PreLoad.GetPlayer().type == PlayerType.BLIND)
        {
            GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice("howtodesc");
        }

        if (PreLoad.GetPlayer().type == PlayerType.DEAF || PreLoad.GetPlayer().type == PlayerType.GENERAL)
        {

            Camera cam = GameObject.Find("SignLangCamera").GetComponent<Camera>();
            //fix cam pos

            //16:9
            Rect camRec = new Rect(0.606f, 0.332f, 0.225f, 0.35f);
            //16:10
            if (System.Math.Round((float)Screen.width / (float)Screen.height,2) == System.Math.Round(16.0f / 10.0f,2))
            {
                camRec = new Rect(0.616f, 0.341f, 0.225f, 0.35f);
            }
            else
            //4:3
            if (System.Math.Round((float)Screen.width / (float)Screen.height,2) == System.Math.Round(4.0f / 3.0f,2))
            {
                camRec = new Rect(0.64f, 0.354f, 0.225f, 0.35f);
            }
            else
            //3:2
            if (System.Math.Round((float)Screen.width / (float)Screen.height,2) == System.Math.Round(3.0f / 2.0f,2))
            {
                camRec = new Rect(0.625f, 0.3471f, 0.225f, 0.35f);
            }
            else
            //8:5
            if (System.Math.Round((float)Screen.width / (float)Screen.height,2) == System.Math.Round(8.0f / 5.0f,2))
            {
                camRec = new Rect(0.617f, 0.342f, 0.225f, 0.35f);
            }
            else
            //5:3
            if (System.Math.Round((float)Screen.width / (float)Screen.height,2) == System.Math.Round(5.0f / 3.0f,2))
            {
                camRec = new Rect(0.612f, 0.338f, 0.225f, 0.35f);
            }


            cam.rect = camRec;

            signLangTranslator = GameObject.Find("Translator").GetComponent<Translator>();
            Text textToTranslate = GameObject.Find("TxtHowToPlay").GetComponent<Text>();
            if (signLangTranslator != null && textToTranslate != null)
            {
                signLangTranslator.TranslateAPI(textToTranslate.text);
            }
            else
            {
                Debug.Log("TRANSLATION FAILED");
            }
        }
    }

    public void touch(string name)
    {
        for (int i = 0; i < touched.Count; i++)
        {
            if (touched[i].name.Equals(name))
            {
                if (!touched[i].touch)
                {
                    touched[i] = new comb { name = touched[i].name, touch = true };

                    GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(name);

                    for (int j = 0; j < touched.Count; j++)
                    {
                        if (!touched[j].name.Equals(name))
                        {
                            touched[j] = new comb { name = touched[j].name, touch = false };
                        }
                    }
                }
                else
                {
                    GameObject.Find("AudioManager").GetComponent<AudioManager>().StopVoice("howtodesc");
                    if (touched[i].name.Equals("start")) SceneManager.LoadScene("_Game");
                    if (touched[i].name.Equals("back")) GameObject.Find("UIManager").GetComponent<UIManager>().SetScene("MainMenu");
                }

                break;
            }
        }
    }
}
