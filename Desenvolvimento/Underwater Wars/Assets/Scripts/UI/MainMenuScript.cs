﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using SimpleJSON;
using System.Linq;
using System.IO;
using System.Collections;
using UnderwaterWars.Networking;

public class MainMenuScript : MonoBehaviour
{
    private const string GET_PLAYER_SCORE_URL = "http://akatsua.ddns.net/uwars/getPlayerScore";
    private const string VOICE_URL = "http://akatsua.ddns.net/uwars/voice";

    private struct comb
    {
        public string name;
        public bool touch;
    }

    private List<comb> touched;

    void Start()
    {
        if (PreLoad.IsFirstTime && PreLoad.GetPlayer().type != PlayerType.BLIND)
        {
            try
            {
                GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayMusic("music");
                PreLoad.IsFirstTime = false;
            }
            catch (Exception) { Debug.Log("Exception..."); }
        }

        if (PreLoad.GetPlayer().type == PlayerType.BLIND)
        {
            GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice("2_1");
        }

        touched = new List<comb>();
        touched.Add(new comb { name = "play", touch = false });
        touched.Add(new comb { name = "howto", touch = false });
        touched.Add(new comb { name = "exit", touch = false });
    }

    public void touch(string name)
    {
        for (int i = 0; i < touched.Count; i++)
        {
            if (touched[i].name.Equals(name))
            {
                if (!touched[i].touch)
                {
                    touched[i] = new comb { name = touched[i].name, touch = true };

                    try
                    {
                        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(name);
                    }
                    catch (Exception) { Debug.Log("Exception..."); }

                    for (int j = 0; j < touched.Count; j++)
                    {
                        if (!touched[j].name.Equals(name))
                        {
                            touched[j] = new comb { name = touched[j].name, touch = false };
                        }
                    }
                }
                else
                {
                    if (touched[i].name.Equals("play"))
                    {
                        GameObject.Find("AudioManager").GetComponent<AudioManager>().StopVoice();
                        if (PreLoad.GetPlayer().deviceType == DeviceType.MOBILE)
                        { 
                            GameObject.Find("Launcher").GetComponent<Launcher>().Connect();
                        }
                    }
                    if (touched[i].name.Equals("howto"))
                    {
                        GameObject.Find("AudioManager").GetComponent<AudioManager>().StopVoice();

                        SceneManager.LoadScene("5.1_B_HowToPlay");
                    }
                    if (touched[i].name.Equals("exit"))
                    {
#if UNITY_EDITOR
                        Debug.Log("Exiting (only on .exe)");
#endif

                        Application.Quit();
                    }
                }

                break;
            }
        }
    }

    public void ReadPlayerPoints()
    {
        getPlayerScore();
    }

    private void getPlayerScore()
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            processScoreJSON(rest.Results);
        };

        Player player = PreLoad.GetPlayer();
        string userId = player.userId;

        var postParams = new Dictionary<string, string>()
                {
                    { "userId", userId }
                };

        rest.POST(GET_PLAYER_SCORE_URL, postParams, methodCall);
        Debug.Log("done posting");
    }

    private void processScoreJSON(string text)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed getPlayerScore  " + text);
        }
        else if (responseServer == "success")
        {
            string score = root["message"]["score"].Value;
            getScoreVoice(score);
        }
    }

    private void getScoreVoice(string score)
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            processScoreVoice(rest.Results);
        };

        string textToSend = "Tem um total de " + score + " pontos.";
        Debug.Log(textToSend);
        var postParams = new Dictionary<string, string>()
                {
                    { "text", textToSend }
                };

        rest.POST(VOICE_URL, postParams, methodCall);
        Debug.Log("done posting");
    }

    private void processScoreVoice(string text)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed getScoreVoice  " + text);
        }
        else if (responseServer == "success")
        {
            string voice = root["message"]["voice"].Value;
            Debug.Log("voice = " + voice);
            voice = voice.Substring(voice.IndexOf(',') + 1);
            Debug.Log("initial length = " + voice.Length);
            if (voice.Length % 4 != 0)
            { 
                voice += new string('=', 4 - voice.Length % 4);
            }
            Debug.Log("final length = " + voice.Length);

            byte[] decodedBytes = Convert.FromBase64String(voice);

            Debug.Log("decodedBytes length = " + decodedBytes.Length);
            string filepath = Application.persistentDataPath + "/voice.wav";
            File.WriteAllBytes(filepath, decodedBytes);
            Debug.Log(filepath);

            string fileURL = "file:///" + filepath;
            StartCoroutine(playAudioClip(fileURL));
        }
    }


    private IEnumerator playAudioClip(string fileURL)
    {
        WWW www = new WWW(fileURL);
        var clip = www.audioClip;
        while (clip.loadState != AudioDataLoadState.Loaded)
        { 
            yield return new WaitForSeconds(1);
        }
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(clip);
    }

}
