﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using SimpleJSON;

public class PointsUIScript : MonoBehaviour
{
    private const string GET_TOP_SCORES_URL = "http://akatsua.ddns.net/uwars/getTopScores";

    public Text playerstxt;
    public Text pointstxt;

    private struct comb
    {
        public string name;
        public bool touch;
    }
    
    private Translator signLangTranslator;
    private List<comb> touched;

    void Start()
    {
        touched = new List<comb>();
        touched.Add(new comb { name = "back", touch = false });

        getTopScores(10);   
    }


    public void getTopScores(int count)
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            processGetTopScoresJSON(rest.Results);
        };

        var postParams = new Dictionary<string, string>()
                {
                    { "count", count.ToString() }
                };

        rest.POST(GET_TOP_SCORES_URL, postParams, methodCall);
        Debug.Log("done posting");
    }


    public void processGetTopScoresJSON(string text)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed getTopScores  " + text);
        }
        else if (responseServer == "success")
        {
            JSONArray scoresNode = root["message"].AsArray;

            string pl = "";
            string po = "";

            for (int i = 0; i < scoresNode.Count; ++i)
            {
                string username = scoresNode[i]["username"].Value;
                string score = scoresNode[i]["score"].Value;

                pl = pl + "#" + (i + 1) + "         " + username + "\n";
                po = po + score + "\n";

                Debug.Log("Username: " + username + " | Score: " + score);

            }

            playerstxt.text = pl;
            pointstxt.text = po;

        }
    }
    public void touch(string name)
    {
        for (int i = 0; i < touched.Count; i++)
        {
            if (touched[i].name.Equals(name))
            {
                if (!touched[i].touch)
                {
                    touched[i] = new comb { name = touched[i].name, touch = true };

                    GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(name);

                    for (int j = 0; j < touched.Count; j++)
                    {
                        if (!touched[j].name.Equals(name))
                        {
                            touched[j] = new comb { name = touched[j].name, touch = false };
                        }
                    }
                }
                else
                {
                    if (touched[i].name.Equals("back")) GameObject.Find("UIManager").GetComponent<UIManager>().SetScene("MainMenu");
                }

                break;
            }
        }
    }
}
