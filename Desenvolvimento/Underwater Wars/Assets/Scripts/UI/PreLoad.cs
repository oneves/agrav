﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
//using VoiceRSS_SDK;

public class PreLoad : MonoBehaviour
{

    public static bool IsFirstTime = true;

    private static Player player;

    public static int numOfTeams = -1;

    public float MusicVolume = 1f;
    public float EffectsVolume = 1f;
    public float VoiceVolume = 1f;

    public static Player GetPlayer()
    {
#if UNITY_EDITOR
        if (player == null)
        {
            player = new Player("Chico Fininho", 200, PlayerType.GENERAL);
        }
#endif

        return player;
    }

    public static void SetPlayer(Player player_)
    {
        player = player_;
    }


    void Start()
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayEffect("intro");
        DontDestroyOnLoad(this);
       // StartCoroutine(Test());
    }

    #region "Dummy Data for Tests"

    public static List<LoginData> logins;

    public struct LoginData
    {
        public string name;
        public string email;
        public string pass;
        public PlayerType type;
        public float points;
    }

    public static List<PointsData> points;

    public struct PointsData
    {
        public string name;
        public float points;
    }

    private static void SetDummyData()
    {
        if (logins == null || logins.Count == 0)
        {
            logins = new List<LoginData>();
            logins.Add(new LoginData() { name = "normal", email = "normal@normal.normal", pass = "normal", type = PlayerType.GENERAL, points = 500 });
            logins.Add(new LoginData() { name = "blind", email = "blind@blind.blind", pass = "blind", type = PlayerType.BLIND, points = 150 });
            logins.Add(new LoginData() { name = "deaf", email = "deaf@deaf.deaf", pass = "deaf", type = PlayerType.DEAF, points = 325 });
        }

        if (points == null || points.Count == 0)
        {
            points = new List<PointsData>();
            points.Add(new PointsData() { name = "normal1", points = 500 });
            points.Add(new PointsData() { name = "blind1", points = 150 });
            points.Add(new PointsData() { name = "deaf1", points = 325 });
            points.Add(new PointsData() { name = "normal2", points = 500 });
            points.Add(new PointsData() { name = "blind2", points = 150 });
            points.Add(new PointsData() { name = "deaf2", points = 325 });
            points.Add(new PointsData() { name = "normal3", points = 500 });
            points.Add(new PointsData() { name = "blind3", points = 150 });
            points.Add(new PointsData() { name = "deaf3", points = 325 });
            points.Add(new PointsData() { name = "normal4", points = 500 });
            points.Add(new PointsData() { name = "blind4", points = 150 });
            points.Add(new PointsData() { name = "deaf4", points = 325 });
        }
    }

    public static Player DummyLogin(string un, string pw)
    {
        SetDummyData();

        foreach (var item in logins)
        {
            if (item.name == un && item.pass == pw)
            {
                player = new Player(item.name, item.points, item.type);
                return player;
            }
        }

        return null;
    }

    public static RegisterLogin.RegisterReturn DummyRegister(string un, string email, string pw, PlayerType playerType)
    {
        SetDummyData();

        bool exists = false;

        foreach (var item in logins)
        {
            if (item.name == un && item.email == email)
            {
                exists = true;
                break;
            }
        }

        if (exists)
            return RegisterLogin.RegisterReturn.ALREADY_REGISTED;

        logins.Add(new LoginData() { name = un, email = email, pass = pw, type = playerType, points = 0 });

        return RegisterLogin.RegisterReturn.SUCCESS;
    }

    public static List<PointsData> GetTop10()
    {
        SetDummyData();

        for (int i = 0; i < points.Count - 1; i++)
        {
            for (int j = i + 1; j < points.Count; j++)
            {
                if (points[i].points < points[j].points)
                {
                    PointsData tempPointData = points[i];
                    points[i] = points[j];
                    points[j] = tempPointData;
                }
            }
        }

        List<PointsData> top10 = new List<PointsData>();

        for (int i = 0; (i < 10 && points.Count >= 10) || (i < points.Count && points.Count < 10); i++)
        {
            top10.Add(points[i]);
        }

        return top10;
    }

    #endregion

    public string words = "Hello";
    IEnumerator Test()
    {
        // Remove the "spaces" in excess
        Regex rgx = new Regex("\\s+");
        // Replace the "spaces" with "% 20" for the link Can be interpreted
        string result = rgx.Replace(words, "%20");
        string url = "http://translate.google.com/translate_tts?tl=en&q=" + result;
        WWW www = new WWW(url);
        yield return www;

        AudioClip c = www.GetAudioClip(false, false, AudioType.WAV);

        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(c);
    }

}

#region Player Defenition

//TODO: Move to proper location?

public enum PlayerType
{
    BLIND,
    DEAF,
    GENERAL
}

public enum DeviceType
{
    COMPUTER,
    MOBILE
}

public class Player
{
    public string name;
    public float points;
    public PlayerType type;

    public string userId;
    public DeviceType deviceType;

    public Player(string name, float points, PlayerType type)
    {
        this.name = name;
        this.points = points;
        this.type = type;
    }

    public Player(string name, float points, PlayerType type, string userId, DeviceType deviceType)
    {
        this.name = name;
        this.points = points;
        this.type = type;
        this.userId = userId;
        this.deviceType = deviceType;
    }
}

#endregion