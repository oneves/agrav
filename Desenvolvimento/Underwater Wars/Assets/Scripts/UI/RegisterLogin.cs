﻿using SimpleJSON;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class RegisterLogin : MonoBehaviour
{

    #region Variables

    public InputField UsernameField;
    public InputField PasswordField;
    public InputField EmailField;

    public ToggleGroup Toogles;

    public Text ErrorMessage;

    public enum RegisterReturn
    {
        SUCCESS,
        ALREADY_REGISTED,
        ERROR
    }

    public void Start()
    {
        if (EmailField != null)
            EmailField.characterLimit = 50;
    }

    private const string LOGIN_URL = "http://akatsua.ddns.net/uwars/login";
    private const string REGISTER_URL = "http://akatsua.ddns.net/uwars/register";

    #endregion

    #region "Public Methods"

    public void Login()
    {
        string un = UsernameField.text;
        string pw = PasswordField.text;

        if (string.IsNullOrEmpty(un))
        {
            ErrorMessage.text = "UTILIZADOR INVÁLIDO";
            return;
        }
        if (string.IsNullOrEmpty(pw))
        {
            ErrorMessage.text = "PALAVRA-PASSE INVÁLIDA";
            return;
        }

        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            ProcessServerLogin(rest.Results);
        };
        Dictionary<string, string> postParams = new Dictionary<string, string>()
                {
                    { "username", un },
                    { "password", pw },
                };
        rest.POST(LOGIN_URL, postParams, methodCall);
        Debug.Log("done posting");
    }

    public void Register()
    {
        string un = UsernameField.text;
        string em = EmailField.text;
        string pw = PasswordField.text;

        Toggle currentToggle = Toogles.GetComponent<ToggleGroup>().ActiveToggles().FirstOrDefault();
        string type = currentToggle.GetComponentInChildren<Text>().text;


        if (string.IsNullOrEmpty(un))
        {
            ErrorMessage.text = "UTILIZADOR INVÁLIDO";
            return;
        }
        if (!Regex.IsMatch(em, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
        {
            ErrorMessage.text = "EMAIL INVÁLIDO";
            return;
        }
        if (string.IsNullOrEmpty(pw))
        {
            ErrorMessage.text = "PALAVRA-PASSE INVÁLIDA";
            return;
        }

        string typeToSend = "0";
        if (type.Equals("SURDO"))
        {
            typeToSend = "1";
        }
        else if (type.Equals("CEGO"))
        {
            typeToSend = "2";
        }

        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            ProcessServerRegister(rest.Results);
        };
        Dictionary<string, string> postParams = new Dictionary<string, string>()
                    {
                        { "username", un },
                        { "password", pw },
                        { "email", em },
                        { "type", typeToSend }
                    };
        rest.POST(REGISTER_URL, postParams, methodCall);
        Debug.Log("done posting");
    }


    #endregion

    #region "Private Methods"


    private void ProcessServerLogin(string text)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed login  " + text);
            ErrorMessage.text = "FALHA NO LOGIN";
        }
        else if (responseServer == "success")
        {
            string username = root["message"]["username"].Value;
            string idtoStore = root["message"]["id"].Value;
            int type = root["message"]["type"].AsInt;
            string playerEmail = root["message"]["email"].Value;
            float totalScore = root["message"]["score"].AsFloat;

            PlayerType playerType = type == 0 ? PlayerType.GENERAL : type == 1 ? PlayerType.DEAF : PlayerType.BLIND;

#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1)
            DeviceType deviceType = DeviceType.MOBILE;
#else
            DeviceType deviceType = DeviceType.COMPUTER;
#endif

            PreLoad.SetPlayer(new Player(username, totalScore, playerType, idtoStore, deviceType));

            if (PreLoad.GetPlayer().type == PlayerType.BLIND)
            {
                GameObject.Find("AudioManager").GetComponent<AudioManager>().StopMusic();
                GameObject.Find("AudioManager").GetComponent<AudioManager>().StopEffect();

                SceneManager.LoadScene("2.1_B_MainMenu");
            }
            else if (PreLoad.GetPlayer().type == PlayerType.DEAF || PreLoad.GetPlayer().type == PlayerType.GENERAL)
            {
                SceneManager.LoadScene("2.2_DG_MainMenu");
            }
        }
    }


    private void ProcessServerRegister(string text)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed register  " + text);
            ErrorMessage.text = "FALHA NO REGISTO";

            if ((string)root["message"] == "@user_already_exists")
            {
                ErrorMessage.text = "EMAIL JÁ REGISTADO";
            }
        }
        else if (responseServer == "success")
        {
            string id = root["message"]["id"].Value;
            Debug.Log("Successfully registered. Id = " + id);
            SceneManager.LoadScene("1.1_LoginRegister");
        }
    }


    private Player login(string un, string pw)
    {
        //TODO: login through the data base and return PlayerType (blind, deaf or normal)
        return PreLoad.DummyLogin(un, pw);
    }

    private RegisterReturn register(string un, string email, string pw, string type)
    {
        PlayerType playerType = PlayerType.GENERAL;

        if (type.Equals("Surdos"))
        {
            playerType = PlayerType.DEAF;
        }
        else if (type.Equals("Cegos"))
        {
            playerType = PlayerType.DEAF;
        }

        //TODO: login through the data base and return RegisterReturn (success, already registed, error)
        return PreLoad.DummyRegister(un, email, pw, playerType);
    }

    #endregion
}
