﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour
{

    public Slider Music;

    public Slider Effects;

    public Slider Voice;

    private struct comb
    {
        public string name;
        public bool touch;
    }

    private List<comb> touched;

    void Start()
    {
        Music.value = GameObject.Find("PreLoad").GetComponent<PreLoad>().MusicVolume;
        Effects.value = GameObject.Find("PreLoad").GetComponent<PreLoad>().EffectsVolume;
        Voice.value = GameObject.Find("PreLoad").GetComponent<PreLoad>().VoiceVolume;

        touched = new List<comb>();
        touched.Add(new comb { name = "back", touch = false });
        touched.Add(new comb { name = "save", touch = false });
    }

    public void PlayMBeep(float volume)
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayEffectWithTempVolume("mus", volume);
    }
    public void PlayEBeep(float volume)
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayEffectWithTempVolume("efe", volume);
    }
    public void PlayVBeep(float volume)
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayEffectWithTempVolume("voz", volume);
    }
    public void PlayBeep(float volume)
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayEffectWithTempVolume("beep", volume);
    }

    public void touch(string name)
    {
        for (int i = 0; i < touched.Count; i++)
        {
            if (touched[i].name.Equals(name))
            {
                if (!touched[i].touch)
                {
                    touched[i] = new comb { name = touched[i].name, touch = true };

                    GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(name);

                    for (int j = 0; j < touched.Count; j++)
                    {
                        if (!touched[j].name.Equals(name))
                        {
                            touched[j] = new comb { name = touched[j].name, touch = false };
                        }
                    }
                }
                else
                {
                    if (touched[i].name.Equals("back")) GameObject.Find("UIManager").GetComponent<UIManager>().SetScene("MainMenu");
                    if (touched[i].name.Equals("save")) Save();

                }
            }
        }
    }

    public void Save()
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().ChangeMusicVolume(Music.value);
        GameObject.Find("AudioManager").GetComponent<AudioManager>().ChangeEffectsVolume(Effects.value);
        GameObject.Find("AudioManager").GetComponent<AudioManager>().ChangeVoiceVolume(Voice.value);

        GameObject.Find("UIManager").GetComponent<UIManager>().SetScene("MainMenu");
    }
}
