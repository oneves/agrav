﻿using UnityEngine;
using System.Collections;

public class UICompass : MonoBehaviour {

    public GameObject needle;

    private bool rotating = true;
    private float rotatingSpeed = 5;
    private Vector3 anglDegrees = Vector3.zero;

    
    public void updateNeedle(float angle_degrees)
    {
        anglDegrees.z = angle_degrees;
        rotating = true;
    }

    void Update()
    {
        if (rotating)
        {
            if (Vector3.Distance(needle.transform.eulerAngles, anglDegrees) > 0.01f)
            {
                //needle.transform.eulerAngles = Vector3.Lerp(needle.transform.eulerAngles, anglDegrees, Time.deltaTime * rotatingSpeed);

                Vector3 angles = needle.transform.eulerAngles;
                angles.z = Mathf.LerpAngle(angles.z, anglDegrees.z, Time.deltaTime * rotatingSpeed);
                needle.transform.eulerAngles = angles;
            }
            else
            {
                needle.transform.eulerAngles = anglDegrees;
                rotating = false;
            }
        }
    }
}
