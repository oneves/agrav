﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    void Start()
    {
        if (PreLoad.GetPlayer().type != PlayerType.BLIND)
        {
            GameObject obj = GameObject.Find("/Canvas/Help/HelpPanel");
            if (obj) obj.SetActive(false);
        }
    }

    public void SetScene(string scene)
    {
        string goToScene = scene;

        if (goToScene.Equals("MainMenu"))
        {
            if (PreLoad.GetPlayer().type == PlayerType.BLIND)
            {
                goToScene = "2.1_B_MainMenu";
            }
            else if (PreLoad.GetPlayer().type == PlayerType.DEAF || PreLoad.GetPlayer().type == PlayerType.GENERAL)
            {
                goToScene = "2.2_DG_MainMenu";
            }
        }

        SceneManager.LoadScene(goToScene);
    }

    public void ShowHelp(string helpMenu)
    {
        GameObject.Find("/Canvas/Help/HelpPanel").SetActive(true);

        if (PreLoad.GetPlayer().type == PlayerType.BLIND)
        {
            try
            {
                GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayHelp(helpMenu);
            }
            catch (Exception) { Debug.Log("Exception..."); }
        }

        GameObject.Find("/SignLangCamera").GetComponent<Camera>().enabled = false;
    }

    public void HideHelp()
    {
        GameObject.Find("/Canvas/Help/HelpPanel").SetActive(false);

        try
        {
            GameObject.Find("AudioManager").GetComponent<AudioManager>().StopHelp();
        }
        catch (Exception) { Debug.Log("Exception..."); }

        GameObject.Find("/SignLangCamera").GetComponent<Camera>().enabled = true;
    }

    public void Exit()
    {

#if UNITY_EDITOR
        Debug.Log("Exiting (only on .exe)");
#endif

        Application.Quit();
    }

}
