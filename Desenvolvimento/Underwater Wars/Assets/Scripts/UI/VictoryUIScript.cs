﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using SimpleJSON;
using System.Linq;
using System.IO;

public class GameStats
{
    public int shotsFired;
    public int minesDeployed;
    public int totalDamageDealt;
    public int shotsHit;
    public float precision;
    public int minesDestroyed;
    public int subsDestroyed;
    public float score;
}

public class VictoryUIScript : MonoBehaviour
{
    private const string VOICE_URL = "http://akatsua.ddns.net/uwars/voice";

    public static GameStats gameStats;

    public Text aspects;
    public Text points;
    public Text finalPoints;

    public bool defeat = false;

    private struct comb
    {
        public string name;
        public bool touch;
    }

    private List<comb> touched;

    void Start()
    {
        touched = new List<comb>();
        touched.Add(new comb { name = "back", touch = false });

        string pl = "", po = "";
        if(TileMap.localPlayerRole == 0)
        {
            pl = "Nº TIROS DISPARADOS\n" +
            "Nº TIROS ATINGIDOS\n";
        }
        else
        {
            pl = "Nº TORPEDOS DISPARADOS\n" +
           "Nº TORPEDOS ATINGIDOS\n";
        }
        pl += "PRECISÃO\n" +
            "MINAS DESTRUÍDAS\n" +
            "SUBMARINOS DESTRUÍDOS\n" +
            "DANO TOTAL EFETUADO";

        po = gameStats.shotsFired + "\n" +
            gameStats.shotsHit + "\n" +
            (int)gameStats.precision + "%\n" +
            gameStats.minesDestroyed + "\n" +
            gameStats.subsDestroyed + "\n" +
            gameStats.totalDamageDealt; ;

        if (TileMap.localPlayerRole == 0)
        {
            pl += "MINAS PLANTADAS";
            po += "\n" + gameStats.minesDeployed;
        }

        aspects.text = pl;
        points.text = po;

        float finalPointsF = gameStats.score;

        finalPoints.text = "PONTUAÇÃO\n" + finalPointsF;
        if (PreLoad.GetPlayer().type == PlayerType.BLIND)
        {
            if (defeat)
                GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice("10_1");
            else
                GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice("9_1");

            getScoreVoice(defeat);
            //#HERE - call server to return points audio and play
        }
    }

    public void getScoreVoice(bool lostGame)
    {
        RESTClient rest = gameObject.AddComponent(typeof(RESTClient)) as RESTClient;
        System.Action methodCall = delegate ()
        {
            Debug.Log(rest.Results);
            processScoreVoice(rest.Results);
        };

        Player player = PreLoad.GetPlayer();
        string userId = player.userId;

        string textToSend = lostGame ? "Perdeu" : "Ganhou";
        textToSend += " o jogo com " + gameStats.score + " pontos.";

        var postParams = new Dictionary<string, string>()
                {
                    { "text", textToSend }
                };

        rest.POST(VOICE_URL, postParams, methodCall);
        Debug.Log("done posting");
    }

    private void processScoreVoice(string text)
    {
        JSONNode root = JSON.Parse(text);
        string responseServer = root["type"];
        if (responseServer == "error")
        {
            Debug.Log("Failed getScoreVoice  " + text);
        }
        else if (responseServer == "success")
        {
            string voice = root["message"]["voice"].Value;
            Debug.Log("voice = " + voice);
            voice = voice.Substring(voice.IndexOf(',') + 1);
            Debug.Log("initial length = " + voice.Length);
            if (voice.Length % 4 != 0)
            {
                voice += new string('=', 4 - voice.Length % 4);
            }
            Debug.Log("final length = " + voice.Length);

            byte[] decodedBytes = Convert.FromBase64String(voice);

            Debug.Log("decodedBytes length = " + decodedBytes.Length);
            string filepath = Application.persistentDataPath + "/voice.wav";
            File.WriteAllBytes(filepath, decodedBytes);
            Debug.Log(filepath);

            string fileURL = "file:///" + filepath;
            StartCoroutine(playAudioClip(fileURL));
        }
    }

    private IEnumerator playAudioClip(string fileURL)
    {
        WWW www = new WWW(fileURL);
        var clip = www.audioClip;
        while (clip.loadState != AudioDataLoadState.Loaded)
        {
            yield return new WaitForSeconds(1);
        }
        GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(clip);
    }

    public void touch(string name)
    {
        for (int i = 0; i < touched.Count; i++)
        {
            if (touched[i].name.Equals(name))
            {
                if (!touched[i].touch)
                {
                    touched[i] = new comb { name = touched[i].name, touch = true };

                    GameObject.Find("AudioManager").GetComponent<AudioManager>().PlayVoice(name);

                    for (int j = 0; j < touched.Count; j++)
                    {
                        if (!touched[j].name.Equals(name))
                        {
                            touched[j] = new comb { name = touched[j].name, touch = false };
                        }
                    }
                }
                else
                {
                    if (touched[i].name.Equals("back")) GameObject.Find("UIManager").GetComponent<UIManager>().SetScene("MainMenu");
                }

                break;
            }
        }
    }
}
