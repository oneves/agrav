﻿using UnityEngine;
using System.Collections;

public class Mine : Unit
{
    [SerializeField]
    private int _explosionRadius = 2;
    [SerializeField]
    private int _explosionDamage = 2;

    private GameObject _parent;
    private Submarine _sub;

    [SerializeField]
    private AudioClip _audioClipExplode;
    private AudioSource _audioSourceExplode;

    void Start()
    {
        _audioSourceExplode = this.gameObject.AddComponent<AudioSource>() as AudioSource;
        _audioSourceExplode.clip = _audioClipExplode;
    }

    void Update()
    {
        UpdateIsPlayingSound();
        if (_visibleCD > 0) _visibleCD -= Time.deltaTime;
    }

    private void UpdateIsPlayingSound()
    {
        _isPlayingSound = false;
        if (_audioSourceExplode.isPlaying) _isPlayingSound = true;
    }

    public void SetParent(GameObject parent)
    {
        _parent = parent;
    }

    public void SetSubmarine(Submarine sub)
    {
        _sub = sub;
    }

    public override bool Damage(float amount)
    {
        this.Explode();
        return true;
    }

    public void Explode()
    {
        if (!IsAlive) return;

        if (IsVisible)
        {
            _audioSourceExplode.Play();
            _isPlayingSound = true;
        }

        base.Kill();

        float totalDamage = 0;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, _explosionRadius);
        foreach (var collider in colliders)
        {
            Unit hitUnit = collider.gameObject.GetComponent<Unit>();
            if (hitUnit != null)
            {
                hitUnit.Damage(_explosionDamage);
                if (hitUnit is Mine || hitUnit is Submarine)
                {
                    if (hitUnit != _parent && hitUnit != this)
                    {
                        totalDamage += _explosionDamage;
                    }
                }
            }
        }
        _sub.addMineDamage(totalDamage);

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Unit unit = collision.collider.gameObject.GetComponent<Unit>();
        this.OnCollision(unit);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.gameObject.GetComponent<Unit>();
        this.OnCollision(unit);
    }
    private void OnCollision(Unit unit)
    {
        if (unit != null && unit.gameObject != _parent)
        {
            this.Explode();
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        Unit unit = collision.gameObject.GetComponent<Unit>();
        if(unit.gameObject == _parent)
        {
            _parent = null;
        }
    }
}
