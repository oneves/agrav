﻿using UnityEngine;
using System.Collections;
using System;

public class Submarine : Unit, IPunObservable
{
    [SerializeField]
    private float _maxSpeed;
    [SerializeField]
    private float _speedAcceleration;
    [SerializeField]
    private float _maxRotationSpeed;
    [SerializeField]
    private float _rotationSpeedAcceleration;
    [SerializeField]
    private float _rammingDamageMultiplier;
    [SerializeField]
    private float _turretRotationSpeed;
    [SerializeField]
    private int _turretDamage;
    [SerializeField]
    private float _turretRange;
    [SerializeField]
    private float _turretCoolDownTime;
    [SerializeField]
    private float _turretIndicatorLength;
    [SerializeField]
    private int _torpedoMaxCount;
    [SerializeField]
    private float _torpedoRegenTime;
    [SerializeField]
    private float _torpedoCoolDownTime;
    [SerializeField]
    private int _mineMaxCount;
    [SerializeField]
    private float _mineRegenTime;
    [SerializeField]
    private float _mineCoolDownTime;
    [SerializeField]
    private float _sightRange;

    [SerializeField]
    private AudioClip _audioClipCollision;
    private AudioSource _audioSourceCollision;
    [SerializeField]
    private AudioClip _audioClipFireTorpedo;
    private AudioSource _audioSourceFireTorpedo;
    [SerializeField]
    private AudioClip _audioClipFireMine;
    private AudioSource _audioSourceFireMine;
    [SerializeField]
    private AudioClip _audioClipFireTurret;
    private AudioSource _audioSourceFireTurret;

    private float _currentSpeed=0;
    private float _currentRotationSpeed;
    public bool _turretOn;
    private float _turretRotation;
    private float _turretCoolDownTimeFinish;
    private int _torpedoCount;
    private float _torpedoRegenTimeFinish;
    private float _torpedoCoolDownTimeFinish;
    private float _torpedoSpeedMultiplier = 1;
    private int _mineCount;
    private float _mineRegenTimeFinish;
    private float _mineCoolDownTimeFinish;
    private LineRenderer _turretIndicatorRenderer;

    public float _forcedVisibleTimeAfterHit = 3; //time in sec that this unit will be visible by its target after a successful hit on an enemy sub
    public Operator _operator;
    public Strategist _strategist;
    
    public void SetOperator(Operator oper)
    {
        _operator = oper;
    }

    public void SetStrategist(Strategist strat)
    {
        _strategist = strat;
    }

    public float CurrentSpeed{ get { return _currentSpeed; } }
    public float TurretRotation { get { return _turretRotation; } }
    public float TurretCoolDownTime {
        get { return _turretCoolDownTime; }
        set { _turretCoolDownTime = value; }
    }
    public float TorpedoSpeedMultiplier {
        get { return _torpedoSpeedMultiplier; }
        set { _torpedoSpeedMultiplier = value; }
    }
    public int TorpedoCount { get { return _torpedoCount; } }
    public int MineCount { get { return _mineCount; } }
    public float SightRange { get { return _sightRange; } }

    public int subTeamId = -1;


    void Start()
    {
        transform.parent = GameObject.Find("Units").transform;
        _torpedoCount = _torpedoMaxCount;
        _mineCount = _mineMaxCount;

        if (!photonView.isMine)
        {
            object[] data = photonView.instantiationData;
            subTeamId = (int)data[0];
            Debug.Log("subTeamId = " + subTeamId);

            if (TileMap.localTeamId == subTeamId)
            {
                TileMap.Instance.setStrategistTeam(this);
                Debug.Log("This sub is now mine. Team id = " + subTeamId);
            }
        }
        _turretIndicatorRenderer = GetComponent<LineRenderer>();
        _turretIndicatorRenderer.sortingOrder = 1;

        _audioSourceCollision = this.gameObject.AddComponent<AudioSource>() as AudioSource;
        _audioSourceCollision.clip = _audioClipCollision;
        _audioSourceFireTorpedo = this.gameObject.AddComponent<AudioSource>() as AudioSource;
        _audioSourceFireTorpedo.clip = _audioClipFireTorpedo;
        _audioSourceFireMine = this.gameObject.AddComponent<AudioSource>() as AudioSource;
        _audioSourceFireMine.clip = _audioClipFireMine;
        _audioSourceFireTurret = this.gameObject.AddComponent<AudioSource>() as AudioSource;
        _audioSourceFireTurret.clip = _audioClipFireTurret;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        base.OnPhotonSerializeView(stream, info);

        if (stream.isWriting)
        {
            Debug.Log("writing on Submarine");
            // We own this sub: send the others our data
            stream.SendNext(1);
        }
        else
        {
            Debug.Log("reading on Submarine");
            // Network sub, receive data
            int xas = (int)stream.ReceiveNext();
        }
    }

    void Update()
    {
        if (!photonView.isMine && PhotonNetwork.connected && !(TileMap.localPlayerRole == 0 && subTeamId == TileMap.localTeamId))
        {
            return;
        }
        //Debug.Log("helloooo");
        float deltaTime = Time.deltaTime;
        UpdateMovement(deltaTime);
        UpdateRotation(deltaTime);
        UpdateTorpedoCD(deltaTime);
        UpdateMineCD(deltaTime);
        UpdateTurretCD(deltaTime);
        UpdateTurretIndicator();
        UpdateIsPlayingSound();
        if (_turretOn) FireTurret();
        if (_visibleCD > 0) _visibleCD -= deltaTime;
    }

    public void Accelerate(float amount)
    {
        amount = Mathf.Clamp(amount, -1, 1);
        float currentMax = _maxSpeed * amount;
        if(currentMax > 0)
        {
            _currentSpeed += _speedAcceleration;
            if (_currentSpeed > currentMax) _currentSpeed = currentMax;
        }
        else if(currentMax <= 0)
        {
            _currentSpeed -= _speedAcceleration;
            if (_currentSpeed < 0) _currentSpeed = 0;
        }
    }

    //Positive = CW; Negative = CCW
    public void Rotate(float amount)
    {
        float currentMax = _maxRotationSpeed * -amount;
        if (currentMax > 0)
        {
            _currentRotationSpeed += _rotationSpeedAcceleration;
            if (_currentRotationSpeed > currentMax) _currentRotationSpeed = currentMax;
        }
        else if (currentMax < 0)
        {
            _currentRotationSpeed -= _rotationSpeedAcceleration;
            if (_currentRotationSpeed < currentMax) _currentRotationSpeed = currentMax;
        }
        else if (currentMax == 0)
        {
            if(_currentRotationSpeed != 0)
            {
                int inverseSign = _currentRotationSpeed > 0 ? -1 : 1;
                _currentRotationSpeed += _rotationSpeedAcceleration * inverseSign;
                if(inverseSign == -1)
                {
                    if (_currentRotationSpeed < 0) _currentRotationSpeed = 0;
                } else
                {
                    if (_currentRotationSpeed > 0) _currentRotationSpeed = 0;
                }
            }
        }
    }
    
    //Positive = CW; Negative = CCW
    public void RotateTurret(float amount)
    {
        _turretRotation += _turretRotationSpeed * -amount;
        while (_turretRotation >= 360) _turretRotation -= 360;
        while (_turretRotation < 0) _turretRotation += 360;
    }

    public void ToggleTurret()
    {
        _turretOn = !_turretOn;
    }


    public void FireTurret()
    {
        if (_turretCoolDownTimeFinish > 0) return;
        _turretCoolDownTimeFinish = _turretCoolDownTime;

        if (PhotonNetwork.connected)
        {
            photonView.RPC("FireTurretRPC", PhotonTargets.All, null);
        }
        else
        {
            FireTurretRPC();
        }
        _strategist._timesFired++;
    }

    [PunRPC]
    public void FireTurretRPC()
    {
        ForceVisible();

        bool savedSetting = Physics2D.queriesStartInColliders;
        //Prevents hitting self
        Physics2D.queriesStartInColliders = false;
        
        Vector3 direction = Quaternion.Euler(0, 0, _turretRotation) * Vector3.up;

        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, direction, _turretRange);
        foreach(var info in hits)
        {
            if (info.collider != null)
            {
                Unit hitUnit = info.collider.gameObject.GetComponent<Unit>();
                if (hitUnit != null)
                {
                    if (hitUnit is Mine || hitUnit is Submarine)
                    {
                        bool kill = hitUnit.Damage(_turretDamage);

                        if (_strategist != null)
                        { 
                            _strategist.addDamage(_turretDamage);
                            if (hitUnit is Mine && kill)
                            {
                                _strategist._minesDestroyed++;
                            }
                            else if (hitUnit is Submarine && kill)
                            {
                                _strategist.killingBlows++;
                            }
                        }
                    }
                    break;
                }
            }
        }

        Physics2D.queriesStartInColliders = savedSetting;

        if(IsVisible) _audioSourceFireTurret.Play();
    }

    public bool FireTorpedo()
    {
        if (_torpedoCount <= 0 || _torpedoCoolDownTimeFinish > 0) return false;
        _torpedoCount--;
        _torpedoCoolDownTimeFinish = _torpedoCoolDownTime;

        float tideDirection = TideAPI.Instance.Direction;
        float tideStrength = TideAPI.Instance.Strength;

        if (PhotonNetwork.connected)
        {
            photonView.RPC("FireTorpedoRPC", PhotonTargets.All, tideDirection, tideStrength);
        }
        else
        {
            FireTorpedoRPC(tideDirection, tideStrength);
        }

        return true;
    }

    [PunRPC]
    public void FireTorpedoRPC(float tideDirection, float tideStrength)
    {
        ForceVisible();

        Torpedo torp = (Torpedo)TileMap.Instance.SpawnUnit(Unit.UnitType.TORPEDO);
        torp.SetSubmarine(this);
        torp.SetParent(gameObject);
        torp.SetInitialSpeed(this._currentSpeed);
        torp.SetSpeedMultiplier(_torpedoSpeedMultiplier);
        torp.transform.position = transform.position;
        torp.transform.rotation = transform.rotation;
        torp.tideDirection = tideDirection;
        torp.tideStrength = tideStrength;


        if (IsVisible) _audioSourceFireTorpedo.Play();
    }

    public bool FireMine()
    {
        if (_mineCount <= 0 || _mineCoolDownTimeFinish > 0) return false;
        _mineCount--;
        _mineCoolDownTimeFinish = _mineCoolDownTime;

        if (PhotonNetwork.connected)
        {
            photonView.RPC("FireMineRPC", PhotonTargets.All, null);
        }
        else
        {
             FireMineRPC();
        }

        return true;
    }

    [PunRPC]
    public void FireMineRPC()
    {
        Mine mine = (Mine)TileMap.Instance.SpawnUnit(Unit.UnitType.MINE);
        mine.SetParent(this.gameObject);
        mine.SetSubmarine(this);
        mine.transform.position = transform.position;

        if(IsVisible) _audioSourceFireMine.Play();
    }

    private void UpdateMovement(float deltaTime)
    {
        float movement = _currentSpeed * deltaTime;

        Vector3 movementAmount = transform.up * movement;
        Vector3 currentPosition = transform.position;
        currentPosition += movementAmount;
        transform.position = currentPosition;
    }

    private void UpdateRotation(float deltaTime)
    {
        float rotation = _currentRotationSpeed * deltaTime;

        Vector3 currentRotation = transform.eulerAngles;
        currentRotation.z += rotation;

        transform.eulerAngles = currentRotation;
    }

    private void UpdateTorpedoCD(float deltaTime)
    {
        if (_torpedoCoolDownTimeFinish > 0)
        {
            _torpedoCoolDownTimeFinish -= deltaTime;
        }

        if (_torpedoRegenTimeFinish > 0)
        {
            _torpedoRegenTimeFinish -= deltaTime;
        }

        //Prevents the first torpedo from regenerating instantly
        if (_torpedoCount == _torpedoMaxCount)
        {
            _torpedoRegenTimeFinish = _torpedoRegenTime;
        }

        if (_torpedoCount < _torpedoMaxCount && _torpedoRegenTimeFinish <= 0)
        {
            _torpedoRegenTimeFinish = _torpedoRegenTime;
            _torpedoCount++;
        }
    }

    private void UpdateMineCD(float deltaTime)
    {
        if (_mineCoolDownTimeFinish > 0)
        {
            _mineCoolDownTimeFinish -= deltaTime;
        }

        if (_mineRegenTimeFinish > 0)
        {
            _mineRegenTimeFinish -= deltaTime;
        }

        //Prevents the first mine from regenerating instantly
        if (_mineCount == _mineMaxCount)
        {
            _mineRegenTimeFinish = _mineRegenTime;
        }

        if (_mineCount < _mineMaxCount && _mineRegenTimeFinish <= 0)
        {
            _mineRegenTimeFinish = _mineRegenTime;
            _mineCount++;
        }
    }

    private void UpdateTurretCD(float deltaTime)
    {
        if (_turretCoolDownTimeFinish > 0)
        {
            _turretCoolDownTimeFinish -= deltaTime;
        }
    }

    private void UpdateTurretIndicator()
    {
        Vector3 basePosition = this.transform.position;
        _turretIndicatorRenderer.SetPosition(0, basePosition);
        _turretIndicatorRenderer.SetPosition(1, basePosition + ((Quaternion.Euler(0, 0, _turretRotation) * Vector3.up) * _turretIndicatorLength));
    }

    private void UpdateIsPlayingSound()
    {
        _isPlayingSound = false;
        if (_audioSourceCollision.isPlaying) _isPlayingSound = true;
        if (_audioSourceFireMine.isPlaying) _isPlayingSound = true;
        if (_audioSourceFireTorpedo.isPlaying) _isPlayingSound = true;
        if (_audioSourceFireTurret.isPlaying) _isPlayingSound = true;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Unit unit = collision.collider.gameObject.GetComponent<Unit>();
        this.OnCollision(unit);
    }

    private void OnCollision(Unit unit)
    {
        if (unit != null)
        {
            if(unit.Type == UnitType.SUBMARINE)
            {
                Vector2 thisVelocity = transform.rotation * new Vector2(0, _currentSpeed);
                Vector2 otherVelocity = unit.transform.rotation * new Vector2(0, ((Submarine)unit).CurrentSpeed);

                //float relativeVelocity = (thisVelocity - otherVelocity).magnitude;
                //if (relativeVelocity > 0)
                //{
                //    unit.Damage(relativeVelocity * _rammingDamageMultiplier);
                //}

                Vector2 collisionDir = (otherVelocity - thisVelocity).normalized;
                float speedAlongDir = ((Vector2.Dot(thisVelocity, collisionDir) / Vector2.Dot(collisionDir, collisionDir)) * collisionDir).magnitude;
                if (speedAlongDir > 0)
                {
                    unit.Damage(speedAlongDir * _rammingDamageMultiplier);
                }

                _currentSpeed = 0;
                if (IsVisible)
                {
                    _audioSourceCollision.Play();
                }

            } else if(unit.Type == UnitType.NONE)
            {
                this.Damage((transform.rotation * new Vector2(0, _currentSpeed)).magnitude);
                _currentSpeed = 0;
                if (IsVisible)
                {
                    _audioSourceCollision.Play();
                }
            }
        } else
        {
            _currentSpeed = 0;
        }
    }

    public void addTorpedoDamage(float damage)
    {
        _operator.addDamage(damage);
    }

    public void addMineDamage(float damage)
    {
        _strategist.addMineDamage(damage);
    }

    public void sendPing(Vector2 pingCoord, string msgStrat, int localTeamId)
    {
        print(msgStrat);
        if (PhotonNetwork.connected)
        {
            photonView.RPC("sendPingRPC", PhotonTargets.All, pingCoord, msgStrat, TileMap.localTeamId);
        }
        else
        {
            sendPingRPC(pingCoord, msgStrat, TileMap.localTeamId);
        }
    }

    [PunRPC]
    public void sendPingRPC(Vector2 pingCoord, string msgStrat, int teamId)
    {
        if (teamId == TileMap.localTeamId)
        {
            Debug.Log(pingCoord);
            TileMap.Instance.uiCtrl.receivePing(pingCoord);
            TileMap.Instance.uiCtrl.setTitle_MsgStrat(msgStrat);
        }
    }



    public bool clientOwnsSub()
    {
        return subTeamId == TileMap.localTeamId || photonView.isMine;
    }
}
