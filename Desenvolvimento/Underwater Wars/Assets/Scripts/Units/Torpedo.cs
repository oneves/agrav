﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Torpedo : Unit
{

    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _damage;

    private float _initialSpeed;
    private float _speedMultiplier = 1;
    private GameObject _parent;
    private Submarine _sub;

    [SerializeField]
    private AudioClip _audioClipExplode;
    private AudioSource _audioSourceExplode;

    private SpriteRenderer spriteRenderer;

    public float tideDirection;
    public float tideStrength;

    void Start()
    {
        _audioSourceExplode = this.gameObject.AddComponent<AudioSource>() as AudioSource;
        _audioSourceExplode.clip = _audioClipExplode;
        string path = Application.persistentDataPath + "/TorpedoSprites";
        Texture2D t = new Texture2D(1, 1);
        int side = Random.Range(1, 3);
        if (Directory.Exists(path))
        {
            path += "/side" + side + ".png";
            byte[] bytes2 = File.ReadAllBytes(path);
            t.LoadImage(bytes2);
        }
        else
        {
            t = Resources.Load<Texture2D>("side" + side);
        }
        Sprite s = Sprite.Create(t, new Rect(0, 0, t.width, t.height), new Vector2(0.5f, 0.5f));
        spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
        spriteRenderer.sprite = s;    
    }

    private void Update()
    {
        float deltaTime = Time.deltaTime;
        UpdateMovement(deltaTime);
        UpdateIsPlayingSound();
        if (_visibleCD > 0) _visibleCD -= deltaTime;
    }

    private void UpdateIsPlayingSound()
    {
        _isPlayingSound = false;
        if (_audioSourceExplode.isPlaying) _isPlayingSound = true;
    }

    public void SetParent(GameObject parent)
    {
        _parent = parent;
    }
    public void SetSubmarine(Submarine sub)
    {
        _sub = sub;
    }

    public void SetInitialSpeed(float initialSpeed)
    {
        _initialSpeed = initialSpeed;
    }

    public void SetSpeedMultiplier(float speedMultiplier)
    {
        _speedMultiplier = speedMultiplier;
    }

    private void UpdateMovement(float deltaTime)
    {
        float movement = (_speed + _initialSpeed) * deltaTime * _speedMultiplier;
               
        Vector3 tidePush = Quaternion.Euler(0, 0, tideDirection) * Vector3.up;
        tidePush *= tideStrength * 0.005f;
        Debug.Log(tideDirection + " " + tideStrength);

        Vector3 movementAmount = transform.up * movement;
        movementAmount += tidePush;

        Vector3 currentPosition = transform.position;
        currentPosition += movementAmount;
        transform.position = currentPosition;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Unit unit = collision.collider.gameObject.GetComponent<Unit>();
        this.OnCollision(unit);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.gameObject.GetComponent<Unit>();
        if (collision.gameObject.tag != "Floor")
        {
            this.OnCollision(unit);
        }
    }

    private void OnCollision(Unit unit)
    {
        if (unit != null)
        {
            if (unit.gameObject != _parent)
            {
                if (unit is Torpedo || unit is Mine || unit is Submarine)
                {
                    bool kill = unit.Damage(_damage);
                    if (unit is Submarine)
                    {
                        if (kill)
                            _sub._operator.killingBlows++;
                        else
                        {
                            Submarine target = (Submarine)unit;
                            if (target.clientOwnsSub())
                            {
                                Vector2 firedBy_pos = _sub.transform.position;
                                _sub.ForceVisible(_sub._forcedVisibleTimeAfterHit);
                                TileMap.Instance.uiCtrl.sendPing(firedBy_pos, TilePing.getCoordString(firedBy_pos));
                            }
                        }
                    }
                    else if (unit is Mine && kill)
                    {
                        _sub._operator._minesDestroyed++;
                    }
                    _sub.addTorpedoDamage(_damage);

                }
                if (IsVisible)
                {
                    _audioSourceExplode.Play();
                    _isPlayingSound = true;
                    base.Kill();
                }
            }
        }
        else
        {
            base.Kill();
        }
    }
}
