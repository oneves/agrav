﻿using UnityEngine;
using System.Collections;
using System;

public class Unit : Photon.PunBehaviour, IPunObservable
{

    public enum UnitType
    {
        MINE,
        TORPEDO,
        SUBMARINE,
        NONE
    }

    [SerializeField]
    private UnitType _type;
    [SerializeField]
    private bool _isKillable = true;
    [SerializeField]
    private float _health;
    [SerializeField]
    private float _visibilityCD;

    private bool _isAlive = true;
    private bool _isVisible = true;
    protected bool _isPlayingSound = false;
    protected float _visibleCD;

    public UnitType Type { get { return _type; } }
    public float Health { get { return _health; } }
    public bool IsAlive { get { return _isAlive; } }
    public bool IsVisible
    {
        get { return _isVisible; }
        set
        {
            if (!_isAlive || _isVisible == value) return;
            _isVisible = value;
            SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>();
            for (int i = 0; i < sprites.Length; i++)
            {
                sprites[i].enabled = _isVisible;
            }
            LineRenderer lineRenderer = GetComponent<LineRenderer>();
            if(lineRenderer != null)
            {
                lineRenderer.enabled = false;
            }
        }
    }
    public bool IsPlayingSound { get { return _isPlayingSound; } }

    public bool IsForcedVisible
    {
        get
        {
            return _visibleCD > 0;
        }
    }

    public void ForceVisible()
    {
        if (!_isAlive) return;

        IsVisible = true;
        if (_visibleCD < _visibilityCD)
            _visibleCD = _visibilityCD;
    }

    public void ForceVisible(float forcedVisibleTimeinSec)
    {
        if (!_isAlive) return;

        IsVisible = true;
        if (_visibleCD < forcedVisibleTimeinSec)
            _visibleCD = forcedVisibleTimeinSec;
    }

    virtual public bool Damage(float amount)
    {
        if (!IsAlive) return false;

        //Debug.Log("on damage: " + ((photonView == null) ? "true" : "false") + " type: " + Type.ToString());
        if (photonView != null && (photonView.isMine || !PhotonNetwork.connected))
        {
            _health -= amount;
            if (_health <= 0 && _isKillable)
            {
                if (Type == UnitType.SUBMARINE)
                {
                    if (PhotonNetwork.connected)
                    {
                        photonView.RPC("KillRPC", PhotonTargets.All, null);
                    }
                    else
                    {
                        KillRPC();
                    }
                }
                return true;
            };
            return false;
        }
        else
        {
            if (_health - amount <= 0 && _isKillable)
            {
                return true;
            };
            return false;
        }        
    }

    [PunRPC]
    public void KillRPC()
    {
        Kill();
    }

    virtual protected void Kill()
    {
        _health = 0;
        _isAlive = false;
        Destroy(GetComponent<Collider2D>());
        GetComponentInChildren<SpriteRenderer>().enabled = false;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            Debug.Log("writing on Unit");
            // We own this unit: send the others our data
            stream.SendNext(_health);
            stream.SendNext(_isAlive);

        }
        else
        {
            // Network unit, receive data
            this._health = (float)stream.ReceiveNext();
            this._isAlive = (bool)stream.ReceiveNext();
        }
    }

    public void SetSprite(Sprite newSprite)
    {
        SpriteRenderer childSprite = GetComponentInChildren<SpriteRenderer>();
        childSprite.sprite = newSprite;
    }
}
