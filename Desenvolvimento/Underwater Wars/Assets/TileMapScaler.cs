﻿using UnityEngine;
using System.Collections;

public class TileMapScaler : MonoBehaviour
{

    public UIController uiCtrl;
    public Camera mainCamera;
    float cam16_9 = 13.27f;
    float cam16_10 = 11.6f;
    float cam4_3 = 9.4f;
    float cam3_2 = 11.1f;
    float cam8_5 = 11.6f;
    float cam5_3 = 12.4f;
    float blind_YAdjust = 5.42f;

    void Start()
    {
        mainCamera.transform.position = new Vector3(999, 999, 999);
        Invoke("scaleMap", 0.1f);
    }

    void scaleMap()
    {
        if (uiCtrl)
            if (!uiCtrl.isBlind)
            {
                //16:9
                mainCamera.transform.position = new Vector3(cam16_9, 3.39f, -16.21f);
                //16:10
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(16.0f / 10.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam16_10, 3.39f, -16.21f);
                }
                else
                //4:3
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(4.0f / 3.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam4_3, 3.39f, -16.21f);
                }
                else
                //3:2
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(3.0f / 2.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam3_2, 3.39f, -16.21f);
                }
                else
                //8:5
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(8.0f / 5.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam8_5, 3.39f, -16.21f);
                }
                else
                //5:3
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(5.0f / 3.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam5_3, 3.39f, -16.21f);
                }
            }
            else
            {
                //16:9
                mainCamera.transform.position = new Vector3(cam16_9, blind_YAdjust, -16.21f);
                //16:10
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(16.0f / 10.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam16_10, blind_YAdjust, -16.21f);
                }
                else
                //4:3
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(4.0f / 3.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam4_3, blind_YAdjust, -16.21f);
                }
                else
                //3:2
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(3.0f / 2.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam3_2, blind_YAdjust, -16.21f);
                }
                else
                //8:5
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(8.0f / 5.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam8_5, blind_YAdjust, -16.21f);
                }
                else
                //5:3
                if (System.Math.Round((float)Screen.width / (float)Screen.height, 2) == System.Math.Round(5.0f / 3.0f, 2))
                {
                    mainCamera.transform.position = new Vector3(cam5_3, blind_YAdjust, -16.21f);
                }
            }
    }
}
