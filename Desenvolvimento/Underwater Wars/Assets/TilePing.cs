﻿using UnityEngine;
using System.Collections;

public class TilePing : MonoBehaviour
{
    private static TilePing lastPing;
    protected bool blindDoubleClick = false;

    public Vector2 pos;
    public UIController UICtrl;

    private static string[] xChars = new string[11] { "0", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };

    void Start()
    {
        var collider = gameObject.AddComponent<BoxCollider2D>();
        collider.isTrigger = true;
        if (!lastPing) lastPing = this;
    }

    void OnMouseDown()
    {
        if (UICtrl.isBlind)
        {
            if (blindBtnAction())
            {
                UICtrl.audio.StopAllVoices(0.9f);
                UICtrl.audio.PlayVoice("pingOnCell", 0.9f);
                UICtrl.audio.PlayVoice(xChars[(int)(pos.x + 1.5f)] + "_coord", 1.9f + 0.6f);
                UICtrl.audio.PlayVoice("_" + (int)(pos.y + 1.5f) + "_coord", 2.4f + 0.6f);
                UICtrl.sendPing(getPingCoord(), getPingString());
            }
            else
                if (UICtrl.audio)
            {
                UICtrl.audio.PlayVoice(xChars[(int)(pos.x + 1.5f)] + "_coord");
                UICtrl.audio.PlayVoice("_" + (int)(pos.y + 1.5f) + "_coord", 0.3f);
            }
        }
        else
        {
            UICtrl.sendPing(getPingCoord(), getPingString());
        }
    }

    private bool blindBtnAction()
    {
        blindDoubleClick = !blindDoubleClick;
        if (lastPing != this)
        {
            lastPing.blindDoubleClick = false;
            lastPing = this;
        }
        if (blindDoubleClick)
            Invoke("resetBlindBtnAction", 0.9f);

        return !blindDoubleClick;
    }
    private void resetBlindBtnAction()
    {
        blindDoubleClick = false;
    }

    public string getPingString()
    {
        return getCoordString(pos);
    }
    public Vector2 getPingCoord()
    {
        return new Vector2(pos.x, pos.y);
    }

    public static string getCoordString(Vector2 coord)
    {
        coord = new Vector2(coord.x + 1, coord.y + 1);
        if (coord.x < 1 || coord.x > 10)
            return ((int)coord.x + (int)coord.y).ToString();


        return xChars[(int)(coord.x + .5f)] + (int)(coord.y + .5f);
    }
}
