﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Translator : MonoBehaviour
{

    public Text textoAtraduzir;

    private string[] Words;
    private string[] Animation;
    private ArrayList MomentsStrings = new ArrayList();
    public ArrayList momentos = new ArrayList();
    bool playing = false;
    int currMoment = 0;
    public string CurrentRHandConf = "";
    public string CurrentLHandConf = "";
    int currentFaceExp = 0;
    public GameObject IkLookatR;
    public GameObject IkLookatL;
    public Transform LeftHandRot;
    public Transform RightHandRot;
    public Transform HeadRotation;
    public Transform HipRotation;
    public Transform HipBone;
    public Transform HeadBone;
    public Transform PalmR;
    public Transform PalmL;
    [Header("Rotation Vars")]
    Vector3 TempR;
    Vector3 TempL;
    Quaternion TempRotR;
    Quaternion TempRotL;
    Quaternion QHeadTemp;
    Quaternion QHipTemp;

    public Animator anim;
    float speedMoment = 1.0f;
    float sTime;
    float jLength;
    //public static float speed = 1f;
    float timerPrev = 0;
    float fracJourney = 0;
    bool knownWord = false;
    // Use this for initialization
    static Translator singleton;
    public TextMesh playerText;

    void Start()
    {

        //MomentsStrings = new ArrayList();
        //momentos = new ArrayList();

        singleton = this;
        TranslateAPI("coiso");
    }

    public static void setSpeed(float s)
    {
        singleton.anim.speed = s;
        BlendShapes.blendSpeed = 1f + s;
    }
    /*
    public void translate(string text)
    {
        StartCoroutine(translate_(text));
    }

    IEnumerator translate_(string text)
    {
        Debug.Log("translating :" + text);
        string animationParams = "";
        string tempChecker = "";

        char[] delimiters = new char[] { ',', '.', '?', '!' };
        string[] tempphrases = text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        for (int j = 0; j < tempphrases.Length; j++)
        {
            char[] delimiters2 = new char[] { ' ' };
            string[] tempWords = tempphrases[j].Split(delimiters2, StringSplitOptions.RemoveEmptyEntries);
            int counter = tempWords.Length;
            int init = 0;
            animationParams = CheckAnimationForWord(tempphrases[j]);

            if (animationParams != "")
            {
                knownWord = true;
                MomentsStrings.Add(animationParams);
            }
            else
            {
                while (init != counter && counter > 0)
                {

                    tempChecker = "";
                    for (int i = init; i < counter; i++)
                    {
                        tempChecker += tempWords[i] + " ";
                        Debug.Log(tempChecker);
                    }



                    tempChecker = tempChecker.Trim();
                    animationParams = CheckAnimationForWord(tempChecker);
                    // Debug.Log("The anim params for " + tempChecker + " are :" + animationParams);
                    Debug.Log(counter + "     " + init);
                    if (animationParams != "")
                    {
                        Debug.Log("encontrou match");
                        MomentsStrings.Add(animationParams);
                        init = counter;
                        counter = tempWords.Length;
                    }
                    else if (animationParams == "" && counter - 1 == init)
                    {
                        Debug.Log("nao encontrou match");
                        foreach (char s in tempChecker)
                        {
                            animationParams = CheckAnimationForWord(s.ToString().ToLowerInvariant());
                            if (animationParams != "")
                                MomentsStrings.Add(animationParams);
                        }
                        init++;
                        counter = tempWords.Length;
                    }
                    else
                    {
                        counter--;
                    }


                }

            }
            while (playing)
                yield return null;

            StartCoroutine("CreateMomentsoffline");
        }

    }
 
    public void translateText()
    {
        string allText = textoAtraduzir.text;
        translate(allText);
    }
       */

    public void startTranslate(string param)
    {
        StartCoroutine("StringToMoments", param);

    }




    public IEnumerator StringToMoments(string momentStrings)
    {

        //char[] delimiters = new char[] { ' ', ',', '.', '?', '!' };
        //string[] momentos = momentStrings.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        string[] momentostexto = momentStrings.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
        for (int i = 0; i < momentostexto.Length; i++)
        {
            if (momentostexto[i].Length > 1)
                CreateMoments(momentostexto[i]);
            //Debug.Log(i);
        }
        //  Debug.Log(momentos[0].ToString() + "-------" + momentos[1].ToString());
        StartCoroutine("TranslateMoments");
        yield return null;
    }

    public void EnterTranslate()
    {
        StartCoroutine(TranslateTextByServer());
    }
    public void TranslateAPI(string txt)
    {
        StartCoroutine(TranslateTextByServer(txt));
    }

    IEnumerator TranslateTextByServer()
    {
        WWWForm param = new WWWForm();
        param.AddField("inputText", textoAtraduzir.text);
        param.AddField("Lang", "PT");
        param.AddField("inputKey", "50c3dbb7d52cdbe1e816d27198b74e57");

        byte[] rawData = param.data;

        string url = "http://193.136.60.223/virtualsign/pt/APItest.php?method=getAllMomentsForText";
        WWW www = new WWW(url, param);
        yield return www;

        try
        {
            string response = www.text;
            Debug.Log("reply from server: " + response);
            startTranslate(response);
        }
        catch
        {
            Debug.Log("error requesting params for text: " + textoAtraduzir.text);
        }

    }
    IEnumerator TranslateTextByServer(string txt)
    {
        WWWForm param = new WWWForm();
        param.AddField("inputText", txt);
        param.AddField("Lang", "PT");
        param.AddField("inputKey", "50c3dbb7d52cdbe1e816d27198b74e57");

        byte[] rawData = param.data;

        string url = "http://193.136.60.223/virtualsign/pt/APItest.php?method=getAllMomentsForText";
        WWW www = new WWW(url, param);
        yield return www;

        try
        {
            string response = www.text;
            Debug.Log("reply from server: " + response);
            startTranslate(response);
        }
        catch
        {
            Debug.Log("error requesting params for text: " + textoAtraduzir.text);
        }

    }

    IEnumerator CreateMomentsoffline()
    {
        momentos.Clear();
        foreach (string confs in MomentsStrings)
        {
            string[] delimiters = new string[] { "||" };
            string[] moments = confs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < moments.Length; i++)
            {

                string[] delimiter = new string[] { "&" };
                string[] gestosAtraduzir = moments[i].Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

                Moment temp = new Moment(momentos.Count);

                if (gestosAtraduzir.Length > 9)
                {

                    temp.HandConfR = gestosAtraduzir[0];
                    Debug.Log(temp.HandConfR);
                    temp.PointFinalR = stringToVec3(gestosAtraduzir[1]);
                    temp.RotationsR = stringToQuaternion(gestosAtraduzir[2]);
                    // temp.RotationsR = stringToVec3(gestosAtraduzir[2]);
                    temp.HandConfL = gestosAtraduzir[3];
                    temp.PointFinalL = stringToVec3(gestosAtraduzir[4]);
                    temp.RotationsL = stringToQuaternion(gestosAtraduzir[5]);
                    //temp.RotationsL = stringToVec3(gestosAtraduzir[5]);
                    temp.FacialExpression = gestosAtraduzir[6];
                    temp.QHead = stringToQuaternion(gestosAtraduzir[7]);
                    temp.QHip = stringToQuaternion(gestosAtraduzir[8]);
                    temp.SpeedM = float.Parse(gestosAtraduzir[9]);
                    momentos.Add(temp);




                }
                else if (gestosAtraduzir.Length > 6)
                {

                    temp.HandConfR = gestosAtraduzir[0];
                    Debug.Log(temp.HandConfR);
                    temp.PointFinalR = stringToVec3(gestosAtraduzir[1]);
                    temp.RotationsR = stringToQuaternion(gestosAtraduzir[2]);
                    // temp.RotationsR = stringToVec3(gestosAtraduzir[2]);
                    temp.HandConfL = gestosAtraduzir[3];
                    temp.PointFinalL = stringToVec3(gestosAtraduzir[4]);
                    temp.RotationsL = stringToQuaternion(gestosAtraduzir[5]);
                    //temp.RotationsL = stringToVec3(gestosAtraduzir[5]);
                    temp.FacialExpression = gestosAtraduzir[6];
                    temp.QHead = HeadRotation.rotation;
                    temp.QHip = HipRotation.rotation;
                    temp.SpeedM = speedMoment;
                    momentos.Add(temp);
                    //pickCurrMoment(momentos.Count - 1);



                }
                else
                {
                    Debug.Log("Word had less than the required parameters, please update");
                }
            }

        }
        while (playing)
            yield return null;
        StartCoroutine("TranslateMoments");
        MomentsStrings.Clear();
    }


    public void CreateMoments(string momento)
    {


        string[] delimiters = new string[] { "||" };
        string[] moments = momento.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

        for (int i = 0; i < moments.Length; i++)
        {

            string[] delimiter = new string[] { "&" };
            string[] gestosAtraduzir = moments[i].Split(delimiter, StringSplitOptions.RemoveEmptyEntries);

            Moment temp = new Moment(momentos.Count);

            if (gestosAtraduzir.Length > 9)
            {

                temp.HandConfR = gestosAtraduzir[0];
                Debug.Log(temp.HandConfR);
                temp.PointFinalR = stringToVec3(gestosAtraduzir[1]);
                temp.RotationsR = stringToQuaternion(gestosAtraduzir[2]);
                // temp.RotationsR = stringToVec3(gestosAtraduzir[2]);
                temp.HandConfL = gestosAtraduzir[3];
                temp.PointFinalL = stringToVec3(gestosAtraduzir[4]);
                temp.RotationsL = stringToQuaternion(gestosAtraduzir[5]);
                //temp.RotationsL = stringToVec3(gestosAtraduzir[5]);
                temp.FacialExpression = gestosAtraduzir[6];
                temp.QHead = stringToQuaternion(gestosAtraduzir[7]);
                temp.QHip = stringToQuaternion(gestosAtraduzir[8]);
                temp.SpeedM = float.Parse(gestosAtraduzir[9]);
                momentos.Add(temp);




            }
            else if (gestosAtraduzir.Length > 6)
            {

                temp.HandConfR = gestosAtraduzir[0].Trim();
                //Debug.Log(temp.HandConfR);
                temp.PointFinalR = stringToVec3(gestosAtraduzir[1]);
                temp.RotationsR = stringToQuaternion(gestosAtraduzir[2]);
                // temp.RotationsR = stringToVec3(gestosAtraduzir[2]);
                temp.HandConfL = gestosAtraduzir[3];
                temp.PointFinalL = stringToVec3(gestosAtraduzir[4]);
                temp.RotationsL = stringToQuaternion(gestosAtraduzir[5]);
                //temp.RotationsL = stringToVec3(gestosAtraduzir[5]);
                temp.FacialExpression = gestosAtraduzir[6];
                temp.QHead = HeadRotation.rotation;
                temp.QHip = HipRotation.rotation;
                temp.SpeedM = speedMoment;
                momentos.Add(temp);
                //pickCurrMoment(momentos.Count - 1);



            }
            else
            {
                Debug.Log("Word had less than the required parameters, please update");
            }

        }

        //  while (playing)
        //      yield return null;


    }

    public void FacialExpression(int anim)
    {

        BlendShapes.targetBlend = anim;
        BlendShapes.changeShape = true;


    }

    IEnumerator TranslateMoments()
    {


        playing = true;

        currMoment = 0;
        //  Debug.Log(momentos.Count+" numero de momentos");

        foreach (Moment m in momentos)
        {
            try {
                QHipTemp = HipRotation.rotation;

                TempR = IkLookatR.transform.position;
                TempL = IkLookatL.transform.position;
                TempRotR = RightHandRot.rotation;
                TempRotL = LeftHandRot.rotation;
                QHeadTemp = HeadRotation.rotation;
                speedMoment = m.SpeedM;
                // Debug.Log("MOMENTO COM CONF " + m.HandConfR);
                timerPrev = 0;
                fracJourney = 0;
                // HipRotation.rotation = ((Moment)momentos[currMoment]).QHip;
                //headRotation.rotation= ((Moment)momentos[currMoment]).QHead;
                if (BlendShapes.targetBlend != Int32.Parse(m.FacialExpression))
                    FacialExpression(Int32.Parse(m.FacialExpression));
                if (m.HandConfL != CurrentLHandConf)
                    anim.CrossFade(m.HandConfL, 0.9f);
                //anim.SetTrigger(m.HandConfL);
                if (m.HandConfR != CurrentRHandConf)
                    anim.CrossFade(m.HandConfR, 0.9f);
                //anim.SetTrigger(m.HandConfR);

                sTime = Time.time;
                if (momentos.Count > currMoment)
                {
                    float RLength = Vector3.Distance(IkLookatR.transform.position, ((Moment)momentos[currMoment]).PointFinalR);
                    float LLength = Vector3.Distance(IkLookatL.transform.position, ((Moment)momentos[currMoment]).PointFinalL);
                    if (RLength > LLength)
                        jLength = RLength;
                    else
                        jLength = LLength;
                }
                CurrentLHandConf = m.HandConfL;
                CurrentRHandConf = m.HandConfR;

            }
            catch (Exception) { }

            //        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(3).length / anim.GetCurrentAnimatorStateInfo(3).speed);


            while (fracJourney < 1f)
            {
                //   Debug.Log("frac is " + fracJourney);
                yield return null;
            }
            yield return new WaitForEndOfFrame();

            currMoment++;
        }
        currMoment--;
        //  currMoment = keptMoment;
        playing = false;
        currMoment = momentos.Count - 1;
        //provavelmente forcar o ultimo moment
        momentos.Clear();
        yield return null;
    }

    /*
    public string CheckAnimationForWord(string word)
    {
        word = word.ToLowerInvariant();
        string anim = "";
        Words = ModuloLeitura.list_palavras.ToArray();
        Animation = ModuloLeitura.list_Combinacoes.ToArray();
        for (int i = 0; i < Animation.Length; i++)
        {
            // Debug.Log(palavras[i] + "-------------->" + palavra);
            if (Words[i].ToLowerInvariant().Equals(word))
            {
                anim = Animation[i];
            }
        }
        return anim;
    }

    */
    public string vec3ToString(Vector3 vec3)
    {
        string temp = vec3.x + "," + vec3.y + "," + vec3.z;
        Debug.Log(temp);
        return temp;
    }

    public Vector3 stringToVec3(string vec)
    {
        string[] xyz = vec.Split(',');
        Vector3 temp = new Vector3(float.Parse(xyz[0]), float.Parse(xyz[1]), float.Parse(xyz[2]));

        return temp;
    }

    public string QuaternionToString(Quaternion Qua)
    {
        string temp = Qua.x + "," + Qua.y + "," + Qua.z + "," + Qua.w;
        Debug.Log(temp);
        return temp;
    }

    public Quaternion stringToQuaternion(string vec)
    {
        string[] xyzw = vec.Split(',');
        Quaternion temp = new Quaternion(float.Parse(xyzw[0]), float.Parse(xyzw[1]), float.Parse(xyzw[2]), float.Parse(xyzw[3]));

        return temp;
    }




    // Update is called once per frame
    void Update()
    {

        /* if(Input.GetKeyDown(KeyCode.Return))
         startTranslate("G9_6R&0.4027042,1.289,1.193377&0.6774886,-0.2030887,-0.1432023,-0.6922847&nenhumaLL&0.6276926,1.009171,1.201562&0.5948294,-0.002022881,-0.4279524,-0.6804643&5||G9_6R&0.3602922,1.220306,1.193225&0.6823922,-0.2090758,-0.1376737,-0.6867858&nenhumaLL&0.6276926,1.009171,1.201562&0.5948293,-0.002022814,-0.4279523,-0.6804644&5||G9_6R&0.3178802,1.161714,1.193109&0.6876117,-0.2142714,-0.132929,-0.680888&nenhumaLL&0.6276926,1.009171,1.201562&0.5948293,-0.002022842,-0.4279523,-0.6804644&5\nG9_6R &0.4027042,1.289,1.193377&0.6774886,-0.2030887,-0.1432023,-0.6922847&nenhumaLL&0.6276926,1.009171,1.201562&0.5948294,-0.002022881,-0.4279524,-0.6804643&5||G9_6R&0.3602922,1.220306,1.193225&0.6823922,-0.2090758,-0.1376737,-0.6867858&nenhumaLL&0.6276926,1.009171,1.201562&0.5948293,-0.002022814,-0.4279523,-0.6804644&5||G9_6R&0.3178802,1.161714,1.193109&0.6876117,-0.2142714,-0.132929,-0.680888&nenhumaLL&0.6276926,1.009171,1.201562&0.5948293,-0.002022842,-0.4279523,-0.6804644&5");
         */

        if (playing)
        {
            //    float distCovered = (Time.time - sTime) * speed*0.1f;
            //  float fracJourney = distCovered / jLength;

            timerPrev += Time.deltaTime;
            fracJourney = timerPrev / anim.GetCurrentAnimatorStateInfo(3).length;

            Vector3 checker = Vector3.zero;
            bool checkerHasValue = false;
            if (momentos.Count > currMoment)
            {
                checkerHasValue = true;
                checker = Vector3.Lerp(IkLookatR.transform.position, ((Moment)momentos[currMoment]).PointFinalR, fracJourney);
            }

            if (checkerHasValue && !float.IsNaN(checker.x))
            {
                IkLookatR.transform.position = Vector3.Lerp(IkLookatR.transform.position, ((Moment)momentos[currMoment]).PointFinalR, fracJourney);
            }

            if (checkerHasValue)
                checker = Vector3.Lerp(IkLookatL.transform.position, ((Moment)momentos[currMoment]).PointFinalL, fracJourney);

            if (!float.IsNaN(checker.x))
            {
                if (checkerHasValue)
                    IkLookatL.transform.position = Vector3.Lerp(IkLookatL.transform.position, ((Moment)momentos[currMoment]).PointFinalL, fracJourney);
            }

        }
    }

    void LateUpdate()
    {

        if (playing && momentos.Count > currMoment)
        {
            Quaternion tempChecker = Quaternion.Slerp(QHipTemp, ((Moment)momentos[currMoment]).QHip, fracJourney); ;
            if (!float.IsNaN(tempChecker.x))
            {
                HipRotation.rotation = Quaternion.Slerp(QHipTemp, ((Moment)momentos[currMoment]).QHip, fracJourney);
                HipBone.transform.rotation = HipRotation.rotation;
            }

            tempChecker = Quaternion.Slerp(TempRotR, ((Moment)momentos[currMoment]).RotationsR, fracJourney); ;
            if (!float.IsNaN(tempChecker.x))
            {
                RightHandRot.rotation = Quaternion.Slerp(TempRotR, ((Moment)momentos[currMoment]).RotationsR, fracJourney);
                //Debug.Log("rotation frac: " + fracJourney);
                PalmR.transform.rotation = RightHandRot.rotation;
            }
            tempChecker = Quaternion.Slerp(TempRotL, ((Moment)momentos[currMoment]).RotationsL, fracJourney); ;
            if (!float.IsNaN(tempChecker.x))
            {

                LeftHandRot.rotation = Quaternion.Slerp(TempRotL, ((Moment)momentos[currMoment]).RotationsL, fracJourney);
                PalmL.transform.rotation = LeftHandRot.rotation;

            }
            tempChecker = Quaternion.Slerp(QHeadTemp, ((Moment)momentos[currMoment]).QHead, fracJourney); ;
            if (!float.IsNaN(tempChecker.x))
            {
                HeadRotation.rotation = Quaternion.Slerp(QHeadTemp, ((Moment)momentos[currMoment]).QHead, fracJourney);
                HeadBone.transform.rotation = HeadRotation.rotation;


            }



        }
        else
        {
            HipBone.transform.rotation = HipRotation.rotation;
            HeadBone.transform.rotation = HeadRotation.rotation;

            PalmR.transform.rotation = RightHandRot.rotation;
            PalmL.transform.rotation = LeftHandRot.rotation;
        }

    }

    public class Moment
    {

        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        string handConfR;

        public string HandConfR
        {
            get { return handConfR; }
            set { handConfR = value; }
        }
        string handRotR;

        public string HandRotR
        {
            get { return handRotR; }
            set { handRotR = value; }
        }


        string pointNameR;

        public string PointNameR
        {
            get { return pointNameR; }
            set { pointNameR = value; }
        }

        string handConfL;

        public string HandConfL
        {
            get { return handConfL; }
            set { handConfL = value; }
        }
        string handRotL;

        public string HandRotL
        {
            get { return handRotL; }
            set { handRotL = value; }
        }



        string pointNameL;

        public string PointNameL
        {
            get { return pointNameL; }
            set { pointNameL = value; }
        }

        Vector3 pointFinalL;

        public Vector3 PointFinalL
        {
            get { return pointFinalL; }
            set { pointFinalL = value; }
        }

        Vector3 pointFinalR;

        public Vector3 PointFinalR
        {
            get { return pointFinalR; }
            set { pointFinalR = value; }
        }
        /*
                Vector3 pointInitL;

                public Vector3 PointInitL
                {
                    get { return pointInitL; }
                    set { pointInitL = value; }
                }

                Vector3 pointInitR;

                public Vector3 PointInitR
                {
                    get { return pointInitR; }
                    set { pointInitR = value; }
                }
                */
        /*
      Vector3 rotationsL;

      public Vector3 RotationsL
      {
          get { return rotationsL; }
          set { rotationsL = value; }
      }

      Vector3 rotationsR;

      public Vector3 RotationsR
      {
          get { return rotationsR; }
          set { rotationsR = value; }
      }*/

        Quaternion rotationsL;

        public Quaternion RotationsL
        {
            get { return rotationsL; }
            set { rotationsL = value; }
        }

        Quaternion rotationsR;

        public Quaternion RotationsR
        {
            get { return rotationsR; }
            set { rotationsR = value; }
        }

        string facialExpression = "0";

        public string FacialExpression
        {
            get { return facialExpression; }
            set { facialExpression = value; }
        }
        Quaternion qHead;

        public Quaternion QHead
        {
            get { return qHead; }
            set { qHead = value; }
        }

        Quaternion qHip;

        public Quaternion QHip
        {
            get { return qHip; }
            set { qHip = value; }
        }

        float speedM;

        public float SpeedM
        {
            get { return speedM; }
            set { speedM = value; }
        }


        public Moment(int counter)
        {
            id = counter;
            //    pointNameL = "";
            //   pointNameR = "";
            facialExpression = "2";
            // handConfL = "Empty";
            // handConfR = "Empty";
            //  handRotL = "G8_3L";
            //   handRotR = "G8_3R";
            //  PointInitR = new Vector3(0.11f, 1.24f, 1.31f);
            //    PointInitL = new Vector3(0.87f, 1.24f, 1.32f);
            //  RotationsR = new Vector3(0, 0, 0);
            // RotationsL =new Vector3(19.43f, -55.93f, -77.32f);
            PointFinalR = new Vector3(0.11f, 1.24f, 1.31f);
            PointFinalL = new Vector3(0.87f, 1.24f, 1.32f);
            speedM = 1.0f;
        }




    }

}
