﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PingSpriteCtrl : MonoBehaviour {

    public Animator anim;
    public Image img;
    float standardSpeed = 1;

    void Start()
    {
        anim=GetComponent<Animator>();
        img = GetComponent<Image>();
    }

    public void moveLocal(Vector2 local)
    {
        transform.localPosition = new Vector3(local.x, local.y, 0);
    }

    public void playAnim()
    {
        anim.SetFloat("animSpeedMult", standardSpeed);
        anim.SetBool("playTrigger",true);
    }

    public bool isPlaying()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("pingAnim");
    }

    public void killAnim()
    {
        anim.SetFloat("animSpeedMult", 99999);
    }

    public GameObject getGameObject()
    {
        return gameObject;
    }
}
