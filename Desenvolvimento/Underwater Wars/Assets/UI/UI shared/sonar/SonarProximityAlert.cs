﻿using UnityEngine;
using System.Collections;

public class SonarProximityAlert : MonoBehaviour
{

    public Animator anim;
    public float proximityRadiosInUnits = 1.5f;
    private bool _isInProximity = false;
    public bool isInProximity
    {
        get
        {
            return _isInProximity;
        }
        set
        {
            _isInProximity = value;
            if (anim)
                anim.SetBool("flash", _isInProximity);
            audioBeep(_isInProximity);
        }
    }

    private static int counter = 0;
    private AudioManager audio;
    private bool isPlayingAudio = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        GameObject audioObj = GameObject.Find("AudioManager");
        if (audioObj)
            audio = audioObj.GetComponent<AudioManager>();
    }


    public GameObject getGameObject()
    {
        return gameObject;
    }

    public void updateProximity(float vectorMagnitude)
    {
        if (proximityRadiosInUnits > vectorMagnitude)
        {
            isInProximity = true;
            counter++;
        }
    }

    public int resetProximityCounter()
    {
        int ret = counter;
        counter = 0;
        return ret;
    }

    private void audioBeep(bool onOrOff)
    {
        if (audio)
        {
            if (onOrOff)
            {
                if (!isPlayingAudio)
                    audio.PlayEffect("proximityWarningBeep", true);
                isPlayingAudio = true;
            }
            else
            {
                isPlayingAudio = false;
                audio.StopEffect("proximityWarningBeep");
            }
        }
    }
}


